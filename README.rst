========================================================
 Linear Reconstruction of Slitless Spectroscopy: LINEAR
========================================================

:Authors: 
  Russell Ryan,
  Stefano Casertano

:Version:
  0.8 :math:`\beta`
  
:Contact:
  rryan@stsci.edu, 
  (410) 338-4352

--------------
 Introduction
--------------

LINEAR is a set of IDL and C codes developed to simulate dispersed
imaging and extract one-dimensional spectra.  Unlike most grism
extraction codes, LINEAR determines the optimal spectrum (for each
source) from the entire canon of available data (that may be rolled or
dithered).  For this :math:`\beta` distribution, we release only the 
simulation-based modules 


+ **simulation:**   Produce a noiseless grism image.

+ **contamination:**   Predict contamination as a function of wavelength for each source.

+ **footprint:**   Generate a *footprint* image for each source. A *footprint* image is a dispersed image, where the pixel value is equal to the average wavelength (averaged over the direct-image footprint).



---------------------
 Directory Structure
---------------------


config/
   Contains all the configuration files for LINEAR.

   config/aXe/ 
       Place holder directory for aXe-style configuration files for 
       descrbing grism properties (including dispersion, trace, and 
       sensitivity).


   config/linear/
       Contains relevant configuration files for running linear.


filters/
   Contains the filter curves used. Note, these files are in a particular 
   format.

src/ 
   Contains all the code.

   linear/
      Contains the IDL code developed for LINEAR.
   utilities/
      Contains general purpose code (IDL and C).
   others/
      Contains several subdirectories contiaining published code that was 
      not developed by Ryan & Casertano as part of linear.

      others/astrolib
         Contains the used IDL-ASTROLIB code.
      others/cbmarkwardt
         Contains mpfit developed by C. Markwardt, used by linear to 
	 perform reverse SIP processes.
      others/jdsmith
         Contains polyfill*pro and *c codes developed by JD Smith, used
	 by linear to compute fractional pixel overlaps
   aXe/
      Contains routines developed by Ryan & Casertano to work with 
      aXe-like configuration files and data.
 	 

man/
   Contains the linear manual by Ryan & Casertano, which includes 
   installation and running instructions.


