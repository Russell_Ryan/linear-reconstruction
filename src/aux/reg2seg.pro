;+
; simple routine to convert from ds9 regions file to a fits
; segmentation map (a la sextractor).
;
; R. Ryan
;-




function reg2seg_filetest,file,name,WRITE=write
  ;+
  ; little script to verify a file exists or can be written
  ;-

  if size(file,/type) ne 7 then begin
     print,name+' should be a string.'
     return,0b
  endif else begin
     if keyword_set(WRITE) then begin
        
        openw,lun,file,/get_lun,error=error
        if error ne 0 then begin
           print,'FILE ('+file+') cannot be written.',error
           return,0b
        endif
        free_lun,lun
     endif else begin
        if ~file_test(file,/read) then begin
           print,name+' ('+file+') not found.'
           return,0b
        endif
     endelse
  endelse

  return,1b
end




pro reg2seg,regfile,imgfile,segfile,SILENT=silent,NRES=nres,GROW=grow
  ;regfile = the saved ds9 regions file
  ;imgfile = the FITS file to grab the WCS info from
  ;segfile = output fits filename

  if n_params() lt 3 then begin
     print,'reg2seg,regfile,imgfile,segfile,/SILENT,NRES=nres,GROW=grow'
     return
  endif


  ;check for the inputs
  if ~reg2seg_filetest(regfile,'regfile') then return
  if ~reg2seg_filetest(imgfile,'imgfile') then return
  if ~reg2seg_filetest(segfile,'segfile',/write) then return

  ;set some parameters
  if ~keyword_set(NRES) then nres=101L ;the resolution of the polygon
  

  ;read the regfile  
  readfmt,regfile,'(A1000)',reg,SILENT=silent  
  reg=strtrim(reg,2)
  nline=n_elements(reg)

  
  ;read the header
  max_exten=1
  exten=0
  repeat begin
     h=headfits(imgfile,exten=exten,errmsg=errmsg)
     valid=strcmp(errmsg,'')
     if valid then begin
        extast,h,ast,nopar
        valid=nopar ge 1
     endif
     exten++
  endrep until ((exten eq max_exten+1) || valid)
  if ~valid then begin
     print,'IMGFILE does not contain valid astrometry.'
     return
  endif

  ;process to get the valid lines and possible requested IDs
  shape=strarr(nline)
  request=lonarr(nline)
  for i=0u,nline-1 do begin
     l=strpos(reg[i],'(')
     r=strpos(reg[i],')')
     if l ne -1 && r ne -1 then begin
        
        vals=strsplit(strmid(reg[i],l+1,r-l-1),',',/ext,count=num)
        if array_equal(valid_num(vals),1b) then begin
           shape[i]=strlowcase(strmid(reg[i],0,l))
           
           ;check for a requested ID
           ll=strpos(reg[i],'{')
           rr=strpos(reg[i],'}')
           if ll ne -1 && rr ne -1 then begin
              index=strmid(reg[i],ll+1,rr-ll-1)
              if valid_num(index,/int) then begin
                 index=long(index)
                 if index eq 0 then print,'WARNING.'
                 request[i]=index
              endif
           endif
        endif
     endif
  endfor
  
  ;check for valid shapes
  g=where(shape ne '',nreg)
  if nreg eq 0 then stop

  ;extract good shapes
  reg=reg[g]
  shape=shape[g]
  request=request[g]
  index=indgen(nreg)+1

  ;find the elements in one, but not the other
  use=cgsetdifference(index,request)
  j=0
  for i=0,nreg-1 do index[i]=(request[i] ne 0)?request[i]:use[j++]
  

  ;make the segmentation map
  seg=lonarr(ast.naxis)


  ;make dummy images
  xim=rebin(reform(findgen(ast.naxis[0]),ast.naxis[0],1),ast.naxis)
  yim=rebin(reform(findgen(ast.naxis[1]),1,ast.naxis[1]),ast.naxis)
  


  ;make image
  for i=0,nreg-1 do begin
     
     ;find good locations
     l=strpos(reg[i],'(')
     r=strpos(reg[i],')')
     vals=float(strsplit(strmid(reg[i],l+1,r-l-1),',',/ext,count=num))
     

     ;process each shape
     valid=1b
     case shape[i] of
        'ellipse': begin        ;extract data for an ellipse
           xc=vals[0]-1         ;subtract for the ds9/IDL coordinates
           yc=vals[1]-1         ;subtract for the ds9/IDL coordinates
           dx=vals[2]
           dy=vals[3]
           pa=vals[4]
           
           ;make dummy arrays
           x=findgen(nres)/(nres-1)*2-1
           y=sqrt(1-x*x)

           ;make the border of the ellipse
           xx=[x,+reverse(x)]*dx
           yy=[y,-reverse(y)]*dy
           
           ;do some trig
           cs=cos(pa*!PI/180.)
           sn=sin(pa*!PI/180.)
              
           ;rotate the ellipse with the rotation matrix
           x=xc+cs*xx-sn*yy
           y=yc+sn*xx+cs*yy
           
        end
        'circle': begin         ;process a circle
           xc=vals[0]-1         ;subtract for the ds9/IDL coordinates
           yc=vals[1]-1         ;subtract for the ds9/IDL coordinates
           r=vals[2]
           
           ;make dummy arrays
           x=findgen(nres)/(nres-1)*2-1
           y=sqrt(1-x*x)

           ;make the border of the ellipse
           x=[x,+reverse(x)]*r+xc
           y=[y,-reverse(y)]*r+yc
           
        end
        'box': begin            ;process a box
           xc=vals[0]-1         ;subtract for the ds9/IDL coordinates
           yc=vals[1]-1         ;subtract for the ds9/IDL coordinates
           dx=vals[2]
           dy=vals[3]
           pa=vals[4]
              
           xx=[-1,1,1,-1]*dx/2.
           yy=[-1,-1,1,1]*dy/2.

           ;do some trig
           cs=cos(pa*!PI/180.)
           sn=sin(pa*!PI/180.)
           
           ;rotate the ellipse with the rotation matrix
           x=xc+cs*xx-sn*yy
           y=yc+sn*xx+cs*yy
        end
        'polygon': begin        ;process a polygon
           ii=indgen(num/2)
           x=vals[2*ii]-1       ;subtract 1 for ds9/IDL coordinates
           y=vals[2*ii+1]-1     ;subtract 1 for ds9/IDL coordinates
        end
        
        else: begin
           print,shape[i]+' was ignored.'
           valid=0b
        end
     endcase


     if valid then begin
        
        ;apply a window... this makes inside faster
        x0=min(x,max=x1)
        y0=min(y,max=y1)
        g=where(xim ge floor(x0) and xim le ceil(x1) and $
                yim ge floor(y0) and yim le ceil(y1),n)
        if n ne 0 then begin

           ;compute which pixels are inside           
           q=where(inside(xim[g],yim[g],x,y))
        
           if keyword_set(GROW) then stop,'NOT IMPLEMENTED YET'
        
                                
           ;fill the segmentation map with this image
           seg[g[q]]=index[i]
        endif else print,'REG2SEG> region is off the frame.'
           
     endif

  endfor

  ;check for uniqueness
  q=uniq(regid,sort(regid))
  if n_elements(q) ne nreg then $
     print,'REG2SEG> There are multiple entries with the same SEGID.'
      

  ;make a header for the segmentation image
  mkhdr,hdr,seg

  ;fill the header with WCS that matches the IMGFILE
  putast,hdr,ast

  ;write the file to disk
  writefits,segfile,seg,hdr



end

