;+
; STAMP MUST HAVE:
;   normal WCS keywords
;   LTV1,LTV2.  they are the negative value of the (x,y) pix that
;               indicate the lower left corner in the mosaic
;   SEGID       should be integer at present (a string eventually)
;
; STAMP MAY HAVE:
;   dlam        to specify the value for each object
;
;-

;pro linear_prepare,imgfile,segfile,root,PAD=pad
imgfile='img.fits'
segfile='seg.fits'
root='test'

  if n_elements(pad) ne 2 then pad=[1,1]
  
  dlamb=[25,25.,50.,100]

  img=readfits(imgfile,himg,/sil)
  seg=readfits(segfile,hseg,/sil)
  extast,himg,ast,noparam
  if noparam eq -1 then stop

  c=compress_index(seg,ind_uniq=segid,count=nobj)
  nobj--                        ;remove the sky 
  h=histogram(c,reverse=ri,min=1,max=nobj,locations=ind)
  b=segid[ind]

  ;put in the header to the zeroth image
  sxaddpar,hdr0,'NAXIS1',ast.naxis[0]
  sxaddpar,hdr0,'NAXIS2',ast.naxis[1]
  sxaddpar,hdr0,'CRPIX1',ast.crpix[0]
  sxaddpar,hdr0,'CRPIX2',ast.crpix[1]
  sxaddpar,hdr0,'CRVAL1',ast.crval[0]
  sxaddpar,hdr0,'CRVAL2',ast.crval[1]
  sxaddpar,hdr0,'CTYPE1',ast.ctype[0]
  sxaddpar,hdr0,'CTYPE2',ast.ctype[1]
  sxaddpar,hdr0,'CD1_1',ast.cd[0,0]
  sxaddpar,hdr0,'CD1_2',ast.cd[0,1]
  sxaddpar,hdr0,'CD2_1',ast.cd[1,0]
  sxaddpar,hdr0,'CD2_2',ast.cd[1,1]
  sxaddpar,hdr0,'EQUINOX',ast.equinox
  sxaddpar,hdr0,'RADESYS',ast.radecsys

  ;write the 0th extension
  mwrfits,0,root+'_img.fits',hdr0,/create
  mwrfits,0,root+'_gpx.fits',hdr0,/create
  
  
  ;open the files to append
  fits_open,root+'_img.fits',fcbi,/append
  fits_open,root+'_gpx.fits',fcbg,/append

  for i=0,nobj-1 do begin
     ;get some lookup values
;     s=segid[ind[i]]
     exten_no=i+1

     ;compute the bounding box
     ij=array_indices(ast.naxis,ri[ri[i]:ri[i+1]-1],/dim)
     i0=(min(ij[0,*])-pad[0])>0 & i1=(max(ij[0,*])+pad[0])<(ast.naxis[0]-1)
     j0=(min(ij[1,*])-pad[1])>0 & j1=(max(ij[1,*])+pad[1])<(ast.naxis[1]-1)

     ;cut out the stamp
     hextract,img,himg,img2,himg2,i0,i1,j0,j1,/silent
     hextract,seg,hseg,seg2,hseg2,i0,i1,j0,j1,/silent


     ;update the image header
     sxaddpar,himg2,'SEGID',s,' Segmentation ID'
     sxaddpar,himg2,'LTV1',-i0,' physical-to-image coordinate'
     sxaddpar,himg2,'LTV2',-j0,' physical-to-image coordinate'
     sxaddpar,himg2,'EXTNUM',exten_no,' extension number'
     sxaddpar,himg2,'dlamb',dlamb[i],' extraction resolution in A'
     fits_write,fcbi,img2,himg2,extname=strtrim(s,2)

     ;update the segmentation header
     sxaddpar,hseg2,'BITPIX',8,' Number of bits per data pixel'
     sxaddpar,hseg2,'SEGID',s,' Segmentation ID'
     sxaddpar,hseg2,'LTV1',-i0,' physical-to-image coordinate'
     sxaddpar,hseg2,'LTV2',-j0,' physical-to-image coordinate'
     sxaddpar,hseg2,'EXTNUM',exten_no,' extension number'
     sxaddpar,hseg2,'dlamb',dlamb[i],' extraction resolution in A'
     fits_write,fcbg,seg2 eq s,hseg2,extname=strtrim(s,2)

  endfor

  ;close the fits files
  fits_close,fcbi
  fits_close,fcbg

 

end
