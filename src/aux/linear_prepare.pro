pro linear_prepare,imgfile,segfile,root,PAD=pad,DLAMB=dlamb

  quiet=!quiet & !quiet=1b

  if n_elements(pad) ne 2 then pad=[1,1]*3
  



  ;read the images
  print,'LINEAR_PREPARE> Reading fits files.'
  img=readfits(imgfile,himg,/sil)
  seg=readfits(segfile,hseg,/sil)
  extast,himg,ast,noparam
  if noparam le 0 then begin
     print,'LINEAR_PREPARE> Error: direct image does not have valid astrometry.'
     return
  end
  

 
  ;compute the number of pixels per object
  print,'LINEAR_PREPARE> Compressing SEG image.'
  c=compress_index(seg,ind_uniq=segid,count=nobj)
  nobj--                        ;remove the sky 
  h=histogram(c,reverse=ri,min=1,max=nobj,locations=ind)
  segid=segid[ind]

  ;do we write dlamb?
  write_dlamb=n_elements(dlamb) eq nobj


  ;open the files to write the extensions
  fits_open,root+'_img.fits',fcbi,/write
  fits_open,root+'_seg.fits',fcbg,/write
  

;  window,0,retain=2,title='image'
;  window,1,retain=2,title='segmap'
  
  for i=0u,nobj-1 do begin
     exten_no=i+1

     ;compute the bounding box
     ij=array_indices(ast.naxis,ri[ri[i]:ri[i+1]-1],/dim)
     i0=(min(ij[0,*])-pad[0])>0 & i1=(max(ij[0,*])+pad[0])<(ast.naxis[0]-1)
     j0=(min(ij[1,*])-pad[1])>0 & j1=(max(ij[1,*])+pad[1])<(ast.naxis[1]-1)
     sz=[i1-i0,j1-j0]+1

     print,i,sz[0],sz[1]


     ;cut out the stamp
     hextract,img,himg,img2,himg2,i0,i1,j0,j1,/silent
     hextract,seg,hseg,seg2,hseg2,i0,i1,j0,j1,/silent

     ;make a new header
     mkhdr,h,img2
     extast,himg2,ast2
     putast,h,ast2

     ;update the new header
     sxaddpar,h,'SEGID',segid[i],' Segmentation ID'
     sxaddpar,h,'LTV1',-i0,' physical-to-image coordinate'
     sxaddpar,h,'LTV2',-j0,' physical-to-image coordinate'
     sxaddpar,h,'EXTNUM',exten_no,' extension number'
     if write_dlamb then $
        sxaddpar,h,'dlamb',dlamb[i],' extraction resolution in A'



     ;make a bitmap
     gpx=seg2 eq segid[i]
     if max(gpx) ne 1 then stop,'ERROR'


     ;update the image
;     sxaddpar,himg2,'SEGID',segid[i],' Segmentation ID'
;     sxaddpar,himg2,'LTV1',-i0,' physical-to-image coordinate'
;     sxaddpar,himg2,'LTV2',-j0,' physical-to-image coordinate'
;     sxaddpar,himg2,'EXTNUM',exten_no,' extension number'
;     if write_dlamb then $
;        sxaddpar,himg2,'dlamb',dlamb[i],' extraction resolution in A'
     fits_write,fcbi,img2,h,extname=strtrim(segid[i],2)

;     wset,0
;     imdisp,img2,/erase,/axis

     ;update the bpx 
     sxaddpar,h,'BITPIX',8,' Number of bits per data pixel'
;     sxaddpar,hseg2,'BITPIX',8,' Number of bits per data pixel'
;     sxaddpar,hseg2,'SEGID',segid[i],' Segmentation ID'
;     sxaddpar,hseg2,'LTV1',-i0,' physical-to-image coordinate'
;     sxaddpar,hseg2,'LTV2',-j0,' physical-to-image coordinate'
;     sxaddpar,hseg2,'EXTNUM',exten_no,' extension number'
;     if write_dlamb then $
;        sxaddpar,hseg2,'dlamb',dlamb[i],' extraction resolution in A'
     fits_write,fcbg,gpx,h,extname=strtrim(segid[i],2)

     
;     wset,1
;     g=where(seg2 eq 0)
;     ss=alog10(seg2)
;     ss[g]=0.
;     imdisp,ss,/erase,/axis

  endfor

  ;close the files
  fits_close,fcbi
  fits_close,fcbg



  t=check_math()
  !quiet=quiet
end
