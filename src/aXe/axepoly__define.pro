;+
;
; AXEPOLY__DEFINE
;
; An object to handle the funky 2-d polynomials defined by aXe, see
; the aXe manual.
;
;-


function axepoly::order
  ;+
  ; return the order
  ;-

  return,self.npar-1
end

function axepoly::poly2d,xy,DOUBLE=double,NPIX=npix
  ;+
  ; evaluate the aXe 2-d polynomials.  this produces polynomial 
  ; coefficients that can be passed to poly (the intrinsic IDL)
  ;-

  type=keyword_set(DOUBLE)?5:4  ;data type of output

  ;extract the number pixels
  sz=size(xy,/dim)
  npix=(size(xy,/n_dim) eq 1)?1:sz[1]


  ;output coefficients
  p=make_array([self.npar,npix],type=type)

  ;loop over parameters
  for i=0,self.npar-1 do begin

     ;extract the spatial coefficients
     coef=(*(*self.coef)[i])
     
     ;loop over the spatial functions.  
     ii=0L
     for j=0,(*self.numb)[i]-1 do begin
        for k=0,j do begin
           p[i,*]+=(coef[ii++]*xy[0,*]^(j-k)*xy[1,*]^k)
        endfor
     endfor

     ;alternative way with only one loop:
;     j=rebin(reform(indgen(n),1,n),n,n)
;     k=transpose(j)
;     g=where(j ge k,nn)
;     j=j[g]-k[g]
;     k=k[g]
;     for ii=0,n-1 do p[i,*]+=(coef[ii]*xy[0,*]^j[ii]*xy[1,*]^k[ii])
  endfor


  

  return,p
end


pro axepoly::cleanup
  ;+
  ; object maintanence
  ;-
  for i=0,self.npar-1 do ptr_free,(*self.coef)[i]

  ptr_free,self.coef,self.numb
end

function axepoly::init,file,key,beam
  ;+
  ; object initalizeation
  ;-
  
  case size(file,/type) of 
     7: conf=config_read(file)
     8: conf=file
     else: stop,'AXEPOLY::INIT> unable to init the polynomial.'
  endcase
     

  ;get the tags
  tags=tag_names(conf)

  ;get a thing to compare against
  test=strupcase(key+'_'+beam)
  g=where(strmid(tags,0,strlen(test)) eq test,n)
  if n eq 0 then return,0b


  ;build outputs
  self.npar=n
  coef=ptrarr(self.npar)
  numb=intarr(self.npar)

  ;loop over npar
  for i=0,self.npar-1 do begin
     g=(where(strmatch(tags,key+'_'+beam+'_'+string(i,f='(I0)'),/fold),n))[0]
     if n eq 0 then begin
        if self.npar eq 1 then begin
           g=(where(strmatch(tags,key+'_'+beam,/fold),n))[0]
           if n eq 0 then begin
              print,'AXEPOLY::INIT> error reading polynomial (no tags)?'
              stop
           endif
        endif else begin
           print,'AXEPOLY::INIT> error reading polynomial (invalid npar)'
           stop
        endelse
     endif
     c=conf.(g)
     n=n_elements(c)
     numb[i]=round((sqrt(1+8*n)-1)/2)
     coef[i]=ptr_new(conf.(g))
  endfor


  self.numb=ptr_new(numb)
  self.coef=ptr_new(coef)
  return,1b
end


pro axepoly__define
  _={AXEPOLY,$
     npar:0,$                   ;how many parameters are there
     numb:ptr_new(),$           ;number of terms per order
     coef:ptr_new() $           ;list of coefficients per order
    }
end
