;+
; AXECONFIG__DEFINE
;
; Master object to handle all operations involving the aXe
; configuration files, including trace, dispersion, flatfield.
;
; R. Ryan
;-





pro axeconfig::GetProperty,XRANGE=xrange,YRANGE=yrange,SENSUNIT=sensunit,$
                           SCIEXT=sciext,UNCEXT=uncext,DQAEXT=dqaext,$
                           NBEAM=nbeam,BEAMS=beams
  if arg_present(XRANGE)   then xrange=self.xrange
  if arg_present(YRANGE)   then yrange=self.yrange
  if arg_present(SENSUNIT) then sensunit=self.sensunit
  if arg_present(SCIEXT)   then sciext=self.science_ext
  if arg_present(UNCEXT)   then uncext=self.errors_ext
  if arg_present(DQAEXT)   then dqaext=self.dq_ext
  if arg_present(NBEAM)    then nbeam=self.nbeam
  if arg_present(BEAMS)    then beams=(*self.beams).beam

end


function axeconfig::has_beam,beam
  
  g=where(strmatch((*self.beams).beam,beam,/fold),n)
  case n of
     0: ok=0b
     1: ok=1b
     else: begin
        print,'AXECONFIG::HAS_BEAM> Multiple entries?!?!'
        ok=0b
     endelse
  endcase
  return,ok
end

  


function axeconfig::which_beam,BEAM=beam,ORDER=order
  ;+ 
  ; return the index for a  beam or order in the list of beams in the object
  ;-

  if primative_type(beam) eq 3 then begin
     g=where(strmatch((*self.beams).beam,beam,/fold),n)
     if n eq 0 then print,'AXECONFIG::WHICH_BEAM> Unable to find beam '+beam
     index=g[0]>0
     return,index
  endif
  if primative_type(order) eq 1 then begin
     g=where((*self.beams).order eq order,n)
     if n eq 0 then print,'AXECONFIG::WHICH_BEAM> Unable to find order '+$
                          strtrim(order,2)
     index=g[0]>0
     return,index
  endif

  ;default to the first order
  return,0
end



function axeconfig::dispersion,x,y,BEAM=beam,ORDER=order
  ;+
  ; return the dispersion in A/pix at (x,y)
  ;-


  ind=self->which_beam(BEAM=beam,ORDER=order)
  nx=n_elements(x)
  ny=n_elements(y)
  
  if nx ne ny then begin
     print,'AXECONFIG::DISPERSION> Uneven vectors. Only using first elements.'
     x=x[0] & nx=1
     y=y[0] & ny=1
  endif 
  
  ;build the input to poly2d
  xy=(nx eq 1)?[x,y]:transpose([[x],[y]])

  ;compute the parameters of the dispersion
  dldp=((*self.beams)[ind]).dldp->poly2d(xy)

  ;return only the slope
  disp=(nx eq 1)?dldp[1]:transpose(dldp[1,*])

  return,disp
end

pro axeconfig::expand,arr,nlast
  ;+
  ; extend an array with a trailing dimension (as nlast). This isn't
  ; really for AXE, but a more general operation.
  ;-
  sz=size(arr,/dim)
  arr=rebin(reform(arr,[sz,1],/over),[sz,nlast])
end


function axeconfig::xyd2xyg,xyd,lamb,DOUBLE=double,BEAM=beam,ORDER=order
  ;+
  ; transform a direct-image pixel to a dispersed-image pixel for some 
  ; wavelength (wavelength can be an array).  The direct image pixel 
  ; is assumed to be on the same grid as the dispersed image.
  ;-



  ind=self->which_beam(BEAM=beam,ORDER=order)

  ;get some dimensions
  sz=size(xyd,/dim)
  nvert=sz[1]
  nlamb=n_elements(lamb)

  ;outputs
  type=keyword_set(DOUBLE)?5:4
  xyg=make_array([sz,nlamb],type=type)



  ;get 2d poly coefficients
  dldp=((*self.beams)[ind]).dldp->poly2d(xyd,double=double)
  dydx=((*self.beams)[ind]).dydx->poly2d(xyd,double=double)
  xoff=((*self.beams)[ind]).xoff->poly2d(xyd,double=double)
  yoff=((*self.beams)[ind]).yoff->poly2d(xyd,double=double)

  ;now expand these for number of lambda steps
  self->expand,dldp,nlamb
  self->expand,dydx,nlamb
  self->expand,xoff,nlamb
  self->expand,yoff,nlamb

  ;subtract off the wavelength for inversion
  dldp[0,*,*]-=rebin(reform(lamb,1,nlamb),nvert,nlamb)

  ;just error check
  if ((*self.beams)[ind]).dldp->order() ne 1 || $
     ((*self.beams)[ind]).dydx->order() ne 1 then begin
     print,'AXEBEAM::XYD2XYG> Error'
     print,'  requires that the dispersion and trace are both'
     print,'  linear functions of arclength and x, respectively.'
     print,'  It would not be hard to fix this for the general'
     print,'  case, but that requires inverting a polynomial '
     print,'  (see fz_roots in IDL) and inverting the definition'
     print,'  of arclength (see volterra.pro that RER wrote).'

     print,'MUST GO TO AXEBEAM for the general solution'

     stop
  endif 

  ;--------------assume linear functions for speed---------------
  dldp=transpose(dldp)
  dydx=transpose(dydx)
  xoff=transpose(xoff)
  yoff=transpose(yoff)

  ;compute the arclength
  s=-dldp[*,*,0]/dldp[*,*,1]        ;hardcoded for LINEAR dispersion

  ;compute (xhat,yhat) pairs
  xhat=s/sqrt(1+dydx[*,*,1]^2)      ;hardcoded for LINEAR trace
  yhat=dydx[*,*,0]+dydx[*,*,1]*xhat ;hardcoded for LINEAR trace
  

  ;add back the various offsets (I don't think this is hardcoded for 
  ;any linear function assumptions)
  xyg[0,*,*]=transpose(xhat+xoff+rebin(xyd[0,*],nlamb,nvert))
  xyg[1,*,*]=transpose(yhat+yoff+rebin(xyd[1,*],nlamb,nvert))


  return,xyg  
end


function axeconfig::sensitivity,lamb,BEAM=beam,ORDER=order,SENSUNIT=sensunit,$
                                _EXTRA=_extra
  ;+
  ; interpolate the sensitivity curve onto a wavelength grid
  ;-

  if arg_present(SENSUNIT) then sensunit=self.sensunit

  i=self->which_beam(BEAM=beam,ORDER=order)

  s=interpol((*((*self.beams)[i]).sens).sensitivity,$
             (*((*self.beams)[i]).sens).wavelength,$
             lamb,_EXTRA=_extra)

  return,s
end


function axeconfig::flat,xy,l

  ;+
  ; evaluate the axe flat field cubes
  ;-

  ndim=size(xy,/n_dim)
  if ndim eq 0 then begin
     print,'AXECONFIG::FLAT> You must specify an (x,y) pair'
     return,!values.f_nan
  endif
  
  ;number of xy pairs
  nxy=(ndim eq 1)?1:(size(xy,/dim))[1]

  ;get dimensionalities
;  nx=n_elements(x)
;  ny=n_elements(y)
  nl=n_elements(l)
  
  ;some error checking
;  if nx eq 0 || ny eq 0 || nl eq 0 then begin
;     print,'AXECONFIG::FLAT> Must specify all: (x,y,l)'
;     return,-1
;  endif
;  if nx ne ny || nx ne nl then begin
;     print,'AXECONFIG::FLAT> inequal array lengths.  Error.'
;     return,-1
;  endif

  if nxy ne nl then begin
     print,'AXECONFIG::FLAT> number of xy pairs must match number of lambdas'
     return,!values.f_nan
  endif

  
  ;output datatype
  type=4                        ;4-float, 5-double

  ;process each type of flat field separately
  case self.gflat.type of
     'unity': val=make_array(nxy,value=1,type=type)
     'gray': begin
        ;make dummy output for polynomial flats
        val=make_array(nxy,value=0,type=type)

        ;assume value is zero for outside the frame
        g=where(xy[0,*] ge 0 and xy[0,*] le self.gflat.dim[0] and $
                xy[1,*] ge 0 and xy[1,*] le self.gflat.dim[1],n)
        
        ;only process the good ones
        if n ne 0 then begin
           ;compute pixel coordinates... Truncate to nearest pixel.
           ;could attempt to interpolate?
           ii=reform(floor(xy[0,g]),n)
           jj=reform(floor(xy[1,g]),n)
           
           ;evaluate the flat field
           val[g]=(*self.gflat.coef)[ii,jj]

        endif

     end

     'polynomial': begin

        ;make dummy output for polynomial flats
        val=make_array(nxy,value=0,type=type)

        ;assume value is zero for outside the frame
        g=where(xy[0,*] ge 0 and xy[0,*] le self.gflat.dim[0] and $
                xy[1,*] ge 0 and xy[1,*] le self.gflat.dim[1],n)
        if n ne 0 then begin

           ;dummy variables
           xx=1d0
           ll=(l[g]-self.gflat.wmin)/((self.gflat.wmax-self.gflat.wmin)>1.)

           ;compute pixel coordinates... Truncate to nearest pixel.
           ;could attempt to interpolate?
           ii=reform(floor(xy[0,g]),n)
           jj=reform(floor(xy[1,g]),n)
           
           ;sum over polynomial coefficients.  This is vectorized 
           ;in the number of pixels to compute, but explicitely loops
           ;over degree of polynomial.  
           for i=0,self.gflat.next-1 do begin
              val[g]+=(xx*((*self.gflat.coef)[*,*,i])[ii,jj])
              xx*=ll
           endfor

        endif
     end
     else: begin
        print,'AXECONFIG::FLAT> flat-field type not supported. returning unity'
        val=make_array(nxy,value=1,type=type)
     end
  endcase
  
  return,val
end


pro axeconfig::cleanup
  ;+
  ; Obligatory clean up object
  ;-

  ;destroy the beams
  for i=0,self.nbeam-1 do begin
     obj_destroy,(*self.beams)[i].xoff
     obj_destroy,(*self.beams)[i].yoff
     obj_destroy,(*self.beams)[i].dydx
     obj_destroy,(*self.beams)[i].dldp
     ptr_free,(*self.beams)[i].sens     
  endfor
  ptr_free,self.beams

  ;destroy the flat field data
  ptr_free,self.gflat.coef

end

function axeconfig::init,file,UNITY=unity,SENSUNIT=sensunit,SILENT=silent,$
                         _EXTRA=_extra
  ;+
  ; initialize the axeconfig object
  ;-

  if size(file,/type) ne 7 then begin
     print,'AXECONFIG::INIT> you must specify a file name'
     return,0b
  endif 
  if ~file_test(file) then begin
     print,'AXECONFIG::INIT> configfile not found.'
     return,0b
  endif

  ;read the config file
  conf=config_read(file,tags=conf_tags,_EXTRA=_extra)


  ;set the directory
  confdir=file_dirname(file)+path_sep()

  
  ;set the sensitivity units
  self.sensunit=(keyword_set(SENSUNIT)?sensunit:1d-17)
  
  ;record the config file name
  self.config_file=file

  ;read a whole bunch of parameters
  if tag_exist(conf,'instrument')  then self.instrument=conf.instrument
  if tag_exist(conf,'camera')      then self.camera=conf.camera
  if tag_exist(conf,'science_ext') then self.science_ext=conf.science_ext
  if tag_exist(conf,'errors_ext')  then self.errors_ext=conf.errors_ext
  if tag_exist(conf,'dq_ext')      then self.dq_ext=conf.dq_ext
  if tag_exist(conf,'dqmask')      then self.dqmask=conf.dqmask
  if tag_exist(conf,'rdnoise')     then self.rdnoise=conf.rdnoise
  if tag_exist(conf,'exptime')     then self.exptime=conf.exptime
  if tag_exist(conf,'pobjsize')    then self.pobjsize=conf.pobjsize
  if tag_exist(conf,'smfactor')    then self.smfactor=conf.smfactor
  if tag_exist(conf,'drzresola')   then self.drzresola=conf.drzresola
  if tag_exist(conf,'drzscale')    then self.drzscale=conf.drzscale
  if tag_exist(conf,'drzlamb0')    then self.drzlamb0=conf.drzlamb0
  if tag_exist(conf,'drzxini')     then self.drzxini=conf.drzxini
  if tag_exist(conf,'drzroot')     then self.drzroot=conf.drzroot
  if tag_exist(conf,'xrange')      then self.xrange=conf.xrange ;added by RER
  if tag_exist(conf,'yrange')      then self.yrange=conf.yrange ;added by RER

  
  ;deal with the flat field
  conf.ffname=confdir+conf.ffname
  if ~keyword_set(UNITY) && $
     tag_exist(conf,'ffname') && $
     file_test(conf.ffname) then begin
     
     ;save the name to the self
     self.gflat.file=conf.ffname       ;flatfield file name

     ;read the FITS information
     fits_open,conf.ffname,fcb
     
     ;sort out which type 
     filter=strcompress(sxpar(fcb.hmain,'FILTER',count=nfilter),/rem)

     ;based on the existence of the FILTER keyword, decide that it 
     ;is a direct image.  Print a warning message, lest we forget
     self.gflat.type=(nfilter eq 1)?'gray':'polynomial'

     ;initialize the various flat field types separately
     case self.gflat.type of
        'gray': begin
           print,'AXECONFIG::INIT> Using a gray flat.  Did you remove any '+$
                 'overscan regions?'
           img=readfits(conf.ffname,ext=1,silent=silent)
           self.gflat.dim=size(img,/dim)
           self.gflat.coef=ptr_new(img,/no_copy)          

        end
        'polynomial': begin
           ;get the dimensions from the first extensions           
           h=headfits(self.gflat.file)
           self.gflat.dim=sxpar(h,'NAXIS*')

           ;extract the min wavelength
           self.gflat.wmin=sxpar(fcb.hmain,'wmin',count=count)
           if count eq 0 then $
              print,'AXECONFIG::INIT> Warning, wmin missing from flatfield file'

           ;record the max wavelength
           self.gflat.wmax=sxpar(fcb.hmain,'wmax',count=count)
           if count eq 0 then $
              print,'AXECONFIG::INIT> Warning, wmax missing from flatfield file'

          ;record the number of extensions, (ie. the number of coefs)
           self.gflat.next=fcb.nextend

          ;make the output array
           coef=make_array([self.gflat.dim,self.gflat.next],type=4)
           for i=0,self.gflat.next-1 do $
              coef[*,*,i]=readfits(self.gflat.file,ext=i,silent=silent)
     
           ;record the parameters
           self.gflat.coef=ptr_new(coef,/no) 
        end

        else: stop
     endcase

     ;close the fits file
     fits_close,fcb
  endif else self.gflat.type='unity'


  ;find out which beams exist
  tags=tag_names(conf)
  g=where(strlen(tags) eq 5 and stregex(tags,'beam[a-z]',/fold) eq 0,nbeam)
  self.nbeam=nbeam

  ;if there are beams, process them
  if self.nbeam ne 0 then begin
     ;container for the beams
     beams=replicate({AXEBEAM},self.nbeam)
     for i=0,self.nbeam-1 do begin

        ;get the BEAM label
        beams[i].beam=strupcase(strmid(tags[g[i]],4,1))

        ;determine the order from the BEAM label
        case beams[i].beam of
           'A': beams[i].order=1
           'B': beams[i].order=0
           'C': beams[i].order=2
           'D': beams[i].order=3
           'E': beams[i].order=-1
           else: stop,'AXECONFIG::INIT> unable to initialize the order'
        endcase
        
        ;read the XOFF parameters
        beams[i].xoff=obj_new('axepoly',conf,'xoff',beams[i].beam)
        if ~obj_valid(beams[i].xoff) then begin
           print,'AXECONFIG::INIT> Cannot initialize xoff, beam='+beams[i].beam
           return,0b
        endif

        ;read the YOFF parameters
        beams[i].yoff=obj_new('axepoly',conf,'yoff',beams[i].beam)
        if ~obj_valid(beams[i].yoff) then begin
           print,'AXECONFIG::INIT> Cannot initialize yoff, beam='+beams[i].beam
           return,0b
        endif

        ;read the DYDX parameters
        beams[i].dydx=obj_new('axepoly',conf,'dydx',beams[i].beam)
        if ~obj_valid(beams[i].dydx) then begin
           print,'AXECONFIG::INIT> Cannot initialize dydx, beam='+beams[i].beam
           return,0b
        endif

        ;read the DLDP parameters
        beams[i].dldp=obj_new('axepoly',conf,'dldp',beams[i].beam)
        if ~obj_valid(beams[i].dldp) then begin
           print,'AXECONFIG::INIT> Cannot initialize dldp, beam='+beams[i].beam
           return,0b
        endif

        
        ;process the sensitivity
        gg=(where(strmatch(tags,'SENSITIVITY_'+beams[i].beam,/fold)))[0]
        if gg ne -1 then begin
           
           ;get the full path to the sensitvit 
           sensfile=confdir+conf.(gg)
           if file_test(sensfile) then begin

              ;read the fits file
              sens=mrdfits(sensfile,1,h,silent=silent)
              sens.sensitivity*=self.sensunit ;scale the sensitivity
              sens.error*=self.sensunit       ;scale the errors 
              
              ;save the data
              beams[i].sens=ptr_new(sens,/no)

           endif else $
              print,'AXECONFIG::INIT> Sensitivity file not found. ',file
        endif else $
           print,'AXECONFIG::INIT> Sensitivity entry missing from axeconfig'
        
     endfor

     ;save the beam data
     self.beams=ptr_new(beams,/no)
  endif else begin
     print,'AXECONFIG::INIT> No valid beams... Must not work.'
     return,0b
  endelse
  
  return,1b
end

pro axeflat__define             ;hold data for an aXe flatfield
  _={AXEFLAT,$
     file:'',$                  ;name of the flat-field file
     type:'',$                  ;type of the flat-field file
     wmin:0.,$                  ;min wavelength (A)
     wmax:0.,$                  ;max wavelength (A)
     next:0,$                   ;number of extensions
     dim:[0l,0l],$              ;dimensionality of the image
     coef:ptr_new() $           ;coeffiecents
    }
end
     
pro axebeam__define             ;hold data for a single AXEBEAM
  _={AXEBEAM,$
     beam:'',$                  ;beam label (A, B, C, ...)
     order:0,$                  ;dispersion order (1, 0, 2,...)
     xoff:obj_new(),$           ;axepoly object for x_offset
     yoff:obj_new(),$           ;axepoly object for y_offset
     dydx:obj_new(),$           ;axepoly object for trace
     dldp:obj_new(),$           ;axepoly object for dispersion
     sens:ptr_new() $           ;container for sensitivity curve
    }
end


pro axeconfig__define
  _={AXECONFIG,$
     config_file:'',$
     instrument:'',$
     camera:'',$
     science_ext:'',$
     dq_ext:'',$
     errors_ext:'',$
     dqmask:0ul,$
     rdnoise:0.,$
     exptime:'',$
     pobjsize:0.,$
     smfactor:0.,$
     drzresola:0.,$
     drzscale:0.,$
     drzlamb0:0.,$
     drzxini:0.,$
     drzroot:'',$
     xrange:[0.,0.],$           ;added by RER
     yrange:[0.,0.],$           ;added by RER
     gflat:{AXEFLAT},$
     sensunit:0d,$
     nbeam:0,$
     beams:ptr_new() $
    }
end
