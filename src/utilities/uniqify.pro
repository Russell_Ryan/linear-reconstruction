;+
; Return only the uniq values of an array (array must be a sortable 
; type: int, flt, etc.)
;-

function uniqify,ind,COUNT=count,IND_UNIQ=ind_uniq

  ind_uniq=uniq(ind,sort(ind))  ;indices of uniq values
  i_q=ind[ind_uniq]             ;uniq values
  if arg_present(COUNT) then count=n_elements(i_q)
  return,i_q
end
