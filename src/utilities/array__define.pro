

function array::n_elements
;+
; return number of elements currently in the array
;-
  return,self.count
end

function array::isempty
;+
; Test if the array is currently empty
;-
  return,~ptr_valid(self.data)
end

function array::type
;+
; return the data type of the array
;-
  return,self.type
end



pro array::copy,obj,FLUSH=flush
;+
; copy this array into another array, optionally flush this array
;-
  if ~obj_valid(obj) || ~obj_isa(obj,obj_class(self)) then return

  obj->extract,a
  self->push,a,/no_copy
  if keyword_set(FLUSH) then obj->flush

end



pro array::extract,arr,COUNT=count,FLUSH=flush
;+
; extract the data from this array. optionally return the number of 
; elements and/or flush it from memory
;-
  count=self.count
  if self->isempty() then begin
     arr=-1
  endif else begin
     arr=(*self.data)[0:self.count-1]
  endelse
  if keyword_set(FLUSH) then self->flush
end





pro array::push,data,NO_COPY=no_copy
;+
; append the input "data" onto the end of the array. optionally use 
; /no_copy to not copy the data (see ptr_new() for more details)
;
; From talking to Sean L, I could add another optional input to 
; govern *WHICH* element the data gets copied in to
;-

  catch,error
  if error ne 0 then begin
     print,'ARRAY::PUSH> Error code: '+strtrim(error,2)
     print,'ARRAY::PUSH> Error message: '+!error_state.msg
     CATCH, /CANCEL
     stop
     return
  endif


  sz=size(data)
  nn=n_elements(sz)
  num=ulong64(sz[nn-1])         ;uptype for issues with integer math
  type=sz[nn-2]
  
  ;check that the types are able to be concatenated
  if self.type ne 0 && self.type ne type then begin
     r=scope_traceback(/structure)
     r=r[n_elements(r)-1]
     print,r.routine+'> Cannot push data, mismatched data types.'
     return
  endif


  ;check if initialized?
  if self->isempty() then begin
     ;must initialize
     self.count=num
     self.type=type
     self.length=n_elements(data)
     self.data=ptr_new(data,NO_COPY=no_copy)
  endif else begin  
     ;is intialized

     ;grow the array
     self->expand,num
  
     ;put the new data in the array
     (*self.data)[self.count]=keyword_set(NO_COPY)?temporary(data):data

     ;update the number
     self.count+=num

  endelse

end

pro array::expand,n
;+
; Grow the array to accommodate a new size
;-

  if (self.count+n) lt self.length then return
  
;  i=ceil(alog(double(n+self.count)/double(self.length))/alog(2.))

  
  ;grow the size by some amount (here is double)
  while (self.count+n) ge self.length do self.length*=2

  ;create a larger array
  tmp=replicate((*self.data)[0],self.length)

  ;replace the data
  tmp[0]=*self.data
  *self.data=temporary(tmp)

end
  



pro array::flush,DATA=data,COUNT=count
;+
; Flush the contents.  wipe data, reset counters.
;-

  if arg_present(DATA) then self->extract,data,COUNT=count
  ptr_free,self.data
  self.count=0ull
  self.type=0
  self.length=0ull
end
  

pro array::cleanup,DATA=data,COUNT=count
;+
; destroy the object, with option to extract the data
;-

  if arg_present(DATA) then begin
     self->extract,data,/flush,COUNT=count  
  endif else begin
     self->flush
  endelse
end

function array::init,data,NO_COPY=no_copy
;+
; build the object, optionally set the size of the initial array
;-
  if n_elements(data) ne 0 then self->push,data,NO_COPY=no_copy
  return,1b
end



pro array__define
;+
; object prototype
;-
  _={ARRAY,$
     data:ptr_new(),$           ;the data
     count:0ull,$               ;number of non-null elements in the data
     type:0,$                   ;the IDL type of the data
     length:0ull $              ;number of elements in data
    }
end
