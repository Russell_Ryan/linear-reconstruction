;+
; SPAREMATRIX__DEFINE
; 
; Description:
;   An IDL Object to do all sparse matrix operations, mostly just 
;   matrix products of the form: x= A.u and y=A^T.v.  Uses a companion
;   C-code to make the products fast (could consider a GPU?), but will
;   default to an IDL version if C cannot be compiled.  Also can
;   read/write *txt files to save the matrix.
;
; Dependencies:
;   C-code: dotproduct.c
;
; Author:
;   R. Ryan & S. Casertano
;
;-



;---- MATRIX DIMENSION UTILITIES ---
function sparsematrix::n_elements
  return,self.n
end

pro sparsematrix::shape,m,n
  m=self.sz[0]
  n=self.sz[1]
end

pro sparsematrix::to_matrix,mat
  mat=fltarr(self.sz)
  mat[*self.i,*self.j]=*self.aij
end



;---- FILE I/O UTILITIES ----
pro sparsematrix::save,file,COMPRESS=compress
  openw,lun,file,/get_lun,error=error,COMPRESS=compress
  if error ne 0 then begin
     print,'SPARSEMATRIX::SAVE> Unable to open file: '+file
     return
  endif
  printf,lun,self.n,self.sz,self.itype,self.ftype  
  printf,lun,*self.i,*self.j,*self.aij
  free_lun,lun
end

pro sparsematrix::read,file,COMPRESS=compress
  n=self.n
  sz=self.sz
  itype=self.itype
  ftype=self.ftype
  openr,lun,file,/get_lun,error=error,COMPRESS=compress
  if error ne 0 then begin
     print,'SPARSEMATRIX::READ> Unable to open file: '+file
     return
  endif
  readf,lun,n,sz,itype,ftype
  *self.i=make_array(n,type=itype)
  *self.j=make_array(n,type=itype)
  *self.aij=make_array(n,type=ftype)
  readf,lun,*self.i,*self.j,*self.aij
  free_lun,lun

  if itype ne self.itype then begin
     *self.i=fix(*self.i,type=self.itype)
     *self.j=fix(*self.i,type=self.itype)
  endif
  if ftype ne self.ftype then *self.aij=fix(*self.aij,type=self.ftype)
end
  

;--- MATRIX PRODUCT UTILITIES ----
function sparsematrix::dotproduct,u,type,n
  vv=make_array(n,type=self.ftype)
  if self.compiled then begin
     err=call_external(self.dll_path,type,self.n,self.sz[0],self.sz[1],$
                       *self.i,*self.j,*self.aij,fix(u,type=self.ftype),vv,$
                       value=[1b,1b,1b,0b,0b,0b,0b,0b])
     if err ne 0 && self.verbose then $
        print,'SPARSEMATRIX::PRODUCT> C-code has error CODE='+strtrim(err,2)
  endif else begin
     stop,'NO IDL IMPLEMENTATION.'
  endelse

  return,vv
end


function sparsematrix::lproduct,u
  if n_elements(u) ne self.sz[1] then begin
     if self.verbose then $
        print,'SPARSEMATRIX::LPRODUCT> error: incompatible dimensions'
     return,fix(!values.f_nan,type=self.ftype)
  endif
  v=self->dotproduct(u,'lproduct',self.sz[0])
  return,v
end
function sparsematrix::rproduct,v
  if n_elements(v) ne self.sz[0] then begin
     if self.verbose then $
        print,'SPARSEMATRIX::RPRODUCT> error: incompatible dimensions'
     return,fix(!values.f_nan,type=self.ftype)
  endif
  u=self->dotproduct(v,'rproduct',self.sz[1])
  return,u
end



;pro sparsematrix::append,ii,jj,aa
;end


;--- OBLIGATORY HOUSE KEEPING UTILITIES ----
pro sparsematrix::cleanup
  ptr_free,self.i,self.j,self.aij
end
function sparsematrix::init,i,j,aij,sz,SILENT=silent,$
                             NO_COMPILED=no_compiled,$
                             NO_COPY=no_copy

  self.verbose=~keyword_set(SILENT)

  ;check inputs
  if n_params() lt 4 then begin
     print,'obj=obj_new("sparsematrix",i,j,aij,sz,/SILENT,/NO_COPY)'
     return,0b
  endif

  ;check dimensionalities
  ni=n_elements(i)
  nj=n_elements(j)
  na=n_elements(aij)
  ns=n_elements(sz)
  ok=(ni eq nj) && (ni eq na) && (ni ne 0) && (ns eq 2)
  if ~ok then begin
     if self.verbose then $
        print,'SPARSEMATRIX::INIT> Error: Invalid triplets.',ni,nj,na
     return,0b
  endif

  ;C-code version
  vers_required=1


  ;set some defaults
  self.itype=15                 ;store index types (ULONG64)
  self.ftype=5                  ;store element types (DOUBLE)
  self.compiled=~keyword_set(NO_COMPILED)
  

  ;compile the code if necessary
  if self.compiled then begin
     ;get path to C code
     st=scope_traceback(/structure)
     st=st[n_elements(st)-1]
     path=file_dirname(st.filename)

     ;set C-compiler flags
     flags=!VERSION.OS_FAMILY eq 'unix'?'-O2':''
     
     ;compile the C-code
     make_dll,'dotproduct',['lproduct','rproduct','version'],$
              input_directory=path,extra_cflags=flags,dll_path=dll_path
     self.dll_path=dll_path
     
     ;test the C code
     vers=call_external(self.dll_path,'version')
     if vers ne vers_required then begin
        self.compiled=0b
        if self.verbose then print,'C cannot be initialized, defaulting IDL.'
     endif
  endif

  ;print a warning message
  if ~self.compiled then begin
     print,'OOPS... there is no IDL Default as of yet.  This code will not '
     print,'         work for you!  Exitting.'
     return,0b
  endif


  ;get numbers
  self.n=ni
  self.sz=sz


  ;store the arrays
  self.i=ptr_new(fix(i,type=self.itype),NO_COPY=no_copy)
  self.j=ptr_new(fix(j,type=self.itype),NO_COPY=no_copy)
  self.aij=ptr_new(fix(aij,type=self.ftype),NO_COPY=no_copy)
  
  return,1b
end
  

pro sparsematrix__define
  _={SPARSEMATRIX,$
     itype:0,$                  ;IDL type of the integers
     ftype:0,$                  ;IDL type of the floats
     compiled:0b,$              ;flag to use compiled code
     dll_path:'',$              ;path the the compiled code
     verbose:0b,$               ;flag to print info
     n:0ull,$                   ;number of non-zero elements
     sz:[0uL,0uL],$             ;dimensionality of matrix
     i:ptr_new(),$              ;i-indices (of non-zero elements)
     j:ptr_new(),$              ;j-indices (of non-zero elements)
     aij:ptr_new() $            ;matrix elements (by definition, nonzero)
    }
end
     
