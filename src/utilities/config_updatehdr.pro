pro config_updatehdr,conf,hdr
  
  tags=tag_names(conf)

  for i=0,n_elements(tags)-1 do begin
     if strlen(tags[i]) gt 8 then begin
        print,'CONFIG_UPDATEHDR> Warning: keyword ('+tags[i]+') '+$
              'is longer than 8 characters.'
        print,'                  Does not conform to fits standard.'
     endif


     n=n_elements(conf.(i))
     val=(n eq 1)?conf.(i):strjoin(strtrim(conf.(i),2),',')

     sxaddpar,hdr,tags[i],val,' CONFIGURATION'
  endfor


end
