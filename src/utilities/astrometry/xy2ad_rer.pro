;+
; compute (RA,Dec) from (x,y) and an astrometry.
; 
; Use the forward SIP (A, B) matrices if present.
;
; Key difference between this and xy2ad from astrolib, is that I
; permit (A, B) matrices to be different size.
;-

pro xy2ad_rer,x,y,ast,a,d
  
  ;dump the astrometry
  cd=ast.cd
  cdelt=ast.cdelt
  crpix=ast.crpix-1             ;to follow IDL pixel(0,0) convention
  ctype = ast.ctype
  crval = ast.crval
  if tag_exist(ast,'PV2')      then pv2=ast.pv2
  if tag_exist(ast,'LONGPOLE') then longpole=ast.longpole
  if tag_exist(ast,'LATPOLE')  then latpole=ast.latpole


  ;update the CD matrix
  if cdelt[0] ne 1. then begin
     cd[0,0] *= cdelt[0]
     cd[0,1] *= cdelt[0]
  endif 
  if cdelt[1] ne 1. then begin
     cd[1,1] *= cdelt[1]
     cd[1,0] *= cdelt[1]
  endif

  ;compute differences wrt the CRPIX
  dx=x-crpix[0]
  dy=y-crpix[1]

  ;apply the distortion
  if tag_exist(ast,'DISTORT') && strcmp(ast.distort.name,'SIP') then begin

     applysip,ast.distort.a,ast.distort.b,dx,dy

;
;     ;dump the matrices
;     a=ast.distort.a
;     b=ast.distort.b
;
;     ;get the dimensionality
;     na=(size(a,/dim))[0]
;     nb=(size(b,/dim))[0];
;
;     if na ne 1 && nb ne 1 then begin
;
;        ;save the deltas
;        dxx=dx
;        dyy=dy
;
;        ;key difference between this and astrolib, is that I allow for 
;        ;different size matrices between A and B!
;        for i=0,na-1 do begin
;           for j=0,na-1 do begin
;              if i+j gt 1 && i+j lt na then dxx+=(dx^i*dy^j*a[i,j])
;           endfor
;        endfor
;        
;        for i=0,nb-1 do begin
;           for j=0,nb-1 do begin
;              if i+j gt 1 && i+j lt nb then dyy+=(dx^i*dy^j*b[i,j])
;           endfor
;        endfor
;        
;
;
;        ;update the differences
;        dx=temporary(dxx)
;        dy=temporary(dyy)
;     endif else begin
;
;        stop,'INVALID SIZED MATRICES'
;     endelse

  endif


  ;transform to angles
  xsi = cd[0,0]*dx + cd[0,1]*dy
  eta = cd[1,0]*dx + cd[1,1]*dy

  ;apply map
  wcsxy2sph,xsi,eta,a,d,ctype=ctype,crval=crval,$
            pv2=pv2,longpole=longpole,latpole=latpole
  

end
