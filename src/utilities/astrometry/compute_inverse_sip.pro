;+
; suite of routines to compute inverse SIP matrices from the forward
; matrices.  this is not a precise transform, but for WFC3/IR this
; seems to be off by ~1e-5 pixels.  It also computes error in
; transform as a 
;
; Notes:
;    Defines a common block called cis_data
;
;-

function cis_matrix,p,order,dim
  ;build a SIP matrix
  ii=0
  m=dblarr(order+1,order+1)
  for i=0,order do begin
     for j=0,order-i do begin
        if i+j ge 2 then m[i,j]=p[ii++]/(double(dim[0])^i*double(dim[1])^j)
     endfor
  endfor
  return,m
end
function cis_astrometry,ast,p,order,npar
  ;build a new astrometry structure from an old one
  new=ast
  ap=p[0:npar[0]-1]
  bp=p[npar[0]:npar[0]+npar[1]-1]
  
  new.distort.ap=cis_matrix(ap,order[0],ast.naxis)
  new.distort.bp=cis_matrix(bp,order[0],ast.naxis)
  return,new
end


function cis_gof,p,dp
  ;compute the variance between old and new points
 
  common cis_data,ra,dec,x,y,ast,nobj,order,npar

  new=cis_astrometry(ast,p,order,npar)
  ad2xy_rer,ra,dec,new,xx,yy

  dx=x-xx
  dy=y-yy

  var=total(dx*dx+dy*dy)/(nobj-total(npar))
  gof=sqrt(var)


  return,gof
end




function compute_inverse_sip,ast,NAP=nap,NBP=nbp,RANDOM=random,NSAMPLE=nsample,$
                             RMS=rms
  common cis_data,ra,dec,x,y,new,nobj,order,npar



  t0=systime(/sec)
  
  ;set defaults (as one more order higher for inverse)
  if ~keyword_set(NAP) then nap=(size(ast.distort.a,/dim))[0]
  if ~keyword_set(NBP) then nbp=(size(ast.distort.b,/dim))[0]
  nobj=keyword_set(NSAMPLE)?nsample:500
  
  ;number of terms in the INVERSE SIP
  napar=(nap+4)*(nap-1)/2
  nbpar=(nbp+4)*(nbp-1)/2
  
  ;build a distortion structure with INVERSE SIP
  distort={name:'SIP',a:ast.distort.a,b:ast.distort.b,$
           ap:dblarr(nap+1,nap+1),bp:dblarr(nbp+1,nbp+1)}


  ;build a new astrometry structure
  tags=tag_names(ast)
  new=create_struct('distort',distort)
  g=where(~strmatch(tags,'distort',/fold),n)
  for i=0,n-1 do new=create_struct(new,tags[g[i]],ast.(g[i]))

  
  ;set sampling
  if keyword_set(RANDOM) then begin
     ;random sampling
     x=randomu(seed,nobj,/double)*new.naxis[0]
     y=randomu(seed,nobj,/double)*new.naxis[1]
  endif else begin
     ;uniform sampling
     delta=sqrt(product(new.naxis)/nobj)
     nx=floor(new.naxis[0]/delta)
     ny=nobj/nx
     x=rebin(reform((dindgen(nx)+0.5)*delta,nx,1),nx,ny)
     y=rebin(reform((dindgen(ny)+0.5)*delta,1,ny),nx,ny)
  endelse

  ;convert to RA/Dec
  xy2ad_rer,x,y,ast,ra,dec


  ;build some more inputs
  order=[nap,nbp]
  npar=[napar,nbpar]


  ;call TNMIN
;  fa={ra:ra,dec:dec,x:x,y:y,ast:new,nobj:nobj,order:order,npar:npar}
;  pi=replicate({value:0d0},total(npar))
;  p=tnmin('cis_gof',parinfo=pi,/auto,/quiet,bestmin=bestmin)


  ;call POWELL
  p=dblarr(total(npar))
  xi=diag_matrix(replicate(1.,total(npar)))
  powell,p,xi,1e-5,bestmin,'cis_gof',/double,itmax=1000

  ;adjust the output
  rms=sqrt(bestmin)
  new=cis_astrometry(new,p,order,npar)
  t1=systime(/sec)
  runtime=t1-t0

  
  ;make diagnostic plot
;  window,0,retain=2,xsize=500,ysize=500  
;  ad2xy,ra,dec,new,xx,yy
;  plot,x-xx,y-yy,ps=2

  return,new

end
