;+
; transform one (x,y) pair with one astrometry structure to
; another (x,y) pair with a different astrometry structure
;
; Nota bene.  the problem is xyxy in astrolib uses headers, but
; I don't like all the baggage associated with working with headers.
; and the xy2ad and ad2xy use astrometries.  There is no associated
; xy2xy in astrolib, so this is it.
;
;-

pro xy2xy,x1,y1,ast1,x2,y2,ast2
  xy2ad_rer,x1,y1,ast1,a,d
  ad2xy_rer,temporary(a),temporary(d),ast2,x2,y2
end

     
