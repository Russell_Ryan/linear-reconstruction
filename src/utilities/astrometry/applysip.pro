pro applysip,a,b,dx,dy
  ;get the dimensionality
  na=(size(a,/dim))[0]
  nb=(size(b,/dim))[0]

  if na eq 1 || nb eq 1 then return
  
  ;save the deltas
  dxx=dx
  dyy=dy
  
  ;key difference between this and astrolib, is that I allow for 
  ;different size matrices between A and B!
  for i=0,na-1 do begin
     for j=0,na-1 do begin
        if i+j gt 1 && i+j lt na then dxx+=(dx^i*dy^j*a[i,j])
     endfor
  endfor
  
  for i=0,nb-1 do begin
     for j=0,nb-1 do begin
        if i+j gt 1 && i+j lt nb then dyy+=(dx^i*dy^j*b[i,j])
     endfor
  endfor
  
  ;overwrite the input
  dx=temporary(dxx)
  dy=temporary(dyy)

end
