function ad2xy_rer_mpfit,xy,cd=cd,crpix=crpix,ctype=ctype,crval=crval,$
                         pv2=pv2,longpole=longpole,latpole=latpole,$
                         distort=distort,ra=ra,dec=dec,cosd=cosd,pix=pix

  dx=xy[0]-crpix[0]
  dy=xy[1]-crpix[1]

  applysip,distort.a,distort.b,dx,dy
  
  ;transform to angles
  xsi = cd[0,0]*dx + cd[0,1]*dy
  eta = cd[1,0]*dx + cd[1,1]*dy

  ;apply map
  wcsxy2sph,xsi,eta,a,d,ctype=ctype,crval=crval,$
            pv2=pv2,longpole=longpole,latpole=latpole
  
  ;compute residual
  res=[(ra-a)*cosd,dec-d]/pix
  return,res
end


pro ad2xy_rer,a,d,ast,x,y

  ;dump the astrometry
  cd=ast.cd
  cdelt=ast.cdelt
  crpix=ast.crpix-1             ;follow IDL (0,0) pixel definition
  ctype = ast.ctype
  crval = ast.crval
  if tag_exist(ast,'PV2')      then pv2=ast.pv2
  if tag_exist(ast,'LONGPOLE') then longpole=ast.longpole
  if tag_exist(ast,'LATPOLE')  then latpole=ast.latpole

  ;update the CD matrix
  if cdelt[0] ne 1. then begin
     cd[0,0] *= cdelt[0]
     cd[0,1] *= cdelt[0]
  endif 
  if cdelt[1] ne 1. then begin
     cd[1,1] *= cdelt[1]
     cd[1,0] *= cdelt[1]
  endif
  
  ;invert CD matrix
  cdinv = invert(cd,stat,/double)

  ;compute deltas without distortion
  wcssph2xy,a,d,xsi,eta,ctype=ctype,crval=crval,$
            pv2=pv2,latpole=latpole,longpole=longpole

  ;invert the CD matrix
  dx = ( cdinv[0,0]*xsi + cdinv[0,1]*eta  )
  dy = ( cdinv[1,0]*xsi + cdinv[1,1]*eta  )
  

  if tag_exist(ast,'DISTORT') && strcmp(ast.distort.name,'SIP') then begin
     ;has distortion model
     print,'AD2XY_RER> Using inverse model'

     ;get size of the distortion coefs
     nap=(size(ast.distort.ap,/dim))[0]
     nbp=(size(ast.distort.bp,/dim))[0]


     if nap ne 1 || nbp ne 1 then begin
        ;have distortion and a reverse model

        ;apply the distortion
        applysip,ast.distort.ap,ast.distort.bp,dx,dy

        ;final coordinates
        x=dx+crpix[0]
        y=dy+crpix[1]
     endif else begin
        na=(size(ast.distort.a,/dim))[0]
        nb=(size(ast.distort.b,/dim))[0]
        if na ne 1 || nb ne 1 then begin
           print,'AD2XY_RER> Iterating with forward model'

           ;have distortion but only forward, therefore must iterate
           n=n_elements(a)

           ;get pixel scale
           getrot,ast,pix
           pix=abs(pix)

           pi=replicate({value:0d,step:1d-2},2)
           fa={cd:cd,crpix:crpix,ctype:ctype,crval:crval,$
               pv2:pv2,longpole:longpole,latpole:latpole,$
               distort:distort,pix:pix,ra:0d,dec:0d,cosd:0d}
           for i=0,n-1 do begin
              fa.ra=a[i]
              fa.dec=d[i]
              fa.cosd=cos(d[i]*!PI/180)
              pi.value=[x[i],y[i]]+crpix
              xy=mpfit('ad2xy_rer_mpfit',functargs=fa,parinfo=pi,$
                       xtol=1e-3,bestnorm=best,/quiet)
              x[i]=xy[0]
              y[i]=xy[1]              
           endfor
        endif else begin
           print,'AD2XY_RER> something amiss with the distortion model'
           x+=crpix[0]
           y+=crpix[1]
        endelse
     endelse
  endif 

end



;     ;get the dimensionality
;     na=(size(a,/dim))[0]
;     nb=(size(b,/dim))[0]
;
;     if na ne 1 && nb ne 1 then begin
;
;        ;save the deltas
;        dxx=dx
;        dyy=dy
;
