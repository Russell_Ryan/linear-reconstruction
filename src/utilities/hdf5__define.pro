;+
;
; HDF5__DEFINE
; 
; An object to access simple HDF5 files (simple in that there are no 
; hierarchies with HDF groups).  This juggles all datatypes and 
; dataspaces.  It can write attributes to both the main file and to 
; the individual datasets.  
;
; This can read/write, but only one at a time (since there does not
; seem to be an easy way to update/append to an existing HDF5 file).
; 
; R. Ryan
;
;-


pro hdf5::GetProperty,FILE=file,MODE=mode,SILENT=silent
  ;+
  ; Method to extract state information from the HDF5 file
  ;-
  if arg_present(FILE) then file=self.file
  if arg_present(MODE) then mode=self.mode
  if arg_present(FID) then fid=self.fid
  if arg_present(SILENT) then silent=~self.verbose
end
pro hdf5::SetProperty,SILENT=silent
  ;+
  ; Method to change the state
  ;-
  if n_elements(silent) ne 0 then self.verbose=~keyword_set(SILENT)
end

;------- Miscellaneous actions -----
function hdf5::N_objects
  return,h5g_get_num_objs(self.fid)
end

function hdf5::get_object,i,OKAY=okay
  catch,err
  if err ne 0 then begin
     print,'HDF5::Get_Object> Unable to get object '+strtrim(i,2)+$
           ' in file '+self.file
     catch,/cancel
     okay=0b
     return,''
  endif
  
  name=h5g_get_obj_name_by_idx(self.fid,i)
  okay=1b
  return,name
end



;------ READING FUNCTIONS -----

pro hdf5::read_attribute,id,_REF_EXTRA=_ref_extra
  ;+
  ; Read an attribute from an HDF file.
  ; 
  ; ID is the optional dataset indicator.  if blank, then uses the 
  ; main file ID
  ;
  ; Keywords are the full keyword indicator for the keyword
  ; 
  ; EXAMPLE:
  ;    hdf5->read_attribute,KEYWORd=keyword
  ;
  ;-

  if self.mode ne 'read' then begin
     if self.verbose then $
        print,'HDF5::read_attribute> read attributes in read mode only.'
     return
  endif

  if size(id,/type) ne 3 then begin
     level=0
     id=self.fid
  endif else level=-1
  if id le 0 then begin
     if self.verbose then print,'HDF5::Read_Attribute> Dataid must be > 0'
     return
  endif

  for i=0,n_elements(_ref_extra)-1 do begin
     aa=h5a_open_name(id,_ref_extra[i])
     a=h5a_read(aa)
     (scope_varfetch(_ref_extra[i],/ref_extra,level=level))=a
  endfor

end

function hdf5::read_data,name,_REF_EXTRA=_ref_extra
  ;+
  ; Read a dataset from the HDF file.
  ; 
  ; The optional keywords read the attributes from that dataset
  ;
  ;-

 
  catch,error
  if error ne 0 then begin
     if self.verbose then begin
        print,'HDF5::READ_DATA> Error code:',error
        print,!error_state.msg
     endif
     catch,/cancel
     return,-1
  endif

  if self.mode ne 'read' then begin
     if self.verbose then $
        print,'HDF5::READ_DATA> Can only read data when mode="read".'
     return,-1
  endif
  if self.fid eq 0 then begin
     if self.verbose then print,'HDF5::READ_DATA> No file is open.'
     return,-1
  endif
  
  type=primative_type(name,/typename)
  case type of
     'INTEGER': name=self->get_object(name)
     'STRING':
     else: begin
        if self.verbose then print,'HDF5::READ_DATA> name must be a string.'
        return,-1
     end
  endcase


  did=h5d_open(self.fid,name)
  tid=h5d_get_type(did)

  t5=systime(/sec)
  data=h5d_read(did,tid)
  t6=systime(/sec)

  
  self->read_attribute,did,_ref_extra=_ref_extra
  h5t_close,tid
  h5d_close,did


  print,'time in hdf5:',t6-t5,(t6-t5)/n_elements(data)

  
  return,data
end

;------ WRITING FUNCTIONS -------
pro hdf5::add_attribute,dataset,_EXTRA=_extra
  ;+
  ; Method to add an attribute to the dataset. 
  ; 
  ; If dataset is missing, then it will add attributes to the main
  ;
  ;-

  if n_tags(_extra) eq 0 then return

  if self.mode eq 'read' then begin
     if self.verbose then $
        print,'HDF5::Add_Attribute> add attributes in write/update mode only.'
     return
  endif

  if size(dataset,/type) ne 3 then dataset=self.fid
  if dataset eq 0 then begin
     if self.verbose then print,'HDF5::ADD_ATTRIBUTE> dataset ID is not valid.'
     return
  endif



  tags=tag_names(_extra)
  for i=0,n_elements(tags)-1 do begin
     at=h5t_idl_create((_extra.(i))[0])
     dim=size(_extra.(i),/dim)>1
     if n_elements(dim) eq 1 then dim=dim[0]
     as=h5s_create_simple(dim)
     a=h5a_create(dataset,tags[i],at,as)
     h5a_write,a,_extra.(i)
     h5a_close,a
     h5t_close,at
     h5s_close,as
  endfor

end


pro hdf5::add_data,name,data,CHUNK=chunk,_EXTRA=_extra
  ;+
  ; Add dataset to an HDF file
  ; 
  ; name is a string name of the dataset
  ; data is the dataset, can be any numeric type(maybe pointer)
  ;
  ; _EXTRA keywords set attributes to the dataset
  ;-

  

  if self.mode eq 'read' then begin
     if self.verbose then $
        print,'HDF5::ADD_DATA> Can only add data when mode="write" or "update".'
     stop
     return
  endif
  if self.fid eq 0 then begin
     if self.verbose then print,'HDF5::ADD_DATA> No file is open.'
     return
  endif
  if n_params() lt 2 then begin
     if self.verbose then print,'pro hdf5::add_data,name,data,_EXTRA=_extra'
     return
  endif
  

  ;create the dataset
  tid=h5t_idl_create(data[0])
  sid=h5s_create_simple(size(data,/dim)>1)
  did=h5d_create(self.fid,name,tid,sid,chunk=chunk)

  ;add attributes from _extra
  self->Add_Attribute,did,_EXTRA=_extra

  ;write the dataset
  h5d_write,did,data

  ;close the data
  h5d_close,did
  h5t_close,tid
  h5s_close,sid
end




;---- OBLIGATORY FUNCTIONS ------
pro hdf5::cleanup
  h5f_close,self.fid
end


function hdf5::init,file,READ=read,WRITE=write,UPDATE=update,$
                    SILENT=silent,_EXTRA=_extra
  ;+
  ; initialize the object. 
  ;
  ; must give file name.
  ; _EXTRA keywords are set as attributes.  Cannot read attributes
  ;        during the initlization phase.
  ;
  ;-

  self.mode='write'
  if keyword_set(READ) then self.mode='read'
  if keyword_set(WRITE) then self.mode='write'
  if keyword_set(UPDATE) then self.mode='update'

  self.file=file
  self.verbose=~keyword_set(SILENT)
  
  case self.mode of
     'read': begin
        
        if ~file_test(self.file) then begin
           if self.verbose then print,'HDF5::INIT> Unable to find file.'
           return,0b
        endif
        if ~h5f_is_hdf5(self.file) then return,0b

        self.fid=h5f_open(self.file)

     end
     'write': begin
        openw,lun,file,/get_lun,error=error
        if error ne 0 then begin
           if self.verbose then print,'HDF5::INIT> Cannot write file.'
           return,0b
        endif
        free_lun,lun
        self.fid=h5f_create(self.file)
        
        
        ;add attributes here
        self->Add_attribute,self.fid,_extra=_extra

     end
     'update': begin
        if ~file_test(self.file) then begin
           openw,lun,self.file,/get_lun,error=error
           if error ne 0 then begin
              if self.verbose then print,'HDF5::INIT> Cannot open file.'
              return,0b
           endif
           if self.verbose then print,'HDF5::INIT> update=read'
           free_lun,lun
           self.fid=h5f_create(self.file)
        endif else begin
           if ~h5f_is_hdf5(self.file) then return,0b
           self.fid=h5f_open(self.file,/write)
        endelse
     end


     else: begin
        print,'HDF5::INIT> Mode not recognized.'
        return,0b
     endelse
  endcase
  return,1b
end
  


pro hdf5__define
  _={HDF5,$
     file:'',$                  ;name of the HDF5 file
     mode:'',$                  ;mode of the file (read or write)
     verbose:0b,$               ;do we print messages to the screen
     fid:0L $                   ;the file identifier
    }
end
