#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <limits.h>


// some types for me
typedef int IND;
typedef double VAL;
typedef struct {
  VAL damp;
  VAL atol;
  VAL btol;
  VAL conlim;
  IND itmax;
  char silent;
} lsqr_control;
typedef struct {
  VAL r1norm;
  VAL r2norm;
  VAL anorm;
  VAL acond;
  VAL arnorm;
  VAL xnorm;
  IND iter;
} lsqr_outputs;


// function Declarations 
void lsqr_normalize(IND,VAL*,VAL);
void lsqr_linear(IND,VAL*,VAL,VAL*);
void lsqr_rproduct(IND,IND,IND*,IND*,VAL*,VAL*,VAL*);
void lsqr_lproduct(IND,IND,IND*,IND*,VAL*,VAL*,VAL*);
VAL lsqrt_sign(VAL);
void lsqr_ortho(VAL,VAL,VAL*,VAL*,VAL*);
VAL lsqr_norm(IND,VAL*);
VAL lsqr_square(IND,VAL*);
int lsqr(int,void**);


// Deifine a macro for setting a keyword to default value
#define keyword_set(a,b) (a==0?b:a)



void lsqr_rproduct(IND num,IND n,IND *i,IND *j,VAL *aij,VAL *u,VAL *v){
  IND k;
  memset(v,0,n*sizeof(VAL));                         // zero out vector
  for(k=0;k<num;k++) v[j[k]]+=(aij[k]*u[i[k]]);      // compute u.A
}

void lsqr_lproduct(IND num,IND m,IND *i,IND *j,VAL *aij,VAL *v,VAL *u){
  IND k;
  memset(u,0,m*sizeof(VAL));                         // zero out the vector
  for(k=0;k<num;k++) {
    u[i[k]]+=(aij[k]*v[j[k]]);      // compute A.v
  }
}
void lsqr_normalize(IND n,VAL *v,VAL a){
  IND k;
  for(k=0;k<n;k++) v[k]/=a;
}

void lsqr_linear(IND n,VAL *v,VAL a,VAL *w){
  IND k;
  for(k=0;k<n;k++) v[k]=w[k]+a*v[k];
}


VAL lsqr_sign(VAL x){
  if(x < 0) return -1.;
  if(x > 0) return +1.;
  return 0.;
}


void lsqr_ortho(VAL a,VAL b,VAL *c,VAL *s, VAL *r){
  VAL t;
  if(b==0){
    *c=lsqr_sign(a);
    *s=0.;
    *r=fabs(a);
  }
  if(a==0){
    *c=0.;
    *s=lsqr_sign(b);
    *r=fabs(b);
  }
  if(fabs(b)>fabs(a)){
    t=a/b;
    *s=lsqr_sign(b)/sqrt(1.+t*t);
    *c=(*s)*t;
    *r=b/(*s);
  } else {
    t=b/a;
    *c=lsqr_sign(a)/sqrt(1.+t*t);
    *s=(*c)*t;
    *r=a/(*c);
  }
}

VAL lsqr_norm(IND n,VAL *v){
  IND i;
  VAL norm;
  norm=sqrt(lsqr_square(n,v));
  return norm;
}
VAL lsqr_square(IND n,VAL *v){
  IND k;
  VAL norm=0;
  for(k=0;k<n;k++) norm+=(v[k]*v[k]);
  return norm;
}
 


int lsqr(int argc,void *argv[]){
  printf("\n\nStarting LSQR.c\n\n\n");

  // inputs 
  IND num;       // number of elements in matrix
  IND n,m;       // dimensionality of matrix (n=unknowns, m=knowns)
  IND *ii,*jj;   // coordinates in the matrix
  VAL *aij;      // matrix elements
  VAL *b;        // vector 
  lsqr_control *ctl;
  lsqr_outputs *out;

  // outputs
  VAL *x0;       // output vector
  VAL *var;      // output variance
  int istop=0;   // stopping condition




  // extract values from the input 
  num=(IND)argv[0];     // number of non-zero elements in matrix
  n=(IND)argv[2];       // "x" size of matrix
  m=(IND)argv[1];       // "y" size of matrix
  ii=(IND*)argv[3];     // i coordinates of nonzero elements
  jj=(IND*)argv[4];     // j coordinates of nonzero elements
  aij=(VAL*)argv[5];    // nonzero elements
  b=(VAL*)argv[6];      // result vector
  x0=(VAL*)argv[7];     // initial guess vector
  var=(VAL*)argv[8];    // variance vector
  ctl=(lsqr_control*)argv[9];   // parameters to control the operation
  out=(lsqr_outputs*)argv[10];  // output parameters



  // set the IDL optional inputs
  VAL damp=keyword_set(ctl->damp,0);
  VAL atol=keyword_set(ctl->atol,1e-12);
  VAL btol=keyword_set(ctl->btol,1e-8);
  VAL conlim=keyword_set(ctl->conlim,1e8);
  IND itmax=keyword_set(ctl->itmax,2*n);
  int silent=keyword_set(ctl->silent,0);
  VAL dampsq=damp*damp;
  

  // output variables
  VAL r1norm=0;
  VAL r2norm=0;
  VAL anorm=0;
  VAL acond=0;
  VAL arnorm=0;
  VAL xnorm=0;
  IND iter=0;

  // internal variables
  IND i;
  VAL ctol=0.;
  VAL ddnorm=0,z=0;
  VAL res2=0,res1=0;
  VAL xxnorm=0.;
  VAL alfa=0,beta=0;
  VAL cs2=-1.,sn2=0.;
  VAL *v,*dv,*w,*dk,*dx;
  VAL *u,*du,*db;
  VAL rhobar,phibar,bnorm,rnorm,r1sq;
  VAL rhobar1,cs1,sn1,psi;
  VAL theta,cs,sn,rho,tau;
  VAL delta,gambar,rhs,zbar,gamma,phi;
  VAL tt1,tt2;
  VAL t1,t2,t3,t4,foo,rtol;
  VAL density;

  v=(VAL*)malloc(n*sizeof(VAL));
  dv=(VAL*)malloc(n*sizeof(VAL));
  w=(VAL*)malloc(n*sizeof(VAL));
  dk=(VAL*)malloc(n*sizeof(VAL));
  dx=(VAL*)malloc(n*sizeof(VAL));
  u=(VAL*)malloc(m*sizeof(VAL));
  du=(VAL*)malloc(m*sizeof(VAL));
  db=(VAL*)malloc(m*sizeof(VAL));


  

  // reset the CTOL
  if (conlim>0) ctol=1./conlim;

  // compute density
  density=100.-100*((VAL)num)/((VAL)n*m);
  


  // assume x on input is an initial guess
  lsqr_lproduct(num,m,ii,jj,aij,x0,db);  // compute shift in b
  for(i=0;i<m;i++) b[i]-=db[i];          // remove the shift
  
  

  // initialize 303
  memset(dx,0,n*sizeof(VAL));      // zero out the tweaks to the guess
  memset(v,0,n*sizeof(VAL));
  memset(w,0,n*sizeof(VAL));
  memcpy(u,b,m*sizeof(VAL));
  beta=lsqr_norm(m,b);

  // line 310 
  if(beta>0){
    lsqr_normalize(m,u,beta);
    lsqr_rproduct(num,n,ii,jj,aij,u,v);
    alfa=lsqr_norm(n,v);
  }

  // line 315 
  if(alfa>0){
    lsqr_normalize(n,v,alfa);
    memcpy(w,v,n*sizeof(VAL));
  }


  // more inits 319
  rhobar=alfa;
  phibar=beta;
  bnorm=beta;
  rnorm=beta;
  r1norm=rnorm;
  r2norm=rnorm;


  
  // line 328 
  arnorm=alfa*beta;
  if(arnorm==0) istop=-1;  // NORM=0 (probably A.u=0)

  // loop 346 
  while(iter < itmax && istop == 0){
    iter++;

    // start the iteration 
    lsqr_lproduct(num,m,ii,jj,aij,v,du);
    lsqr_linear(m,u,-alfa,du);
    beta=lsqr_norm(m,u);


    // line 358 
    if(beta > 0){
      lsqr_normalize(m,u,beta);
      anorm=sqrt(anorm*anorm+alfa*alfa+beta*beta+dampsq);
      lsqr_rproduct(num,n,ii,jj,aij,u,dv);
      lsqr_linear(n,v,-beta,dv);
      alfa=lsqr_norm(n,v);
      if(alfa>0) lsqr_normalize(n,v,alfa);
    }      

    // line 368
    rhobar1=sqrt(rhobar*rhobar+dampsq);
    cs1=rhobar/rhobar1;
    sn1=damp/rhobar1;
    psi=sn1*phibar;
    phibar=cs1*phibar;
    
    // 374
    lsqr_ortho(rhobar1,beta,&cs,&sn,&rho);


    // updates
    theta=sn*alfa;
    rhobar=-cs*alfa;
    phi=cs*phibar;
    phibar*=sn;
    tau=sn*phi;
    

    // updates the x/w arrays (385)
    tt1=phi/rho;
    tt2=-theta/rho;
    for(i=0;i<n;i++){
      dk[i]=w[i]/rho;
      dx[i]+=(tt1*w[i]);
      w[i]=v[i]+tt2*w[i];
      var[i]+=(dk[i]*dk[i]);
    }
    ddnorm+=lsqr_square(n,dk);


    // use more rotations
    delta=sn2*rho;
    gambar=-cs2*rho;
    rhs=phi-delta*z;
    zbar=rhs/gambar;
    xnorm=sqrt(xxnorm+zbar*zbar);
    gamma=sqrt(gambar*gambar+theta*theta);
    cs2=gambar/gamma;
    sn2=theta/gamma;
    z=rhs/gamma;
    xxnorm+=(z*z);
    
    // test for convergence
    acond=anorm*sqrt(ddnorm);
    res1=phibar*phibar;
    res2+=(psi*psi);
    rnorm=sqrt(res1+res2);
    arnorm=alfa*fabs(tau);

    
    // 419
    r1sq=rnorm*rnorm-dampsq*xxnorm;
    r1norm=sqrt(fabs(r1sq));
    if(r1sq<0.) r1norm=-r1norm;
    r2norm=rnorm;
    
    // 433
    t1=rnorm/bnorm;
    t2=arnorm/(anorm*rnorm);
    t3=1./acond;
    foo=anorm*xnorm/bnorm;
    t4=t1/(1.+foo);
    rtol=btol+atol*foo;

    // issue a stop criterion
    if(iter >= itmax) istop=7;
    if(1.+t3 <= 1.) istop=6;
    if(1.+t2 <= 1.) istop=5;
    if(1.+t4 <= 1.) istop=4;
    if(t3 <= ctol) istop=3;
    if(t2 <= atol) istop=2;
    if(t1 <= rtol) istop=1;


    if(ctl->silent == 0) {
      printf("%6ld %.3e %.3e %.3e %.3e %.3e %.3e\n",iter,
	     r1norm,r2norm,t1,t2,anorm,acond);
    }
  }
  
  // set outputs
  out->r1norm=r1norm;
  out->r2norm=r2norm;
  out->anorm=anorm;
  out->acond=acond;
  out->arnorm=arnorm;
  out->xnorm=xnorm;
  out->iter=iter;


  // add back the offsets
  for(i=0;i<m;i++) b[i]+=db[i];
  for(i=0;i<n;i++) x0[i]+=dx[i];


  // free memory
  free(v);
  free(dv);
  free(w);
  free(dk);
  free(dx);
  free(u);
  free(du);
  free(db);

  return istop;
}






int main(void){
  return 1;
}
