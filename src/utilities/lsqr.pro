;+
; This is a translation and reorganization of the Python
; implementation by Jeffery Kline with contributions by Mridul
; Aanjaneya and Bob Myhill and adapted for SciPy by Stefan van der Walt.
; This Python software was based on the ForTran version by Paige 
; & Saunders (1982a,1982b).  
;
; Translation to IDL was done by Russell Ryan.  During that
; translation, several IDL-centric modifications were added for the 
; purposes of performance.
;
; V1.0 of the IDL implementation.   R. Ryan Jan 12, 2016
;

;-


function lsqr_norm,a,SQUARE=square
  ;+
  ; Compute the L2 norm of a vector.  But allow user to return the 
  ; square of the norm (ie. save the sqrt calculation)
  ;-
  sqr=total(a*a)
  ret=keyword_set(SQUARE)?sqr:sqrt(sqr)
  return,ret
end

function lsqr_sgn,x
  ;+
  ; the sign function.
  ;-
  if x lt 0 then return,-1
  if x eq 0 then return,0
  if x gt 0 then return,1
  if ~finite(x) then print,'LSQR_SGN> x is not finite',x    
end

pro lsqr_sym_ortho,a,b,c,s,r
  ;+
  ; stable implementation of the Givens rotation
  ; http://en.wikipedia.org/wiki/Givens_rotation
  ;-
  if b eq 0 then begin
     c=lsqr_sgn(a)
     s=0.
     r=abs(a)
     return
  endif
  if a eq 0 then begin
     c=0.
     s=lsqr_sgn(b)
     r=abs(b)
     return
  endif
  if abs(b) gt abs(a) then begin
     t=a/b
     s=lsqr_sgn(b)/sqrt(1.+t*t)
     c=s*t
     r=b/s
  endif else begin
     t=b/a
     c=lsqr_sgn(a)/sqrt(1.+t*t)
     s=c*t
     r=a/c
  endelse
end




function lsqr_idl,a,b,DAMP=damp,ATOL=atol,BTOL=btol,CONLIM=conlim,$
                  ISTOP=istop,IMSG=imsg,ITMAX=itmax,ITER=iter,$
                  R1NORM=r1norm,R2NORM=r2norm,ANORM=anorm,ACOND=acond,$
                  ARNORM=arnorm,XNORM=xnorm,VAR=var,ERR=err,$
                  SILENT=silent
  ;+
  ; main procedure.  Solve the sparse, linear, least-squares problem.
  ; translated from the python implementation in scipy:
  ;
  ; github.com/scipy/scipy/blob/v0.14.0/scipy/sparse/linalg/isolve/lsqr.py
  ;
  ; by Russell Ryan.  But added a few IDL-like things.
  ;-
  

  ;check inputs
  if ~obj_isa(a,'sparsematrix') then begin
     imsg='invalid sparsematrix'
     istop=-1
     return,0b
  endif else a->shape,m,n       ;get shape (254)
  
  ;check number of inputs
  nb=n_elements(b)
  if nb ne m then begin
     imsg='invalid dimensionality '+strn(m)+'x'+strn(n)+' '+strn(nb)
     istop=-2
     return,0b
  endif

  
  ;set some initial values
  if n_elements(DAMP)   eq 0 then damp=0.0
  if n_elements(ATOL)   eq 0 then atol=1d-12
  if n_elements(BTOL)   eq 0 then btol=1d-8
  if n_elements(CONLIM) eq 0 then conlim=1e8
  if n_elements(ITMAX)  eq 0 then itmax=2*n
  calc_var=arg_present(VAR) || arg_present(ERR)
  sil=keyword_set(SILENT)

  ;init the var (257)
  if calc_var then var=fltarr(n)


  ;print a starting message.
  if ~sil then begin
     print
     print,'    Sparse Equations and Least Squares: LSQR'
     print
     print,'Solves Ax=b, where A is sparse.  Translated to IDL from scipy: '
     print,'github.com/scipy/scipy/blob/v0.14.0/scipy/sparse/linalg/isolve/lsqr.py'
     print
     print,format='(%"The A matrix has %d rows and %d columns.")',m,n
     print
  endif

  ;init values (280-295)
  iter=0L
  istop=0
  ctol=(conlim gt 0)?1./conlim:0.
  anorm=0.
  acond=0.
  dampsq=damp*damp;double(damp)*double(damp)
  ddnorm=0.
  res2=0.
  xnorm=0.
  xxnorm=0.
  z=0.
  cs2=-1.
  sn2=0.



  ;set up the first vectors (u,v) for bidiagonization
  ;these stasify beta#u = b alfa#v=A'u (301)

  ;line 303
  v=fltarr(n)
  u=b                           ;USE_B
  x=fltarr(n)
  alfa=0.
  beta=lsqr_norm(u)
  w=fltarr(n)


  ;line 310
  if beta gt 0 then begin
     u/=beta
     v=a->rproduct(u)
     alfa=lsqr_norm(v)
  endif 

  ;line 315
  if alfa gt 0 then begin
     v/=alfa
     w=v
  endif

  ;more inits (319)
  rhobar=alfa
  phibar=beta
  bnorm=beta
  rnorm=beta
  r1norm=rnorm
  r2norm=rnorm

  ;print the starting point of the system
  if ~sil then begin
     fmt='(I6,1X,E9.3,1X,E10.3,1X,E10.3,1X,E9.3,1X,E9.3,1X,E9.3)'
     h1='  Iter    r1norm     r2norm '
     h2='    Compat       SQR   Norm(A)   Cond(A)'
     print,h1,h2
     print,iter,r1norm,r2norm,1.,alfa/beta,anorm,acond,f=fmt
  endif
 

  ;line 328
  arnorm=alfa*beta
  if arnorm eq 0 then begin
     imsg='NORM == 0 (probably A.u = 0)'
     istop=11
     print,imsg
     return,x
  endif

  ;start loop (346)
  while iter lt itmax && istop eq 0 do begin
     iter++

     u=a->lproduct(v)-alfa*u
     beta=lsqr_norm(u)

     ;line 358
     if beta gt 0 then begin
        u/=beta
        anorm=sqrt(anorm*anorm+alfa*alfa+beta*beta+dampsq)
        v=a->rproduct(u)-beta*v
        alfa=lsqr_norm(v)
        if alfa gt 0 then v/=alfa
     endif

     ;line 368
     rhobar1=sqrt(rhobar*rhobar+dampsq)
     cs1=rhobar/rhobar1
     sn1=damp/rhobar1
     psi=sn1*phibar
     phibar=cs1*phibar

     ;use symortho (374)
     lsqr_sym_ortho,rhobar1,beta,cs,sn,rho
     
     ;more updates (378)
     theta=sn*alfa
     rhobar=-cs*alfa
     phi=cs*phibar
     phibar=sn*phibar
     tau=sn*phi


     ;update x/w (385)
     t1=phi/rho
     t2=-theta/rho
     dk=w/rho
     x+=(t1*w)
     w=v+t2*w
     ddnorm+=lsqr_norm(dk,/square)
     if calc_var then var+=(dk*dk)

     ;use more rotations (396)
     delta=sn2*rho
     gambar=-cs2*rho
     rhs=phi-delta*z
     zbar=rhs/gambar
     xnorm=sqrt(xxnorm+zbar*zbar)
     gamma=sqrt(gambar*gambar+theta*theta)
     cs2=gambar/gamma
     sn2=theta/gamma
     z=rhs/gamma
     xxnorm+=(z*z)

     

     ;test for convergence (410)
     acond=anorm*sqrt(ddnorm)
     res1=phibar*phibar
     res2+=(psi*psi)
     rnorm=sqrt(res1+res2)
     arnorm=alfa*abs(tau)
     
     ;distinguish (419)
     r1sq=rnorm*rnorm-dampsq*xxnorm
;     if iter eq 1 then print,iter,sqrt(rnorm*rnorm/xxnorm),damp
     r1norm=sqrt(abs(r1sq))
     if r1sq lt 0 then r1norm=-r1norm
     r2norm=rnorm


     ;line 433
     test1=rnorm/bnorm
     test2=arnorm/(anorm*rnorm)
     test3=1./acond
     foo=anorm*xnorm/bnorm
     t1=test1/(1+foo)
     rtol=btol+atol*foo
 
     ;line 440
     if iter    ge itmax then istop=7
     if 1+test3 le 1.    then istop=6
     if 1+test2 le 1.    then istop=5
     if 1+t1    le 1.    then istop=4
     if test3   le ctol  then istop=3
     if test2   le atol  then istop=2
     if test1   le rtol  then istop=1

     ;test to print
     prnt=(istop ne 0) || $
          (n le 40) || $
          (iter le 10) || $
          (iter ge itmax) || $
          (test3 le 2*ctol) || $
          (test2 le 10*atol) || $
          (test1 le 10*rtol)
     if prnt && ~sil then print,iter,r1norm,r2norm,test1,test2,anorm,acond,f=fmt
  endwhile

  
  ;make a message
  case istop of
     7: imsg='maximum number of iterations'
     6: imsg='test3 <= 0.'
     5: imsg='test2 <= 0.'
     4: imsg='t < = 0.'
     3: imsg='test3 <= ctol'
     2: imsg='test3 <= atol'
     1: imsg='test3 <= rtol'
     else: 
  endcase



  ;some outro printing
  if ~sil then begin
     print
     print,'LSQR> finished.'
     print
  endif

  if arg_present(ERR) then err=sqrt(var)

  ;compute residuals
;  brecon=a->lproduct(x)
;  resid=brecon-b
  
  ;chi2=total(resid*resid) =? r1sq or res1


  return,x
end
              



function lsqr,ii,jj,aij,dim,b,DAMP=damp,ATOL=atol,BTOL=btol,CONLIM=conlim,$
              ISTOP=istop,IMSG=imsg,ITMAX=itmax,ITER=iter,$
              R1NORM=r1norm,R2NORM=r2norm,ANORM=anorm,ACOND=acond,$
              ARNORM=arnorm,XNORM=xnorm,VAR=var,ERR=err,$
              SILENT=silent,USEIDL=useidl,X0=x0


  if keyword_set(USEIDL) then begin
     ;use the IDL code
;     stop,'must apply shift to aij for initial x0!'


     a=obj_new('sparsematrix',ii,jj,aij,dim)
     res=lsqr_idl(a,b,DAMP=damp,ATOL=atol,BTOL=btol,CONLIM=conlim,$
                  ISTOP=istop,IMSG=imsg,ITMAX=itmax,ITER=iter,$
                  R1NORM=r1norm,R2NORM=r2norm,ANORM=anorm,ACOND=acond,$
                  ARNORM=arnorm,XNORM=xnorm,VAR=var,SILENT=silent)
     obj_destroy,a
     
  endif else begin
     ; 
     ; IDL type    C type
     ; byte        char
     ; long        int
     ; ulong       unsigned int ?
     


     ;use the C code
     ftype=5                    ;IDL=double,C=double
     itype=3                   ;IDL=long64,C=long

     st=scope_traceback(/structure)
     st=st[n_elements(st)-1]
     input_directory=file_dirname(st.filename)
     cflags=!VERSION.OS_FAMILY eq 'unix'?'-O2':''

     make_dll,'lsqr',['lsqr'],input_directory=input_directory,$
              extra_cflags=cflags,extra_lflags='-lm',dll_path=dll_path
     
     nx=dim[0]
     ny=dim[1]
     num=n_elements(aij)

    

     in={damp:0d,atol:1d-12,btol:1d-8,conlim:1d8,itmax:2*fix(ny,type=itype),$
         silent:1b}
     out={r1norm:0d,r2norm:0d,anorm:0d,acond:0d,arnorm:0d,xnorm:0d,iter:0ll}


     if keyword_set(DAMP) then in.damp=damp
     if keyword_set(ATOL) then in.atol=atol
     if keyword_set(BTOL) then in.btol=btol
     if keyword_set(CONLIM) then in.conlim=conlim
     if keyword_set(ITMAX) then in.itmax=itmax
     in.silent=keyword_set(SILENT)


     
     nn=n_elements(x0)
     res=(nn eq 0)?make_array(ny,value=0,type=ftype):fix(x0,type=ftype)
     var=make_array(ny,value=0.,type=ftype)

     istop=call_external(dll_path,'lsqr',$
                         fix(num,type=itype),$
                         fix(nx,type=itype),$
                         fix(ny,type=itype),$
                         fix(ii,type=itype),$
                         fix(jj,type=itype),$
                         fix(aij,type=ftype),$
                         fix(b,type=ftype),$
                         res,var,in,out,$
                         value=[1b,1b,1b,0b,0b,0b,0b,0b,0b,0b,0b])


     r1norm=out.r1norm
     r2norm=out.r2norm
     anorm=out.anorm
     acond=out.acond
     arnorm=out.arnorm
     xnorm=out.xnorm
     iter=out.iter
     imsg=''

  endelse
  err=sqrt(var)


  return,res
end  

  
  
  
