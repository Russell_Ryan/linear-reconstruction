
;function sparse_histogram,data,locations=locations

data=[[0,0,0,1,1,1,0],$
      [0,0,0,0,0,0,0],$
      [2,2,0,0,3,3,3],$
      [0,0,8,8,0,0,0]]






  type=size(data,/type)
  g=where([1,2,12,3,13,14,15] eq type,n)
  if n ne 1 then begin
     print,'SPARSE_HISTOGRAM> Warning.  This does not have much use for '
     print,'                  floating point data.'
  endif

  ;map the data to unique set
;  sorted_data=data[sort(data)]
  data_enum=data[uniq(data,sort(data))]
  mapped_data=value_locate(data_enum,data)

  ;histogram them
  h=histogram(mapped_data,min=1,locations=b,reverse=ri)
  locations=data_enum[b]
  nobj=n_elements(h)
  npix=total(h,/preserve)       ;total number of pixels

  xyd=ri[nobj+1:n_elements(ri)-1]

;  return,h
end
