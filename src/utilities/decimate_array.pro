;+
; To decimate an array.  The inds can be any sortable type.
;
; This is the fastest I've tested that does not rely on differencing, 
; which is prone to catastrophic cancellation.  
; 
; see: http://www.idlcoyote.com/code_tips/drizzling.html 
;
;-


function decimate_array,inds,data,COUNT=count,LOCATIONS=locations,$
                        DOUBLE=double,MODE=mode
  

  ;set the type
  dbl=keyword_set(DOUBLE)

  ;compress the index before histogramming
  inds_comp=compress_index(inds,ind_uniq=iq)

  ;compute the histogram
  h=histogram(inds_comp,reverse=ri,locations=b)
  count=n_elements(h)
  
  ;record the locations if requested
  if arg_present(LOCATIONS) then locations=iq[b]
  
  ;sum over repeated indices
;  tot=make_array(count,float=~dbl,double=dbl)
;  for k=0ul,count-1 do tot[k]=total(data[ri[ri[k]:ri[k+1]-1]],double=dbl)


  if n_elements(MODE) eq 0 then mode='total'
  out=make_array(count,float=~dbl,double=dbl)

  ;switch the mode based on type
  case strlowcase(mode) of
     'total': for k=0ul,count-1 do $
        out[k]=total(data[ri[ri[k]:ri[k+1]-1]],double=dbl)
     'average': for k=0ul,count-1 do $
        out[k]=total(data[ri[ri[k]:ri[k+1]-1]],double=dbl)/h[k]
     'max': for k=0ul,count-1  do $
        out[k]=max(data[ri[ri[k]:ri[k+1]-1]])
     'min': for k=0ul,count-1  do $
        out[k]=min(data[ri[ri[k]:ri[k+1]-1]])
     'median': for k=0ul,count-1 do $
        out[k]=median(data[ri[ri[k]:ri[k+1]-1]],double=dbl)
     'count': out=h
     else: stop
  endcase

  return,out
end
