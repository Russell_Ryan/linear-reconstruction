/*****************************************************************
 *                       DOTPRODUCT                              *
 *                                                               *
 *  Compute the dot-product between a set of matrix coordinates  *
 *  and values.  This is considerably faster than equivalent     *
 *  IDL-only routines.                                           *
 *                                                               *
 *  by R. Ryan                                                   *
 *                                                               *
 *****************************************************************/



#include <stdlib.h>
#include <stdio.h>

/* 

   DATA TYPES

   IDL        C
   int        short
   long       int
   long64     long long
   float      float
   double     double


*/

/* set return variables */
#define SUCCESS 0                   /* flag for successful run */
#define VERSION 1                   /* version number */

/* set the data types */
typedef unsigned long SIZE;         /* ulong()   from IDL */
typedef unsigned long long INDEX;   /* ulong64() from IDL */
typedef double ELEMENT;             /* double()  from IDL */



int version(int argc,void*argv[]){
  /* return the version number */
  return VERSION;
}

int rproduct(int argc,void*argv[]){
  /*
     Simple program to compute  the dot product of the form:

            u = v.A
      
     Where A, u, v are known as sparse constructs in the driving IDL 
     code (sparsematrix__define.pro)
  */

  /* initialize data types */
  SIZE k,n,ni,nj;
  INDEX *i,*j;
  ELEMENT*aij,*vi,*xj;
  
  /* grab the inputs */
  n=(SIZE)argv[0];
  ni=(SIZE)argv[1];
  nj=(SIZE)argv[2];
  i=(INDEX*)argv[3];
  j=(INDEX*)argv[4];
  aij=(ELEMENT*)argv[5];
  vi=(ELEMENT*)argv[6];

  /* grab the outputs */
  xj=(ELEMENT*)argv[7];

  /* assume the output vector is zeroed out.  basic dot-product math */
  for(k=0;k<n;k++) xj[j[k]]+=(aij[k]*vi[i[k]]);

  return SUCCESS;
}


int lproduct(int argc,void* argv[]){
  /*
     Simple program to compute  the dot product of the form:

            v= A.u
      
     Where A, u, v are known as sparse constructs in the driving IDL 
     code (sparsematrix__define.pro)
  */

  /* initialize the variables */
  SIZE k,n,ni,nj;
  INDEX *i,*j;
  ELEMENT *aij,*uj,*xi;
  
  /* grab the inputs */
  n=(SIZE)argv[0];
  ni=(SIZE)argv[1];
  nj=(SIZE)argv[2];
  i=(INDEX*)argv[3];
  j=(INDEX*)argv[4];
  aij=(ELEMENT*)argv[5];
  uj=(ELEMENT*)argv[6];

  /* grab the outputs */
  xi=(ELEMENT*)argv[7];


  /* assume the output v vector is zeroed out.  basic dot-product math */
  for(k=0;k<n;k++) xi[i[k]]+=(aij[k]*uj[j[k]]);

  return SUCCESS;
}
