;+
; find indices of a for elements that are *IN* a, but *NOT IN* b.
; Preservie duplicates and order of a.
; 
; Returns the indices of a... So values in a, but not b will be 
; ind=nonmatch(a,b)
; auniq=a[ind]
;
; R Ryan
;-

function nonmatch,a,b,COUNT=count
  na=n_elements(a)
  nb=n_elements(b)
  if na eq 0 then begin
     print,'NONMATCH> a must be specified.'
     return,-1
  endif
  if nb eq 0 then return,lindgen(na)
  
  ;integer types
  ints=[1,2,3,12,13,14,15]
  

  ;find all uniq indices in both sets
  ab=[a,b]
  enum=ab[uniq(ab,sort(ab))]

  ;compressed indices
  aa=value_locate(enum,a)
  bb=value_locate(enum,b)

  ;range of histogram
  mn=0L
  mx=n_elements(enum)
  
  ;compute histograms
  ha=histogram(aa,min=mn,max=mx,bin=1,reverse=ra,locations=bin)
  hb=histogram(bb,min=mn,max=mx,bin=1)

  g=where(hb eq 0 and ha ne 0,n)
  if n ne 0 then begin
     ;process if there are differences
     count=total(ha[g],/preserve)
     indices=lonarr(count)
     jj=0L
     for i=0L,n-1 do begin
        ii=g[i]
        dj=ha[ii]
        indices[jj:jj+dj-1]=ra[ra[ii]:ra[ii+1]-1]
        jj+=dj
     endfor
     indices=indices[sort(indices)]
  endif else begin
     ;everything in b is in a
     count=0L
     indices=-1L
  endelse

  return,indices
end
