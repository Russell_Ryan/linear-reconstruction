;+
; Convert a host of arrays to an array of structures.  Structures can 
; be anonymous (by omitting str or leaving to '') or named (by passing 
; a string for str). 
;
; R. Ryan
;-

function array_to_struct,str,_EXTRA=_extra

  ;get tags
  tags=tag_names(_extra)
  ntag=n_elements(tags)

  ;test the number of elements per tag
  num=lonarr(ntag)
  for i=0,ntag-1 do num[i]=n_elements(_extra.(i))

  ;check that all numbers are equal  
  ok=array_equal(num,num[0])
  if ~ok then begin
     print,'ARRAY_TO_STRUCTURE> Cannot build structure since tags have '
     print,'                    unequal numbers of elements.'
     return,-1
  endif


  ;make one element of the output structure 
  if size(str,/type) eq 7 && ~strcmp(str,'',/fold) then begin
     ;make a named structure
     out=create_struct(name=str)
  endif else begin
     ;make an anonymous structure
     out=create_struct(tags[0],(_extra.(0))[0])
     for i=1,ntag-1 do out=create_struct(out,tags[i],(_extra.(i))[0])
  endelse


  ;fill the output
  out=replicate(out,num[0])
  out_tag=tag_names(out)
  for i=0,ntag-1 do begin
     g=where(strmatch(out_tag,tags[i],/fold),nmat)
     if nmat eq 1 then out.(g[0])=_extra.(i)
  endfor

        
  

  return,out
end

