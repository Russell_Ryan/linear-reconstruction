pro copy_keyword,hdr,key,REPVAL=repval


  newkey='O'+strupcase(key)


  t=sxpar(hdr,newkey,count=count)


  if count eq 0 then begin
     origval=sxpar(hdr,key,count=count)
     
     if count eq 1 then begin
        
        sxaddpar,hdr,newkey,origval,' Replaced from '+strupcase(key)+$
                 ' ('+systime()+')'

     endif

  end

  if n_elements(REPVAL) eq 1 then sxaddpar,hdr,key,repval




end
