Function gather_2d, i, j, c, subi, subj, method=method, timing=timing
;
; Two-dimensional gathering (aka decimation) based on value_locate and
; reverse indices in histogram.  Assumes i, j are unsigned integers.
; value_locate is used to compress the range of i, j, and the combined
; index.

; How to use:
;
; RESULT = GATHER_2D (I, J, C, SUBI, SUBJ [, METHOD=METHOD])
;
; Here I, J, C are three arrays of length NINPUT
; SUBI, SUBJ, RESULT are three arrays of length NOUTPUT <= NINPUT
; The pairs [SUBI, SUBJ] replicate all pairs [I, J] *WITHOUT* repetition
;
; If a pair [SUBI[K], SUBJ[K]] matches multiple pairs [I[L1], J[L1]], 
;    [I[L1], J[L2]], ..., [I[LN],J[LN]], then
;    RESULT[K] = C[L1] + C[L2] + ... + C [LN]
;
; METHOD is one of eight methods listed at
; https://www.idlcoyote.com/tip_examples/time_test_drizzle_alg.pro
; (code in ARRAY_DECIMATE.PRO).  The default is Method 7, Craig
; Markwardt's FDDRIZZLE-like thinned WHERE histogram loop.  At the
; moment there is no provision for using multiple methods and/ or
; report method and timing information.
;
; Before running ARRAY_DECIMATE, range compression using
; VALUE_LOCATE is used on I and J, as well as on the combined
; index IJ (used internally).  This leads to an efficient use of
; HISTOGRAM.
;
; Note that I and J can in principle be any orderable
; array type (including string!), but of course beware of the
; concept of "equal" as applied to floats or doubles.
; 

if (keyword_set(method) eq 0) then method = 0
if (keyword_set(timing) eq 0) then timing = 0

t0 = systime(1)

ienum = i[sort(i)]
ienum = ienum[uniq(ienum)]
iindex = value_locate (ienum, i)
ni = long64(max(iindex)+1)

t1 = systime(1)
if (timing) then print, ' Normalized range of first index: ', t1-t0

jenum = j[sort(j)]
jenum = jenum[uniq(jenum)]
jindex = value_locate (jenum, j)

t2 = systime(1)
if (timing) then print, ' Normalized range of second index: ', t2-t1

ij = iindex + long64 (jindex) * ni

ijenum = ij[sort(ij)]
ijenum = ijenum [uniq(ijenum)]
ijindex = value_locate (ijenum, ij)

t3 = systime(1)
if (timing) then print, ' Normalized range of combined index: ', t3-t2

; Decimate


;print,'DECIMATION needs to be updated to use decimate_array'
cc=decimate_array(ijindex,c,locations=newinds)

;cc = array_decimate (ijindex, c, newinds=newinds, method=method)
                  ; so cc[i] is the total of all values of c where ijindex eq j
                  ; and newinds are the corresponding new indices
subij = ijenum [newinds]
subiindex = subij mod ni
subjindex = subij / ni

; stop

subi = ienum [subiindex]
subj = jenum [subjindex]

t4 = systime(1)
if (timing) then print, ' Decimation using method '+string(method,format='(i0)')+': ', t4-t3
if (timing) then print, ' Total time elapsed: ', t4-t0

return, cc
end
