;+
; N_UNIQ
;
; Return the number of unique values in an array
;
;-

function n_uniq,a
  q=uniq(a,sort(a))
  n=n_elements(q)
  return,n
end
