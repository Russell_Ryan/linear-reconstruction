;+
; LASTCHAR
; 
; return the last character of a string array, not really grism-
; centric, but I didn't need this until now.  Have to be careful
; when dealing with arrays.
;
; by R. Ryan
;-

function lastchar,str 
  if size(str,/type) ne 7 then return,str
  n=n_elements(str)
  len=strlen(str)
  last=strmid(str,len-1,1)
  last=last[lindgen(n)*(n+1)]
  
  if n eq 1 then last=last[0]

  return,last
end
