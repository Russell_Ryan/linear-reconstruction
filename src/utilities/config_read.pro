;+
; Read a configuration file and return a structure.  
; 
; R Ryan
;-
function config_read,configfile,COUNT=count,TAGS=tags,PROTOTYPE=prototype,$
                    _EXTRA=_extra

  
  ;check for valid inputs
  if size(configfile,/type) ne 7 then begin
     print,'CONFIG_READ> configfile must be a string.'
     return,-1
  endif

  ;check if file exists
  if ~file_test(CONFIGFILE) then begin
     print,'CONFIG_READ> config file ('+configfile+') not found.'
     return,-2
  endif
  
  ;get the name of the structure
  name=strsplit(configfile,'/',/extract,count=n)
  name=strsplit(name(n-1),'.',/ext,count=n)
  name=strjoin(name(0:n-2),'_')
  name=idl_validname(name,/convert_all)

  ;number of lines in file
  n=file_lines(configfile)
  tags=strarr(n)
  data=strarr(n)

  ;read the file
  openr,lun,configfile,/get_lun,error=error
  if error ne 0 then begin
     print,'CONFIG_READ> cannot read file.'
     return,-3
  endif

  ;read each line
  for i=0,n-1 do begin
     line=''
     readf,lun,line
     line=strtrim(line,2)

     ;assume line is SPACE delimited
     pos=strpos(line,' ')

     ;ok, there might be a TAB delimiter?
     if pos eq -1 then pos=strpos(line,string(9b))

     ;there's probably an elegant way to combine the previous two 
     ;lines into a single line using regular expressions or an OR 
     ;operator.  oh well.  


     ;record the name/data if it's valid
     if pos ne -1 && strmid(line,0,1) ne '#' then begin
        tags(i)=strmid(line,0,pos)
        data(i)=strmid(line,pos+1,strlen(line))
     endif
  endfor

  ;close the file
  close,lun & free_lun,lun


  ;get the good entries
  g=where(tags ne '',n)
  if n eq 0 then begin
     print,'CONFIG_READ> No valid entries.'
     return,-4
  endif
  tags=tags(g)
  data=data(g)

  ;only record unique names
  q=uniq(tags)
  count=n_elements(q)
  if count eq 0 then begin
     print,'CONFIG_READ> no valid entries.'
     return,-5
  endif
  tags=tags(q)
  data=data(q)



  ;do I prototype the structure?
  ptype=keyword_set(PROTOTYPE)
  if ptype then begin
     openw,lun,name+'__define.pro',/get_lun,error=error
     if error ne 0 then begin
        print,'CONFIG_READ> WARNING.  Cannot prototype function.'
        ptype=0b
     endif else begin
        printf,lun,'pro '+name+'__define'
        printf,lun,'  _={'+strupcase(name)+',$'
     endelse

  endif



  ;regular expressions for integers and floats
  intregex='^[-+]?[0-9][0-9]*$'
  fltregex='^[-+]?([0-9]+\.?[0-9]*|\.[0-9]+)([eEdD][-+]?[0-9]+)?$'

  ;read the valid entries
  first=1b
  for i=0,count-1 do begin
     ;remove a comment
     thisdata=strsplit(data(i),'(#|;|%)',/regex,count=n,/extract)
     thisdata=strtrim(thisdata(0),2)

     ;split arrays by space, comma, or TAB
     vector_tokens=[' ',',',string(9b)]
     regexpr='('+strjoin(vector_tokens,'|')+')'
     thisdata=strsplit(thisdata,regexpr,count=ntok,/extract,/preserve)
     if ntok eq 1 then begin
        ;a scalar data entry
        thisdata=thisdata(0)
     endif else begin
        ;an array data entry
        g=where(thisdata ne '',nn)
        if nn eq 0 then begin
           print,'CONFIG_READ> Warning: the line is not valid. This should '
           print,'             not happen?  so the TAGS=tags and COUNT=count '
           print,'             will be wrong.'
           goto,skip_this_line
        endif else thisdata=thisdata(g)
     endelse

;    
;     g=scalar?0:where(thisdata ne '')
;     thisdata=thisdata(g)
;     ntok=n_elements(thisdata)
;     scalar=(ntok eq 1)
;     
      
     ;test the data type
     isint=array_equal(stregex(thisdata,intregex,/bool),1b)
     isflt=array_equal(stregex(thisdata,fltregex,/bool),1b)
 
     ;recast the data if need-be
     type=7
     if isflt && ~isint then type=5
     if isint then type=3
     thisdata=fix(thisdata,type=type)


     ;check vector type
     ntok=n_elements(thisdata)
     scalar=ntok eq 1
     if scalar then thisdata=thisdata(0)

     

     ;concatenate up the structure
     if first then begin
        config=create_struct(tags(i),thisdata)
        first=0b
     endif else begin
        config=create_struct(config,tags(i),thisdata)
     endelse

     ;if we have to protoype
     if ptype then begin

        ;sort out the datatype
        case type of
           3: var=scalar?'0l':'lonarr('+strtrim(ntok)+')'
           5: var=scalar?'0d':'dblarr('+strtrim(ntok)+')'
           7: var=scalar?'""':'strarr('+strtrim(ntok)+')'
           else: stop,'variable type not prototyped.'
        endcase

        ;update the prototype
        printf,lun,'     '+tags(i)+':'+var+((i eq count-1)?' ':',')+'$'

     endif
     skip_this_line:

  endfor
  




  ;update with the EXTRA for command line settings
  nextra=n_tags(_extra)
  if nextra ge 1 then begin
     etags=tag_names(_extra)
     for i=0,nextra-1 do begin
 
        ;new CODE
        g=where(strmatch(strmid(tags,0,strlen(etags[i])),etags[i],/fold),n)
        case n of
           0:
           1: config.(g[0])=_extra.(i)
           else: print,'CONFIG_READ: Multiple keywords match '+etags[i]
        endcase
        
     endfor
  endif


  ;close the prototype?
  if ptype then begin
     printf,lun,'   }'
     printf,lun,'end'
     close,lun
     free_lun,lun

     ;now copy the data over to the prototpyed version
     conf=create_struct(name=name)
     struct_assign,config,conf
     config=temporary(conf)
  endif

  
  return,config

end
