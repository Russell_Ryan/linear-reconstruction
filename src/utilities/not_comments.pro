;+
; return the indices of a string array whose first character is not 
; in a list of potential comment characters.
;
; R Ryan
;-

function not_comments,str,COUNT=count,SILENT=silent
  comments=['#',';','%']        ;potential comment tokens

  ;check that str is valid
  n=n_elements(str)
  if n eq 0 || size(str,/type) ne 7 then begin
     count=0
     return,-1
  endif

  ;remove the spaces
  str2=strtrim(str,2)


  ;test against each of the comments
  ;could speed this up with a repeat-loop and have it terminate once 
  ;logic is all 0b, but that requires a bit more coding...
  logic=replicate(1b,n)
  for i=0,n_elements(comments)-1 do begin
     first=strmid(str2,0,strlen(comments[i]))
     logic and= (first ne comments[i])
  endfor

  ;find the good entries
  g=where(logic,count)

  ;print a warning if no entries are valid
  if count eq 0 && ~keyword_set(SILENT) then $
     print,'NOT_COMMENTS: No valid entries...'

  return,g
end
  
