;+
; POOL.PRO
;
; Description:
;
; A general purpose threadpool procedure to run a fuction or procedure
; many times with some ITERATEABLE input and CONSTANT inputs.
;
; Calling Sequence:
;     IDL> pool,nthread,worker,[EXECUTABLES=executables,ARGS=args,$
;          CONSTS=consts,ITERS=iters,/is_function,RETVAL=retval,$
;          POLLTIME=polltime,OUTROOT=outroot,/silent,/monitor,$
;          CALLBACK=callback,STATUS=status,ERRMSG=errmsg]
;
; Inputs:
;     nthread: scalar integer.  Number of threads to process with.
;     worker: scalar string.  Name of the function/procedure to run.
;
; Optional Inputs:
;     executables: vector string.  IDL commands to issue to each
;                  thread upon startup.
;     args: vector string.  The IDL variable names to pass to worker
;     consts: scalar structure.  This declares which variables are 
;             passed to the worker, but not iterated over.  The 
;             structure tags must appear in the args list.  These 
;             variables can be of any type and are passed using the 
;             IDL save and restore procedures.
;     iters: scalar structure. This declares which variables are to 
;            be iterated over.  The structure tags must appear in the 
;            args list. These data must be only numeric types (no
;            structures, objects, pointers, etc.)  This could be
;            relaxed in future version.
;     /is_function: flag to declare if worker is a function.  default 
;                   is to assume that owrker is a procedure.
;     polltime: scalar float.  Time to wait before repolling the 
;               threads.
;     outroot: scalar string.  Root name of the output log file (one 
;              log file per thread)
;     /silent: flag to suppress printing
;     /monitor: flag to enable some monitoring programs
;     callback: scalar string.  Define a user callback function. 
;               Declared like:
;
;     PRO MY_CALLBACK,status,error,oThread,userdata
;         ;Various IDL commands in here
;         ;useful to monitor the code in your own special way.
;     END
;
;
; Outputs:
;
; Optional Outputs:
;     STATUS: scalar integer.  giving status of the run.
;     ERRMSG: scalar string.  Any error message obtained by a thread
;     REVAL: any type. The return variable if the worker is a function
;
; Comon Blocks:
;    POOL         Contains the "self" that controls the algorithm's operation
;    POOL_RETURN  Contains info for returning a value (for a function)
;    POOL_ITER    Contains the iteratable data
;    POOL_LOCK    Locks the bridge, so only one instance of pool
;
; Internal Routines:
;    These internal routines are not meant to be run from outside 
;    of pool, but I document them just to be safe.
; 
;    pool_data__define: defines the self structure
;    pool_iterate__define: defines the iteratable data
;    pool_valid: check that the input is valid
;    pool_callback: runs when thread finishes
;    pool_start: starts a thread
;    
; Notes:
;    This is more natural to write as an Object, but the IDL_IDLBridge 
;    cannot use object methods as callback functions.  So that would 
;    require a helper procedure that uses call_method, but that seemed
;    almost as clunky as using common blocks. 
;
; Limitations:
;    Currently not easy to accept multiple outputs.  Such as a
;    procedure (or function) that has several optional outputs.  An 
;    example would be like poly_fit.  There one always returns the 
;    fitting coefficients as it is a function, but may also want to 
;    return the chi2.  This would be impossible with pool.  However
;    a simple work-around would be to wrap poly_fit in a separate 
;    function that returns a floating array.  Then the onus is on 
;    the user to interpret this array outside of pool.  
;
;    At present passing/returning structures is difficult.  This 
;    could be remedied in a few ways, but I have not felt the need 
;    for this yet (though the advantages are obvious, the first that 
;    comes to mind is to address the previous limitation).
;
; Examples:
;    To demonstrate how to pass an iteratable variable (x in this
;    case) and a constant variable (pi in this case).  this example 
;    is also a function, but procedural examples are just as 
;    straightforward.
;    IDL> pool,5,'sin',args=['x','pi'],/is_function,retval=y,$
;         iters={x:findgen(100)},const={pi:!PI}
;    Now sin is declared as:
;    FUNCTION sin,x,pi
;       y=sin(x*pi)
;       return,y
;    END
;
;
; History:
;      Written by Russell Ryan (STScI).  Mar 1, 2016
;
;-





pro pool_data__define
  ;+
  ; Define the control variables for the pool algorithm.
  ;-
  _={POOL_DATA,$
     iter:0ul,$                 ;the current iterate to start
     ndone:0ul,$                ;number of iterates that have finished
     ntodo:0ul,$                ;number of iterates to process
     nthread:0u,$               ;number of threads
     threads:obj_new(),$        ;container for all the threads
     t0:0d0,$                   ;time the pool began
     command:'',$               ;the command to execute
     callback:'',$              ;user defined callback procedure
     has_callback:0b,$          ;has a user-defined callback procedure?
     verbose:0b,$               ;do we print?
     memleak:0b,$               ;do we clean the memory leak?
     is_function:0b $           ;is it a function?
    }
end
pro pool_iterate__define
  ;+
  ; Define the set of iteratable variables
  ;-
  _={POOL_ITERATE,$
     niter:0u,$                 ;number of iterateable variables
     piter:ptr_new(),$          ;collection of iterateable variables
     itags:ptr_new() $          ;names of the iterateable variables
    }
end


function pool_valid,struct,type
  ;+
  ; make sure a structure is a valid type
  ;-
  valid=size(struct,/type) eq 8 && strcmp(type,tag_names(struct,/str),/fold)
  return,valid
end


pro pool_callback,stat,err,thread,user
  ;+
  ; run this when a thread finishes
  ;-

  common pool,self
  common pool_return,ret,retvar
  if ~pool_valid(self,'pool_data') then return
  case stat of
     0:
     1: return
     2: 
     3: stop,'POOL_ERROR: '+err
     4: stop,'POOL_ABORT: '+err
  endcase

  ;is there a return variable
  if self.is_function then begin
     t=thread->GetVar(retvar)
     if self.ndone eq 0 then ret=replicate(t,self.ntodo) else ret[user]=t     
  endif

  ;clean up the memory leakage
  if self.memleak then begin
     widgets=widget_info(/managed)
     g=where(widget_info(widgets,/valid),n)
     if n ne 0 then begin
        widgets=widgets[g]
        types=widget_info(widgets,/event_pro)
        g=where(strmatch(types,'IDL_IDLBRIDGETIMER_EVENT',/fold),n)
        for i=0,n-1 do widget_control,widgets[g[i]],/destroy
     endif
   endif

  ;call a user-defined callback function
  if self.has_callback then call_procedure,self.callback,stat,err,thread,user

  ;update a counter
  self.ndone++

  
  ;print the screen
  if self.verbose then begin

     ;compute diagnostics
     frac=float(self.ndone)/float(self.ntodo)   ;fraction completed
     time=(systime(/sec)-self.t0)*(1./frac-1.)  ;average time left
     time=string(time,f='(F0.1)')+' sec'        ;thing to print
     
     ;the message to print     
     f='(%"\33[1M POOL> completed %i/%i = %5.1f\%, ~%s remaining\33[1A")'
     print,self.ndone,self.ntodo,100.*frac,time,f=f
           
  endif
        
  ;restart the bridge
  pool_start,thread

end

 

pro pool_start,thread
  ;+ 
  ; use this to start a thread
  ;-

  common pool,self
  common pool_iter,iter

  ;don't bother if we're done or it's invalid
  if self.iter eq self.ntodo || ~pool_valid(self,'pool_data') then return

  ;make sure the thread is ok
  case thread->Status(error=error) of
     0:
     1: return
     2: 
     3: stop,'POOL> error: '+error
     4: stop,'POOL> abort: '+error
  endcase  

  ;set the iterateables to the thread
  if pool_valid(iter,'pool_iterate') then begin
     for j=0,iter.niter-1 do begin
        val=reform(((*iter.piter).(j))[self.iter,*,*,*,*,*,*,*])
        if n_elements(val) eq 1 then val=val[0]

        ; to restore any data type
        iter_file=filepath('pool_iter.idl',/tmp)
        (scope_varfetch((*iter.itags)[j],/enter))=val
        call_procedure,'save',(scope_varfetch((*iter.itags)[j],/enter)),$
                       filename=iter_file
        thread->SetVar,'iter_file',iter_file
        thread->Execute,'restore,iter_file'

;        case size(val,/type) of
;           1: thread->SetVar,(*iter.itags)[j],val
;           2: thread->SetVar,(*iter.itags)[j],val
;           3: thread->SetVar,(*iter.itags)[j],val
;           4: thread->SetVar,(*iter.itags)[j],val
;           5: thread->SetVar,(*iter.itags)[j],val
;           6: thread->SetVar,(*iter.itags)[j],val
;           7: thread->SetVar,(*iter.itags)[j],val
;           8: 
;           9: thread->SetVar,(*iter.itags)[j],val
;           10:
;           11: 
;           12: thread->SetVar,(*iter.itags)[j],val
;           13: thread->SetVar,(*iter.itags)[j],val
;           14: thread->SetVar,(*iter.itags)[j],val
;           15: thread->SetVar,(*iter.itags)[j],val
     endfor
  endif


  ;actually start the bridge here
  thread->SetProperty,user=self.iter++
  thread->Execute,self.command,/nowait
end




pro pool,nthread,worker,EXECUTES=executes,ARGS=args,CONSTS=consts,$
         ITERS=iters,IS_FUNCTION=is_function,RETVAL=retval,$
         POLLTIME=polltime,OUTROOT=outroot,SILENT=silent,$
         MONITOR=monitor,CALLBACK=callback,STATUS=status,ERRMSG=errmsg
  
              
  ;+
  ; main program to build and run a thread pool
  ;-


  common pool,self
  common pool_return,ret,retvar
  common pool_iter,iter
  common pool_lock,lock

  ;lock the pool if it's being used
  if size(lock,/type) ne 0 then begin
     status=-1 & errmsg='Pool is locked.'     
  endif else lock=1b



  ;set defaults
  if n_elements(POLLTIME) eq 0 then polltime=1.
  status=1 & errmsg=''
  
  if nthread le 0 then stop

  ;build an internal structure
  self={pool_data}
  self.nthread=nthread
  self.threads=obj_new('IDL_Container')
  self.is_function=keyword_set(IS_FUNCTION)
  self.memleak=float(strmid(!version.release,0,3)) lt 8.3
  self.verbose=~keyword_set(SILENT)
  if size(callback,/type) eq 7 then begin
     self.has_callback=1b
     self.callback=callback
  endif

  ;figure out the calling
  has_args=keyword_set(ARGS)
  has_consts=keyword_set(CONSTS)
  has_iters=keyword_set(ITERS)
  has_monitor=keyword_set(MONITOR)

  ;build the command
  a=has_args?strjoin(args,','):''
  if keyword_set(IS_FUNCTION) then begin
     retvar='pool_return'       ;name of returned variable (for a function)
     self.command=retvar+'='+worker+'('+a+')'
  endif else begin
     self.command=worker+(has_args?',':'')+a
  endelse


  ;write the constants to a file  
  if has_consts then begin
     ;write a save file
     constfile='pool_'+idl_validname(systime(),/convert_all)+'.idl.Z'
     constfile=filepath(constfile,/tmp)
     save,consts,file=constfile,/compress
    
     ;properties of the constants     
     ctags=tag_names(consts)
     nconst=n_elements(ctags)
  endif

  ;process iteratble variables
  if has_iters then begin

     ;structure of the iteratables
     iter={pool_iterate}

     ;number of iterateable variables
     iter.niter=n_tags(ITERS)
     ok=bytarr(iter.niter)

     ;get the dimensions of the variables, and if they're equal
     dims=size(iters.(0),/dim) & ok[0]=1b
     for i=1,iter.niter-1 do begin
        d=size(iters.(i),/dim)
        ok[i]=array_equal(d,dims)
     endfor

     ;if equal, then they're ok to use
     if array_equal(ok,1b) then begin
        iter.piter=ptr_new(iters)
        iter.itags=ptr_new(tag_names(iters))
        self.ntodo=dims[0]
     endif else begin
        if self.verbose then $
           print,'POOL> Iteratable variables not valid, cannot proceed.'
        goto,CLEANUP
     endelse
  endif

  ;record the start time
  self.t0=systime(/sec)


  ;start the threads
  if has_monitor then pid=strarr(self.nthread)
  outroot=keyword_set(OUTROOT)?(outroot+'-'):''
  cd,current=cwd
  startup=pref_get("IDL_STARTUP")
  nexec=n_elements(executes)
  for i=0,self.nthread-1 do begin

     ;build a thread
     output=outroot+strtrim(i,2)+'.log'
     thread=obj_new('IDL_IDLBridge',output=output,callback='pool_callback')
     
     ;check that the thread is valid
     if ~obj_valid(thread) then begin
        status=0 & errmsg='Pool unable to initialize thread.'
        goto,CLEANUP
     endif

     ;execute some commands here
     thread->Execute,'cd,"'+cwd+'"'
     if file_test(startup) then thread->Execute,"@"+startup
     for j=0,nexec-1 do thread->Execute,executes[j]



     ;process the const variables
     if has_consts then begin
        thread->Execute,'restore,"'+constfile+'"'
        for j=0,nconst-1 do thread->Execute,ctags[j]+'=consts.'+ctags[j]
        thread->Execute,'delvar,consts'
     endif

     ;add this thread to the collection
     self.threads->Add,thread

     ;start the monitoring
     if has_monitor then begin
        spawn,'xterm -hold -T '+output+' -e "tail -F '+output+'" &',res
        res=strsplit(res,' ',/ext,count=npid)
        pid[i]=res[npid-1]
     endif
     

     ;start this thread
     pool_start,thread
  endfor

  ;clean up the const file
  if has_consts then file_delete,constfile,/allow_non


  ;block the bridges
  idle=bytarr(self.nthread)
  repeat begin
     for i=0,self.nthread-1 do begin
        thread=self.threads->Get(pos=i)
        case thread->Status(error=error) of
           0: idle[i]=1b        ;thread has yet to start
           1: idle[i]=0b        ;thread is running
           2: idle[i]=1b        ;thread is finished
           3: begin             ;thread has an error
              idle[i]=0b
              status=0 & errmsg='Thread has an error.'              
              if self.verbose then begin
                 print,'POOL> Thread ('+strtrim(i,2)+') halted with message:'
                 print,'      '+error
              endif
              goto,CLEANUP
           end
           4: begin             ;thread was aborted
              idle[i]=0b
              status=0 & errmsg='Pool was aborted.'              
              if self.verbose then begin
                 print,'POOL> Thread ('+strtrim(i,2)+') aborted with message:'
                 print,'      '+error          
              endif
              goto,CLEANUP
           end
           else:begin           ;thread error?
              idle[i]=0b
              status=0 & errmsg='Pool has unknown status.'
              if self.verbose then begin
                 print,'POOL> Thread ('+strtrim(i,2)+') has unknown status: '+$
                       strtrim(s,i)
              endif
              goto,CLEANUP
           end
        endcase
     endfor
     
     ;quit blocking the threads if all are done and there are 
     ;no more iterates to process
     done=array_equal(idle,1b) && self.ndone eq self.ntodo

     ;only wait if we are not done
     if ~done then wait,polltime
  endrep until done

  ;get the return value
  if self.is_function && arg_present(RETVAL) then retval=temporary(ret)
  
  ;just to move the line
  if self.verbose then print


CLEANUP:                         ;clean up the memory

  ;destroy the monitors
  if has_monitor then for i=0,self.nthread-1 do spawn,'kill -9 '+pid[i]
  
    


  ;destroy the bridges
  obj_destroy,self.threads

  ;free the iterateable variables
  ptr_free,iter.piter,iter.itags

  ;unlock
  unlock=temporary(lock)

end


