;+
; Get a random file name
;-

function random_file,n,SUFFIX=suffix,NCHAR=nchar
  if n_elements(n) eq 0 then n=1
  if n_elements(nchar) eq 0 then nchar=12


  if keyword_set(SUFFIX) then begin
     if strmid(suffix,0,1) ne '.' then suffix='.'+suffix
  endif else begin
     suffix=''
  endelse

  ;valid charachters
  b=byte('01234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')

  ;get a bunch of characters
  bytes=b[floor(randomu(s,nchar,n)*n_elements(b))]

  ;make into file names
  files=string(bytes)+suffix
  
  ;test if the files exist
  tests=file_test(files)

  ;print an error message, just in case
  g=where(tests eq 1b,n)
  if n ne 0 then begin
     print,'WARNING: random files are found to exist:'
     for i=0,n-1 do print,files[g[i]]
  endif

  return,files
end
