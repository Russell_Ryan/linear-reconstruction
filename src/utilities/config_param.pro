function config_param,struct,param,SILENT=silent,VALID=valid
  
  if size(struct,/type) ne 8 then begin
     if ~keyword_set(SILENT) then print,'CONFIG_PARAM> struct is invalid type.'
     valid=0b
     return,-1
  endif

  if size(param,/type) ne 7 then begin
     if ~keyword_set(SILENT) then print,'CONFIG_PARAM> param is invalid type.'
     valid=0b
     return,-1
  endif

  tags=tag_names(struct)
  g=where(strmatch(tags,param,/fold),n)
  valid=n eq 1
  if valid then begin
     return,struct.(g[0])
  endif else begin
     if ~keyword_set(SILENT) then $
        print,'CONFIG_PARAM> parameter ('+param+') not found.'
     return,-1
  endelse
end
