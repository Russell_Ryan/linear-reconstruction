pro savefile::getvar,FILE=file,NAMES=names,COUNT=count
  ;+
  ; get various properties of the object
  ;-
  if arg_present(FILE) then file=self.file
  if arg_present(NAMES) then names=*self.names
  if arg_present(COUNT) then count=self.count
end


pro savefile::load,file
  ;+
  ; Load an IDL save file
  ;-

  if ~file_test(file) then return
  self->destroy
  self.file=file
  
  self.obj=obj_new('IDL_Savefile',file)
  
  names=self.obj->names()
  self.count=n_elements(names)
  if self.count eq 0 then begin
     self->destroy
  endif else begin
     self.names=ptr_new(names,/no)
  endelse


end 
 
function savefile::restore,var,VALID=valid
  ;+
  ; restore a variable
  ;-

  out=-1
  valid=0b

  if self.count ge 1 then begin
     g=where(strmatch(*self.names,var,/fold),n)

     case n of
        0: print,'SAVEFILE::RESTORE> Variable ('+var+') not found.'
        1: begin
           self.obj->restore,var
           out=scope_varfetch(var)
           valid=1b
        end
        else: print,'SAVEFILE::RESTORE> Multiple entries ('+var+'), invalid?'
     endcase
  endif

  return,out
end


pro savefile::destroy
  ;+
  ; Destroy various things in the object
  ;-
  if obj_valid(self.obj) then obj_destroy,self.obj
  if ptr_valid(self.names) then ptr_free,self.names  
end

pro savefile::cleanup
  ;+
  ; standard cleanup method
  ;-
  self->destroy
end
function savefile::init,file
  ;+
  ; standard initialization method
  ;-
  if size(file,/type) eq 7 then self->load,file

  return,1b
end

pro savefile__define
  _={SAVEFILE,$
     file:'',$                  ;filename of current save file
     obj:obj_new(),$            ;IDL_Savefile object
     names:ptr_new(),$          ;names of vars in file
     count:0L $                 ;number of vars in file
    }
end
     
