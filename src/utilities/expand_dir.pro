;+
; expand an environment variable (with first character $) into 
; the setting in the .login file
;
; By R. Ryan
;-
pro expand_dir,dir
  psep=path_sep()
  t=strsplit(dir,psep,/ext)
  g=where(strmatch(t,'\$*'),n)
  for i=0,n-1 do begin
     tt=strmid(t[g[i]],1,strlen(t[g[i]])-1)
     tt=getenv(tt)
     if tt ne '' then t[g[i]]=tt
  endfor
  dir=strjoin(t,psep())
end
