pro putsip,h,ast
  if array_equal(strupcase(ast.ctype),['RA---TAN-SIP','DEC--TAN-SIP']) && $
     tag_exist(ast,'distort') && strcmp(ast.distort.name,'SIP',/fold) then begin
     
     sa=size(ast.distort.a,/dim)
     sxaddpar,h,'A_ORDER',sa[0]-1
     for i=0,sa[0]-1 do begin
        ii=strtrim(i,2)
        for j=0,sa[1]-1 do begin
           jj=strtrim(j,2)
           key='A_'+ii+'_'+jj
           ij=i+j
           if i+j le sa[0]-1 && ij gt 1 then sxaddpar,h,key,ast.distort.a[i,j]
        endfor
     endfor



     sb=size(ast.distort.b,/dim)
     sxaddpar,h,'B_ORDER',sb[0]-1
     for i=0,sb[0]-1 do begin
        ii=strtrim(i,2)
        for j=0,sb[1]-1 do begin
           jj=strtrim(j,2)
           key='B_'+ii+'_'+jj
           ij=i+j
           if ij le sb[0]-1 && ij gt 1 then sxaddpar,h,key,ast.distort.b[i,j]
        endfor
     endfor

  endif
end

