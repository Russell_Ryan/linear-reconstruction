;+
; Compress a 1-d index to be entirely continuous.
;
; We took many of these ideas from JBailin's posts on Fanning's
; website: http://www.idlcoyote.com/code_tips/valuelocate.html
;
; IND is the 1d index to compress
; COUNT is optional output, the number of unique indices returned.
;
; IND_UNIQ - can get original indices with: ind = ind_comp[ind_uniq]
;
; RETURN the unique 1-d indices
;
;-

function compress_index,ind,COUNT=count,IND_UNIQ=ind_uniq
  type=primative_type(ind,/typename)

;  if ~strcmp(type,'INTEGER',/fold) then begin
;     print,'IND must be an integer type.'
;     return,-1
;  endif

  ind_uniq=uniqify(ind,count=count)
  if count eq 1 then begin
     ind_comp=replicate(0l,n_elements(ind))
  endif else begin
     ind_comp=value_locate(ind_uniq,ind)
  endelse

  return,ind_comp
end
