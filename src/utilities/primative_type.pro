;+
; return the primative type:
;     0                 = UNDEFINED   (0)
;     1,2,3,12,13,14,15 = INTEGERS    (1)
;     4,5,6,9           = FLOATS      (2)
;     3                 = STRINGS     (3)
;     8                 = STRUCTURES  (4)
;     10                = POINTERS    (5)
;     11                = OBJECTS     (6)
;-


function primative_type,data,IDL_TYPE=idl_type,TYPENAME=typename

  ;get the IDL numeric type
  idl_type=size(data,/type)

  ;convert to the PRIMATIVE type
  case idl_type of
     0: prim_type=0             ;UNDEFINED
     1: prim_type=1             ;BYTE
     2: prim_type=1             ;INTEGER
     3: prim_type=1             ;LONG 
     4: prim_type=2             ;FLOAT
     5: prim_type=2             ;DOUBLE
     6: prim_type=2             ;COMPLEX (float)
     7: prim_type=3             ;STRING
     8: prim_type=4             ;POINTER
     9: prim_type=2             ;COMPLEX (double)
     10: prim_type=5            ;STRUCTURE
     11: prim_type=6            ;OBJECT
     12: prim_type=1            ;UINT
     13: prim_type=1            ;ULONG
     14: prim_type=1            ;LONG64
     15: prim_type=1            ;ULONG64
     else: prim_type=0          ;UNDEFINED
  endcase
  

  ;do we convert to a word
  if keyword_set(TYPENAME) then begin
     idl_type=size(data,/tname)
     case prim_type of
        0: prim_type='UNDEFINED'
        1: prim_type='INTEGER'
        2: prim_type='FLOAT'
        3: prim_type='STRING'
        4: prim_type='POINTER'
        5: prim_type='STRUCTURE'
        6: prim_type='OBJECT'
        else: prim_type='UNDEFINED'
     endcase
  endif

  
  return,prim_type
end

