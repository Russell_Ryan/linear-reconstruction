pro contamination,conf,axeconf,objects,flts
  
  nobj=n_elements(objects)
  if nobj eq 0 then return
  
  nimg=n_elements(flts)
  if nimg eq 0 then return

  lamb0=conf.lamb0
  lamb1=conf.lamb1
  dlamb=conf.dlamb


  nlam=ceil(float(lamb1-lamb0)/dlamb)+1
  limits=(findgen(nlam+1)-0.5)*dlamb+lamb0
  lam=findgen(nlam)*dlamb+lamb0

  ;get some things of the objects
  flux=fltarr(nobj)
  name=strarr(nobj)
  segid=lonarr(nobj)
  for j=0ul,nobj-1 do begin
     objects[j]->GetProperty,flux=fx,name=nm,segid=sg
     flux[j]=fx
     name[j]=nm
     segid[j]=sg
  endfor



  
  arr=obj_new('array')

  for i=0u,nimg-1 do begin
     
     odt=ptrarr(nobj)
     obj_flux=fltarr(nlam,nobj)
     etal=fltarr(nlam,nobj)
     flux=fltarr(nobj)

     for j=0ul,nobj-1 do begin
        tmp=objects[j]->read_odt(flts[i],axeconf,'A',count=count)
        if count ne 0 then begin

           ;compute flux for object on uniq pix. ignore wavelength
           val=decimate_array(tmp.xyg,tmp.val,loc=xyg,count=n)*flux[j]
           
           cat=replicate({obj:j,xyg:0ull,val:0.},n)
           cat.xyg=xyg
           cat.val=val
           arr->push,cat,/no_copy


           ;compute f(lam)
           l=value_locate(limits,tmp.wav)
           obj_flux[0,j]=decimate_array(l,tmp.val)


           ;save the catalog
           odt[j]=ptr_new(tmp,/no_copy)
        endif
     endfor
     arr->flush,dat=cat,count=count
     if count eq 0 then stop


     for j=0ul,nobj-1 do begin
        if ~ptr_valid(odt[j]) then stop
        tmp=*odt[j]
        
        
        g=where(cat.obj ne j,n)
        if n ne 0 then begin
           ;get entries that are not this object.
;           ocat=cat[g]
           oxyg=cat[g].xyg
           oobj=cat[g].obj
           oval=cat[g].val

           h=histogram(tmp.wav,bin=dlamb,reverse=ri,$
                       min=lamb0-0.5*dlamb,max=lamb1+0.5*dlamb)
           for k=0u,nlam-1 do begin
              xyg=uniqify(tmp[ri[ri[k]:ri[k+1]-1]].xyg,count=nxyg)
              for p=0,nxyg-1 do begin
                 gg=where(oxyg eq xyg[p],nn)
                 if nn ne 0 then $
                    etal[k,j]+=total(oval[gg]*flux[oobj[gg]])
              endfor
           endfor
        endif
     endfor


     ;compute contamination
     cont=etal/(etal+obj_flux)
     g=where(~finite(cont),n)
     if n ne 0 then cont[g]=0.

     
     ;write out the contamination
     flts[i]->GetProperty,file=file
     root=file_basename(file,'_flt.fits')
     outfile=root+'_cnt.fits'
    

     fits_open,outfile,fcb,/write
 
     ;create the 0th header and write the file
     h=['']
     sxaddpar,h,'IMAGE',file,' original FLT image'
     sxaddpar,h,'NOBJ',nobj,' number of objects'
     sxaddhist,'Contamination arrays',h
     config_updatehdr,conf,h
     
     mwrfits,0,outfile,h,/create
;     fits_write,fcb,0,h

     out=replicate({lam:0.,cont:0.},nlam)
     out.lam=lam
     for j=0u,nobj-1 do begin
        h=['']
        sxaddpar,h,'EXTNAME',name[j],' extension name'
        sxaddpar,h,'SEGID',segid[j],' ID from segmap'
        sxaddpar,h,'FLUX',flux[j],' lux from the direct image'
        out.cont=cont[*,j]
        mwrfits,out,outfile,h,/sil
;        fits_write,fcb,out,h,extname=name           
     endfor
;     fits_close,fcb


     


     ptr_free,odt
  endfor


  obj_destroy,arr





end
