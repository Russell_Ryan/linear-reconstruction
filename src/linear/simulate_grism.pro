pro simulate_grism_write,img,extname,ast,outfile
  mkhdr,h,img
  sxaddpar,h,'EXTNAME',extname,' extension'
  ast2hdr,h,ast
  mwrfits,img,outfile,h,/silent
end



pro simulate_grism,conf,axeconf,objects,flts

  ;number of objects
  nobj=n_elements(objects)
  if nobj eq 0 then return

  nimg=n_elements(flts)
  if nimg eq 0 then return


  ;which beam to simulate
  beam='A'

;  ;read file list
;  files = read_filelist(conf.fltlst,count=nimg)


  ;build the filter object
  filter=obj_new('filter',get_directory('LINEAR_FILTER')+conf.filter)
  if ~obj_valid(filter) then stop,'SIMULATE_GRISM> filter is invalid'

  ;read the SEDs
  sedlib=read_sedlst(conf,COUNT=count)

  ;get/set some properties of the output
  axeconf->GetProperty,sciext=sciext,uncext=uncext,dqaext=dqaext
  scitype=4                     ;in IDL notation (4=float, 5=double)
  unctype=4                     ;in IDL notation (4=float, 5=double)
  dqatype=2                     ;in IDL notation (2=int, 3=long)


  ;process each image
  for i=0u,nimg-1 do begin

     ;get the image size
     flts[i]->GetProperty,file=imgfile,ast=ast

     

     ;make the tables
;     make_tables,objects,flts[i],axeconf,conf

     ;make output image
     img=make_array(ast.naxis,type=scitype,value=0)
     
     ;fill with the objects
     for j=0ul,nobj-1 do begin
        odt=objects[j]->read_odt(flts[i],axeconf,beam,count=nodt,$
                                 sensunit=sensunit)
        
        if count ne 0 then begin
           objects[j]->GetProperty,name=name
           gg=where(strmatch(sedlib.name,name,/fold),nn)
           if nn ne 0 then begin
              if file_test(sedlib[gg].file) then begin
                 ;read the SED
                 readcol,sedlib[gg].file,l,f,f='d,d',/sil
                 l*=(1+sedlib[gg].z) ;redshift the spectrum
           
                 ;compute average flux
                 ave=filter->aveflux(l,f)*sensunit

                 ;rescale the spectrum
                 f*=(10.0d0^(-0.4*(sedlib[gg].mag+48.6))/ave)

                 ;put onto the pixels of the image
                 flam=interpol(f,l,odt.wav)

                 ;decimate over the pixel
                 val=decimate_array(odt.xyg,odt.val*flam,loc=xyg)
           
                 ;update the image
                 img[xyg]+=val
              endif else print,'SIMULATE_GRISM> SED file '+sedlib[gg].file+$
                               ' is not found.'
           endif else print,'SIMULATE_GRISM> Object '+name+$
                            ' is not the SED file.'
        endif
     endfor                     ;loop over j on objects


     npix=ast.naxis[0]*ast.naxis[1]

     ;check some properties
     g=where(img eq 0,n)
     if n eq npix then print,'SIMULATE_GRISM> Warning... The image, '+$
                             imgfile+', is all zeroes.'
     g=where(~finite(img),n)
     if n ne 0 then print,'SIMULATE_GRISM> Warning... The image, '+$
                          imgfile+', has '+strtrim(n,2)+' NaNs or INFs.'

;     ;do we have to add the gzip suffix?
;     zip=strsplit(imgfile,'.',/ext,count=n)
;     if strcmp(zip[n-1],'gzip',/fold) then imgfile=str
;
;     ;remove the 'gzip' suffix
;     imgfile=file_dirname(imgfile)+path_sep()+file_basename(imgfile,'.gz')


     ;check if the file already exists.
     if file_test(imgfile) then begin
        print,'SIMULATE_GRISM> File exists.  Overwriting'
        
;        print,'SIMULATE_GRISM> The file, '+imgfile+$
;              ', exists.  Appending a suffix.'
;        loc=stregex(imgfile,'_[0-9]*\.(fits)',length=len);
;
;        if loc eq -1 then begin
;           loc=stregex(imgfile,'\.(fits)',len=l)
;           suf=strmid(imgfile,loc+1,strlen(imgfile)-2)
;           num=1
;        endif else begin
;           suf=strmid(imgfile,loc+1,loc+len-2)
;           pos=strpos(suf,'.')
;           suf=strmid(suf,pos+1,strlen(suf)-2)
;           num=fix(strmid(suf,0,pos))
;
;        endelse
;        root=strmid(imgfile,0,loc)
;        imgfile=root+'_'+strtrim(num+1,2)+'.'+suf
     endif     
     



     ;write out the images
     h0=['']
     sxaddpar,h0,'NOBJ',nobj,' number of objects'
     sxaddhist,'Created by linear',h0
     config_updatehdr,conf,h0
     
     ;write the zeroth extnesion
     mwrfits,0,imgfile,h0,/create
     
     ;write the SCIENCE extension
     simulate_grism_write,img,sciext,ast,imgfile
     
     ;make UNCERTAINTY extension
     img=make_array(ast.naxis,value=1,type=unctype)
     simulate_grism_write,img,uncext,ast,imgfile

     ;make DQA extension
     img=make_array(ast.naxis,value=0,type=dqatype)
     simulate_grism_write,img,dqaext,ast,imgfile


     ;zip the file?
     if conf.zip then begin
        spawn,'gzip -f '+imgfile,res,err
        if err ne '' then print,'SIMULATE_GRISM> '+err
     endif

  endfor

  ;clean up memor usage
  obj_destroy,filter



end
