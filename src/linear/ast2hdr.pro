pro ast2hdr_matrix,h,name,mat
  sz=size(mat,/dim)
  if sz[0] ne 1 then begin
     sxaddpar,h,name+'_ORDER',sz[0]-1
     for i=0,sz[0]-1 do begin
        ii=strtrim(i,2)
        for j=0,sz[1]-1 do begin
           jj=strtrim(j,2)
           if i+j ge 2 && i+j lt sz[0] then $
              sxaddpar,h,name+'_'+ii+'_'+jj,mat[i,j]
        endfor
     endfor
  endif
end

pro ast2hdr,h,ast
  catch,error
  if error ne 0 then begin

     stop,'PARAMETER MISSING?'
  endif
  


  ok=array_equal(sxpar(h,'NAXIS*'),ast.naxis)
  if ~ok then stop,'update naxis'

  ctype=strmid(ast.ctype,0,8)
  if ~array_equal(ctype,['RA---TAN','DEC--TAN']) then begin
     print,'AST2HDR> Only allowed to work with [RA---TAN,DEC--TAN]'
     return
  endif
  
  sxaddpar,h,'EQUINOX',ast.equinox
  sxaddpar,h,'CD1_1',ast.cd[0,0]
  sxaddpar,h,'CD2_1',ast.cd[1,0]
  sxaddpar,h,'CD1_2',ast.cd[0,1]
  sxaddpar,h,'CD2_2',ast.cd[1,1]
  sxaddpar,h,'CRPIX1',ast.crpix[0]
  sxaddpar,h,'CRPIX2',ast.crpix[1]
  sxaddpar,h,'CRVAL1',ast.crval[0]
  sxaddpar,h,'CRVAL2',ast.crval[1]
  sxaddpar,h,'RADESYS',ast.radecsys

    
  if array_equal(ast.ctype,['RA---TAN-SIP','DEC--TAN-SIP']) then begin

     has_distort=tag_exist(ast,'distort')
     if has_distort && strcmp(ast.distort.name,'SIP',/fold) then begin
        ;has right CTYPE and distortion
        sxaddpar,h,'CTYPE1',ast.ctype[0]
        sxaddpar,h,'CTYPE2',ast.ctype[1]
        
        ast2hdr_matrix,h,'A',ast.distort.a
        ast2hdr_matrix,h,'B',ast.distort.b
        ast2hdr_matrix,h,'AP',ast.distort.ap
        ast2hdr_matrix,h,'BP',ast.distort.bp
        
     endif else begin
        ;CTYPE says SIP, but does not have SIP distortion
        print,'AST2HDR> CTYPE claims to be SIP, but SIP coefs are absent.'
        print,'         defaulting to no distortion.'      
        sxaddpar,h,'CTYPE1','RA---TAN'
        sxaddpar,h,'CTYPE2','DEC--TAN'
     endelse
  endif else begin
     ;CTYPE says no distortion
     sxaddpar,h,'CTYPE1',ast.ctype[0]
     sxaddpar,h,'CTYPE2',ast.ctype[1]
  endelse
     

  
end
