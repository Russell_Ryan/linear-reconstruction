

;+
; extract the optimal grism spectrum from a set of observations
;-


pro extract_grism,conf,axeconf,objects,images

  t1=systime(/sec)

  ;set some defaults
  silent=1b                     ;be very, very quiet
  beam='A'                      ;extract with beam='A'

  ;number of images
  nimg=n_elements(images)
  if nimg eq 0 then return

  nobj=n_elements(objects)
  if nobj eq 0 then return

  ;get some things from the aXe config
  axeconf->GetProperty,sciext=sciext,uncext=uncext,dqaext=dqaext,$
                       sensunit=sensunit
  dqa_over=1024                 ;DQA value for overlapping orders



  ;read the filter to get the photplam value
  filt=obj_new('filter',get_directory('LINEAR_FILTER')+conf.filter)
  filt->GetProperty,photplam=photplam
  obj_destroy,filt

  ;compute conversion from DIRECT image units to ergs/s/cm2/A
  zero=tag_exist(conf,'zero')?conf.zero:25.
  photflam=10d^(-0.4*zero)*0.1088/(sensunit*photplam*photplam)


  ;get properties of the objects
  cat=replicate({flux:0.,nwav:0u},nobj)
  for i=0,nobj-1 do begin
     objects[i]->GetProperty,flux=f,nlamb=nl
     cat[i].flux=photflam*f
     cat[i].nwav=nl
  endfor
  
  ;print a warning
  if ~strcmp(conf.wcstype,'fltlst',/fold) then begin
     print,'EXTRACT_GRISM> Warning... We require: WCSTYPE=fltlst in extraction.'
     print,'               Resetting for you.'
     conf.wcstype='fltlst'
  endif


  ;set up some variables for the looping
  mmat=obj_new('array')
  bvec=obj_new('array')
  obs=obj_new('array')


  ;do some things for variable wavelength arrays
  cwav=total(cat.nwav,/cum,/pres) ;cumulative number of unknowns
  npar=cwav[nobj-1]               ;number of unknowns
  cwav=[0,cwav[0:nobj-2]]         ;shift the unknowns


  ;loop over images
  for i=0u,nimg-1 do begin
     
;     webmonitor,alt.file,i

     ;read the images
     images[i]->readfits,sci,scihdr,sciext,/sil
     images[i]->readfits,unc,unchdr,uncext,/sil
     images[i]->readfits,dqa,dqahdr,dqaext,/sil

     ;find the current bad pixel list
     bpx=where(dqa ne 0,nbpx)

     
     ;number of pixels in the image
     npix=n_elements(sci)


     for j=0u,nobj-1 do begin
        ;read the ODT
        odt=objects[j]->read_odt(images[i],axeconf,beam,count=nodt,sens=su)

        if nodt ne 0 then begin
           ;apply the BPX
           omt=objects[j]->read_omt(images[i],axeconf,beam,count=nomt)

           apply_mask=1b
           case 1 of
              nomt ne 0 && nbpx ne 0: xym=uniqify([bpx,omt.xyg])
              nomt ne 0 && nbpx eq 0: xym=omt.xyg
              nomt eq 0 && nbpx eq 0: xym=bpx
              nomt eq 0 && nbpx ne 0: apply_mask=0b
           endcase
           if apply_mask then begin
              g=nonmatch(odt.xyg,xym,count=n)
              if n eq 0 then stop
              odt=odt[g]        ;remove the bad pixels
           endif


           ;Extract the values for faster access.  Stefano pointed 
           ;out this short-coming of IDL
           odt_xyg=odt.xyg
           odt_wav=odt.wav
           odt_val=odt.val
           delvarx,odt          ;clean up the memory

           ;limits for the wavelength integration
           objects[j]->wavelengths,limits=lim


           ;find points in the intervals
           mn=min(lim,max=mx)
           g=where(odt_wav ge mn and odt_wav le mx,count)
           if count ne 0 then begin
              ;only keep the good values
              select,g,odt_xyg,odt_wav,odt_val
              
      
              lam_id=value_locate(lim,odt_wav) ;wavelength indices

              ;compute the matrix elements for these points
              ii=odt_xyg+npix*i ;ragged grid
              jj=lam_id+cwav[j] ;ragged grid


              ;matrix elements
              ij=temporary(jj)+npar*temporary(ii)
              aij=decimate_array(ij,odt_val,loc=ijuniq,count=nij)

              ;compute unique matrix elements and pixel 
              iiuniq=ijuniq  /  npar
              jjuniq=ijuniq mod npar                                
              xyg=iiuniq mod npix         ;compute grism pixel

              ;update the matrix
              foo=replicate({ij:0ull,aij:0.},nij)
              foo.ij=temporary(ijuniq)
              foo.aij=temporary(aij)
              mmat->push,foo,/no


              ;store the observations
              opt=replicate({sci:0.,unc:0.,xyg:0ull},nij)
              opt.sci=sci[xyg]
              opt.unc=unc[xyg]
              opt.xyg=temporary(xyg)
              obs->push,opt,/no
        

           endif
        endif
     endfor                     ;loop on j over OBJECT

     ;only keep the unique xyg for this image
     obs->extract,dat,/flush,count=ndat     
     q=uniq(dat.xyg,sort(dat.xyg))
     dat=dat[q]

     ;save this data to the b-vector
     bvec->push,dat,/no_copy
     
  endfor                        ;loop on i over NFLT
  ;destroy the temporary array
  obj_destroy,obs



  ;extract the data 
  obj_destroy,mmat,data=m,count=nmat
  obj_destroy,bvec,data=b,count=nobs

  ;get the 2d indices of the matrix
  jj=compress_index(m.ij mod npar,count=nj,ind_uniq=jjuniq)
  ii=compress_index(m.ij  /  npar,count=ni,ind_uniq=iiuniq)

  ;form the parameters of the matrix
  aij=m.aij/b[ii].unc  
  vec=b.sci/b.unc
 
  ;clean up memory usage
  delvarx,m,b


; pass in objects/images.  return ii,jj,aij,b
;----- ABOVE IS TO BUILD THE MATRIX ----




  ;matrix dimensions
  dim=[ni,nj]


; The damp target math is the same as applying an initial guess. the 
; difference is how stefano implemented it.  he saw the damp target 
; as necessary iff damp != 0, but we should probably apply this shift 
; irrespective


  ;add a damping target
;  damp_solution=tag_exist(conf,'damp') && (conf.damp ne 0.)
;  if damp_solution then begin
;     print,'EXTRACT_GRISM> Setting damping target to 0'
;
;     damp_target=0.
;     damp_target=replicate(damp_target,nlam,nobj)
;     damp_target=reform(damp_target,npar,/overwrite)
;
;     for k=0ul,nmat-1 do vec[ii[k]]-=(damp_target[jj[k]]*aij[k])
;  endif



  ;set an initial guess
;  ss=oconf->sensitivity(lam)
;  gg=where(ss gt 0.001)
;  x0=fltarr(nlam,nobj)
;  for i=0,nobj-1 do x0[gg,i]=flam[i]
;  x0=reform(x0,npar)

  ;New LSQR
  restmp=lsqr(ii,jj,aij,dim,vec,err=errtmp,damp=conf.damp,$
              istop=istop,imsg=imsg)
  
  if istop lt 0 then stop,'LSQR error: '+imsg



  ;properties of the output
  value=!values.f_nan           ;defaut value



  ;remove the damping target from the solution
;;  if damp_solution then restmp+=damp_target

  ;get indices for a ragged array
  obj_id=value_locate(cwav,jjuniq)
  lam_id=jjuniq-cwav[obj_id]
  hobj=histogram(obj_id,reverse=ri) ;just for populating the output



  t2=systime(/sec)


  

  
  ;write out the results
  h=['']
  sxaddpar,h,'SIMPLE','T',' Dummy created by linear'
  sxaddpar,h,'BITPIX',8,' Primary header'
  sxaddpar,h,'NAXIS',0,' No data associated with this header'
  sxaddpar,h,'EXTEND','T',' Extensions will be present'
  sxaddpar,h,'NOBJ',nobj,' Number of objects'
  sxaddpar,h,'NIMG',nimg,' Number of grism images'
  sxaddpar,h,'NPIX',nobs,' Number of extracted pixels'
  sxaddpar,h,'UNITS',sensunit,' erg/s/cm**2/A'
  sxaddpar,h,'ISTOP',istop,' LSQR stopping criterion'
  sxaddpar,h,'IMSG',imsg,' LSQR stopping message'
  sxaddpar,h,'RUNTIME',t2-t1,' Run time in seconds'
  sxaddhist,'extracted spectra',h

  ;add the configuration settings to the header
  config_updatehdr,conf,hdr


  ;write the 0th extension
  mwrfits,0,conf.output,h,/create
  

  ;write each object
  for i=0,nobj-1 do begin
     objects[i]->GetProperty,name=name,segid=segid,dlamb=dlamb
     objects[i]->wavelengths,lambda=lam

     ;ragged arrays
     out={lam:lam,$
          flam:make_array(cat[i].nwav,value=value),$
          ferr:make_array(cat[i].nwav,value=value)}
     if hobj[i] ne 0 then begin
        g=ri[ri[i]:ri[i+1]-1]
        out.flam[lam_id[g]]=restmp[g]
        out.ferr[lam_id[g]]=errtmp[g]
     endif
     
     
   
     ;make the header
     h=['']    
     sxaddpar,h,'EXTNAME',strtrim(name,2),' extension name'
     sxaddpar,h,'SEGID',segid,' ID from segmap'
     sxaddpar,h,'flux',cat[i].flux,' native flux of image'
     sxaddpar,h,'nlamb',cat[i].nwav,' number of requested wavelength points'
     sxaddpar,h,'dlamb',dlamb,' resolution in A'

     ;update the file
     mwrfits,out,conf.output,h,/silent
  endfor

end
