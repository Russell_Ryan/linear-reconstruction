;+
; An object to do all operations with a filter curve
;-


pro filter::SetProperty,ZEROPOINT=zeropoint,SILENT=silent
  ;+
  ; set some values to the object
  ;-
  if n_elements(ZEROPOINT) ne 0 then self.zero=zeropoint
  if n_elements(SILENT)    ne 0 then self.verbose=~silent
end
pro filter::GetProperty,ZEROPOINT=zeropoint,SILENT=silent,PHOTPLAM=photplam
  ;+ 
  ; retrieve some values from the object
  ;-
  if arg_present(ZEROPOINT) then zeropoint=self.zero
  if arg_present(SILENT)    then silent=~self.verbose
  if arg_present(PHOTPLAM)  then photplam=self.photplam
end

function filter::integrate,xx,yy
  ;+
  ; a helper function to do the numerical intergration.  this is to 
  ; ensure all integrations are done consistently, so if a new 
  ; algorithm is to be used (such as trapezoidal rule), this can 
  ; be easily updated for everything
  ;+

  ;create dummy variables
  x=xx & y=yy

  ;use int_tabulated:
  f=int_tabulated(temporary(x),temporary(y),/double,/sort)
  return,f
end



function filter::aveflux,lam,flam,IS_FNU=is_fnu,MAGNITUDE=magnitude,$
                         EXTRAP=extrap
  ;+
  ; compute bandpass-averaged flux in AB units
  ;
  ; NOTA BENE: 
  ;  tran has been rescaled as tran -> tran/freq/norm to save on 
  ;  calculations at this stage.
  ;-


  ;check that the object is initialized
  if ~self.init then return,!values.f_nan

  ;check for extrapolations
  mnl=min(lam,max=mxl)

  ;determine if we are extrapolating
  extrap=self.maxl gt mxl || self.minl lt mnl
  if extrap && self.verbose then $
     print,'FILTER::AVEFLUX> Warning: Extrapolating the SED.'

  ;compute the f_nu spectrum
  if keyword_set(IS_FNU) then begin
     fnu=interpol(flam,lam,*self.lamb)
  endif else begin
     fnu=interpol(flam,lam,*self.lamb)*(*self.lamb)/(*self.freq)
  endelse

  ;perform integration
  ave=self->integrate(*self.freq,fnu*(*self.tran))

  ;throw an error...
  if ave lt 0 && self.verbose then $
     print,'FILTER::AVEFLUX> Warning: average flux is negative.'

  
  if ~finite(ave) && self.verbose then $
     stop,'FILTER::AVEFLUX> Warning: average flux is NaN'

  ;convert to magnitudes if need-be
  if keyword_set(MAGNITUDE) then ave=-2.5*alog10(ave)+self.zero
  return,ave
end




pro filter::read,file,ZEROPOINT=zeropoint,OKAY=okay
  ;+
  ; read a filter curve
  ;
  ; NOTE:
  ;     The format must be:
  ;     
  ;     100                  # number of elements in filter
  ;     10000  1e-2          # wavelength in A and transmission
  ;
  ;-



  ;check datatype of file
  if size(file,/type) ne 7 then begin
     print,'FILTER::READ> file should be a string.'
     okay=0b
     return
  endif


  ;check for a valid file
  if ~file_test(file) then begin
     print,'FILTER::READ> file ('+file+') not found.'
     okay=0b
     return
  endif


  ;set the speed of light
  c=2.99792458d10               ;cm/s
  

  ;open the filter curve file
  openr,lun,file,error=error,/get_lun
  okay=error eq 0
  if ~okay then return


  ;in case the user wants to overwrite the current state
  if self.init then ptr_free,self.lamb,self.freq,self.tran

  ;read the data
  n=0l & readf,lun,n
  data=replicate({FILTERFILE},n)
  readf,lun,data
  free_lun,lun

  ;extract the data
 ; lamb=data.wave                ;assume wavelength is in Angstrom
 ; tran=data.tran                ;store
 ; delvarx,data                  ;clean up old memor

  ;compute the pivot wavelength
  num=self->integrate(data.lamb,data.tran*data.lamb)
  den=self->integrate(data.lamb,data.tran/data.lamb)
  self.photplam=sqrt(num/den)

  ;normalize the curve
  freq=c*(1.0d8/data.lamb)      ;will be in Hertz
  data.tran/=freq               ;normalize per energy (Planck const cancels)
  data.tran/=self->integrate(freq,data.tran)
  
  ;record wavelengths
  self.minl=min(data.lamb)
  self.maxl=max(data.lamb)

  ;record data to the self structure
  *self.freq=freq
  *self.lamb=data.lamb
  *self.tran=data.tran
  self.zero=(size(zeropoint,/type) ne 0)?zeropoint:0.

  ;set an init flag
  self.init=1b
  
end
  

pro filter::cleanup
  ;+
  ; obligatory object lifecycle method
  ;-
  ptr_free,self.lamb,self.tran,self.freq
end
function filter::init,file,ZEROPOINT=zeropoint,SILENT=silent
  ;+
  ; obligatory object to build the filter curve
  ;-

  ;output flag
  okay=1b

  ;set the verbose setting
  self.verbose=~keyword_set(SILENT)

  ;allocate pointers
  self.lamb=ptr_new(/allocate)
  self.freq=ptr_new(/allocate)
  self.tran=ptr_new(/allocate)

  ;load data if need-be
  if size(file,/type) eq 7 then self->read,file,ZEROPOINT=zeropoint,okay=okay
     

  ;return the flag
  return,okay
end


pro filterfile__define          ;helper structure to read filter curves
  ;+
  ; This specfies the format of the filter file
  ;-
  _={FILTERFILE,$
     lamb:0d,$
     tran:0d $
    }
end


pro filter__define
  _={FILTER,$
     init:0b,$                  ;flag to show that the object is init
     file:'',$                  ;name of filter curve
     verbose:0b,$               ;flag to be verbose
     zero:0.,$                  ;magnitude zeropoint
     minl:0.,$                  ;min wavelength
     maxl:0.,$                  ;max wavelength
     photplam:0d,$              ;pivot wavelength
     lamb:ptr_new(),$           ;wavelength array
     freq:ptr_new(),$           ;frequency array
     tran:ptr_new() $           ;transmission array
    }
end
  
