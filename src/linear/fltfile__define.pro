pro fltfile::readfits,img,hdr,ext,SILENT=silent
  img=mrdfits(self.file,ext,hdr,SILENT=silent)
end


function fltfile::xy2xy,xd,yd,ast
  ;wanna subsume xy2xy into this
  xy2xy,reform(xd),reform(yd),ast,xf,yf,*self.ast
  xyf=transpose([[temporary(xf)],[temporary(yf)]])
  return,xyf
end


function fltfile::__extast__,file,ext,ast
  if ~file_test(file) then begin
     print,'FLTFILE::__EXTAST__> File ('+file+') not found.'
     return,0b
  endif
  hdr=headfits(file,ext=ext)
  extast,hdr,ast,noparam
  if noparam le 0 then begin
     print,'FLTFILE::__EXTAST__> No valid astrometry in '+file+'.'
     return,0b
  endif

  return,1b
end
pro fltfile::GetProperty,FILE=file,NAXIS=naxis,AST=ast
  if arg_present(FILE) then file=self.file
  if arg_present(NAXIS) then naxis=(*self.ast).naxis
  if arg_present(AST) then ast=(*self.ast)
end



pro fltfile::cleanup
  ptr_free,self.ast
end
function fltfile::init,input,extname,FOVFILE=fovfile
                       
  
  if keyword_set(FOVFILE) then begin
     if size(input,/type) ne 8 && $
        ~tag_exist(input,'root') && $
        ~tag_exist(input,'crval1') && $
        ~tag_exist(input,'crval2') && $
        ~tag_exist(input,'orientat') then begin
        print,'FLTFILE::INIT> Invalid input.'
        return,0b
     endif

     ;load from a catalog
     self.file=input.root+'_flt.fits'
     if ~self->__extast__(fovfile,extname,ast) then return,0b

     ;make a rotation matrix
     cs=cos(input.orientat*!DPI/180)
     sn=sin(input.orientat*!DPI/180)
     rot=[[+cs,-sn],[+sn,+cs]]  ;rotation matrix
     
     ;fill the astrometry
     ast.crval[0]=input.crval1
     ast.crval[1]=input.crval2
     ast.cd #= rot

  endif else begin
     if size(input,/type) ne 7 || n_elements(input) ne 1 then begin
        print,'FLTFILE::INIT> Invalid input.'
        return,0b
     endif
     
     self.file=input

     ;read the astrometry
     if ~self->__extast__(input,extname,ast) then return,0b

  endelse

  ;check for SIP
  if tag_exist(ast,'DISTORT') && strcmp(ast.distort.name,'SIP') then begin
     na=(ast.distort.a)[0]
     nb=(ast.distort.b)[0]
     nap=(ast.distort.ap)[0]
     nbp=(ast.distort.bp)[0]
          
     has_forward=(na ne 1 || nb ne 1)
     has_reverse=(nap ne 1 || nbp ne 1)
     
     if has_forward && ~has_reverse then $
        print,'FLTFILE::INIT> Warning: Has only forward SIP.  Both are needed'
     if ~has_forward && has_reverse then $
        print,'FLTFILE::INIT> Warning: Has only reverse SIP.  Both are needed'
  endif

  ;record the astrometry
  self.ast=ptr_new(ast,/no_copy)

  return,1b
end

pro fltfile__define
  _={FLTFILE,$
     file:'',$
     ast:ptr_new() $
    }
end
