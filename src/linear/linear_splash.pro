pro linear_splash,conf

  print
  print,'Linear Extraction and Simulation of Slitless Spectroscopy'
  print
  tags=tag_names(conf)
  len=max(strlen(tags))+2
  tags=string(tags,f='(A'+string(len,f='(A0)')+')')
  for i=0,n_elements(tags)-1 do begin
     print,tags[i],'   ',strtrim(conf.(i),2)
  endfor
  print



end
