;+
;
; disperse a given set of (x,y) pairs for a series of wavelengths and 
; return the geometric parameters (x,y,area).  The (x,y) pairs are in the
; dispersed image frame. 
; 
;
; INPUTS:
;    xy_direct: a 2xN-element floating type array specifying the
;               corners of a polygon to disperse (not closed),
;               therefore N is the number of vertices. the polygon 
;               needn't actually represent a polygon, but
;               rather could encode a myriad of (x,y) pairs
;    lambda: a floating value of wavelength (in A)
;    beam: an object of type AXEBEAM (see axebeam__define.pro)
;
; OPTIONAL INPUTS:
;    imagesize: the size of the dispersed image.  Needed for the 
;               polygon-clipping of JD Smith. Default=[1014,1014]
;
; OUTPUTS:
;    an array of PAT structure (pixel area table), see pat__define.pro
;
; OPTIONAL OUTPUTS:
;    count: number of output grism pixels that have been affected
;
;-




function spec_drizzle_loop,xg,yg,imagesize,COUNT=count,DEBUG=debug
  ;+
  ; explicity loop over wavelength... important if xg/yg is large
  ;-
  arr=obj_new('array')
  sz=size(xg,/dim)
  ndim=size(xg,/n_dim)
  if ndim eq 1 then sz=[sz,1]

  for ii=0L,sz[1]-1 do begin
     ;compute fractional areas using polyfillaa (JD Smith)
     pi=[0,sz[0]]
     pix=polyfillaa(xg[*,ii],yg[*,ii],imagesize[0],imagesize[1],$
                    areas=frac,poly_indices=pi)
     
     ;only run if objects are in bounds
     if pix[0] ne -1 then begin
        ;for some reason, some areas are NaNs.  Something in 
        ;JD Smith's code?  I guess NaN area is area=0?
        bad=where(~finite(frac),nbad)
        if nbad ne 0 then frac[bad]=0.


        ;build the Pixel Area Table (PAT)
        pat=replicate({PAT},n_elements(pix))
        pat.lam=ii
        pat.xyg=pix
        pat.val=frac

        arr->push,pat,/no_copy
     endif
  endfor

  ;extract the data
  obj_destroy,arr,data=pat,count=count

  return,pat
end
     
function spec_drizzle_vector,xg,yg,imagesize,COUNT=count,DEBUG=debug
  ;+
  ; compute pixel areas as a vector-based calculation
  ;-

  sz=size(xg,/dim)
  nvert=sz[0]
  nlamb=sz[1]
  

  

  ;compute fractional areas using JD Smith code
  pi=nvert*lindgen(nlamb+1)
  pix=polyfillaa(xg,yg,imagesize[0],imagesize[1],areas=frac,poly_indices=pi,$
                 debug=debug)

  ;only work if we have good pixels
  if pix[0] ne -1 then begin
     ;number of pixels that got affected
     count=n_elements(pix)
     
     ;for some reason, some areas are NaNs.  Something in 
     ;JD Smith's code?  I guess NaN area is area=0?
     bad=where(~finite(frac),nbad)
     if nbad ne 0 then frac[bad]=0.

     ;build the Pixel Area Table (PAT)
     pat=replicate({PAT},count)
     if nlamb ne n_elements(pi)-1 then stop,'WTF?'
     
     i=0ul                      ;a dummy counter
     for j=0UL,nlamb-1 do begin ;n_elements(pi)-2 do begin
        
        ;number of pixels that were affected for this pixel
        n=pi[j+1]-pi[j]

        if n ne 0 then begin
           ;fill the PAT
           pat[i:i+n-1].lam=j
           pat[i:i+n-1].xyg=pix[pi[j]:pi[j+1]-1]
           pat[i:i+n-1].val=frac[pi[j]:pi[j+1]-1]
;           print,total(pat[i:i+n-1].val)

        ;this should be true:
        ;1./total(pat[i:i+n-1].val) == ratio of pixel areas

           ;update the counter
           i+=n
        endif

     endfor
     ;just a quick error checking
     if i ne count then begin
        print,'An unexpected error occurred in spec_drizzle_vector.'
        stop,'Debug.'
     endif
  endif else begin
     ;if no pixels were affected
     count=0
     pat=-1
  endelse

  return,pat
end


function spec_drizzle,xyd,lambda,oconf,beam,IMAGESIZE=imagesize,$
                      COUNT=count,LOOP=loop,DEBUG=debug
  ;+
  ; This is the actual calculation
  ;-
  
  ;set a default for WFC3/IR
  if n_elements(IMAGESIZE) ne 2 then imagesize=[1014,1014]

  ;make sure the polygon is valid
  if size(xyd,/n_dim) ne 2 then begin
     print,'xyd is not a polygon'
     count=0
     return,-1
  endif

  ;check the beam is valid
  if size(oconf,/type) ne 11 || ~obj_isa(oconf,'axeconfig') then begin
     print,'oconf is not a valid aXeCONFIG object.'
     count=0
     return,-1
  endif

  ;get size of polygon
  sz=size(xyd,/dim)



  ;get numbers
  nvert=sz[1]                   ;number of vertices
  nlamb=n_elements(lambda)      ;number of wavelength
  ntot=nlamb*nvert              ;total number of output vertices

  ;check if the wavelength is valid
  if nlamb eq 0 then begin
     print,'lambda does not exist.'
     count=0
     return,-1
  endif



  
  ;convert from direct image coordinates to grism image
  ;coordinates for a given wavelength
  xyg=oconf->xyd2xyg(xyd,lambda,/double,beam=beam)

  ;reform 
  xg=reform(xyg[0,*,*],nvert,nlamb)
  yg=reform(xyg[1,*,*],nvert,nlamb)



  
  ;remove pixels which are out of bounds
  oconf->GetProperty,xrange=xrange,yrange=yrange
  xave=total(xg,1)/nvert
  yave=total(yg,1)/nvert
  g=where(xave ge xrange[0] and xave le xrange[1] and $
          yave ge yrange[0] and yave le yrange[1],num)
  

  ;only do it if there are good pixels
  if num eq 0 then begin
     count=0
     pat=-1
  endif else begin
     ;extract the pixels which could fall and fix out of range pixels
     xg=0>xg[*,g]<(imagesize[0]-1)
     yg=0>yg[*,g]<(imagesize[1]-1)


     ;add this as a switch
     if num eq 1 || keyword_set(LOOP) then begin
        pat=spec_drizzle_loop(xg,yg,imagesize,count=count,DEBUG=debug)
     endif else begin
        pat=spec_drizzle_vector(xg,yg,imagesize,count=count,debug=debug)
     endelse
     
;     ;index the wavelength
;     pat.wav=lambda[pat.wav]

  endelse


  return,pat
end
