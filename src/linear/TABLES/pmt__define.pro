;+
; Pixel-Mask Table (PMT)
; Very similar to the PAT, but only keeps pixels to mask.
;-

;obj:0u,$
;beam:''

pro pmt__define
  _={PMT,$
     inherits xyg,$
     inherits val $
    }
end
