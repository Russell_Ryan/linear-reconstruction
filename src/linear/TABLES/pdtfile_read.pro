function pdtfile_read,file,HDR=hdr,COMPRESS=compress,_EXTRA=_extra
                      ;IMAGE=image
;  ;hardcode
;  hdr=create_struct(name='PDTFILE_HEADER')
;  tab=create_struct(name='PDT')


  ;check that file exists.
  if ~file_test(file) then begin
     print,'PDTFILE_READ> File not found.'
     return,-1
  endif


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;find the suffix
  toks=strsplit(file_basename(file),'.',/ext,count=count)
  last=toks[count-1]
  
  ;figure out which file it is
  options=['pdt','pmt']
  match,strlowcase(toks),options,tid,oid,count=count

  ;check the number of matches
  case count of
     0: print,'TABLES_WRITE> Only valid file types: '+strjoin(options,',')
     1: type=options[oid[0]]
     else: print,'TABLES_WRITE> Filename has multiple types. Cannot write.'
  endcase
  if size(type,/type) eq 0 then return,-1

  ;make the file header
  hdr=create_struct(name=type+'file_header')
  tab=create_struct(name=type)



  ;name the output file
  infile=file
  compress=keyword_set(COMPRESS) eq 1
  gzip=strcmp(last,'gz',/fold)

  if ~compress &&  gzip then compress=1b

;
;  case compress of
;     0: if ~gzip then infile+='.gz'
;     1: if  gzip then compress=1b
;  endcase



  ;check to compress the file
  compress=keyword_set(COMPRESS)
  if ~compress then begin
     tok=strsplit(file,'.',/ext,count=n)
     compress=strcmp(tok[n-1],'gz',/fold)
  endif     

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




  ;open the file to read
  openr,lun,infile,/get_lun,error=error,COMPRESS=compress
  if error ne 0 then begin
     print,'PDTFILE_READ> ERROR:'
     print,!error_state.msg
     stop
  endif

  ;read the header
  readu,lun,hdr

  ;paste in keyword?
  n=n_tags(_extra)
  if n ne 0 then begin
     etags=tag_names(_extra)
     htags=tag_names(hdr)
     elen=strlen(etags)
     for i=0,n-1 do begin
        htag=strmid(htags,0,elen[i])
        gg=(where(strmatch(htag,etags[i],/fold),nn))[0]
        if nn eq 1 then hdr.(gg)=_extra.(i)
     endfor
  endif

  ;read the data, if valid
  if tag_exist(hdr,'count') && hdr.count gt 0 then begin
     ;read the data
     tab=replicate(tab,hdr.count)
     readu,lun,tab
  endif else tab=-1

  ;close the file
  free_lun,lun

  return,tab
end



  
