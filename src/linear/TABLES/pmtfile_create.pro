;+
; CREATE_PMT
;
; build a Pixel mask table for a given object in a given image
; uses convex hulls to speed things up
;-



;this is more like create_pdt_run
pro pmtfile_create,obj,lam,pet,oconf,conf,fltast,segast,beam
                        

  ;set a default count to return
  count=0L
  
  ;compute the delta
;  nlam=n_elements(lam)
;  dlam=lam[1:nlam-1]-lam[0:nlam-2]
;  if ~array_equal(dlam,dlam[0]) then begin
;     print,'PMTFILE_CREATE> The wavelength grid must be linear.'
;     return
;  endif
;  dlam=dlam[0]

  ;get the file name
  pmtfile=tables_name(obj,beam,fltast.file,/create,dir=conf.savedir,$
                      compress=conf.zip)

  ;find the pixels
  gpix=where(pet.obj eq obj,npix)
  if npix eq 0 then begin
     print,'PMTFILE_CREATE> No objects that match ID.'
     if keyword_set(WRITE) then stop,'GET THIS DONE'
     return,
  endif

  ;print a message?



  ;uniqify
  xyd=uniqify(pet[gpix].xyd)
  
  ;compute the pixels (x,y)
  xd=xyd mod segast.naxis[1]
  yd=xyd  /  segast.naxis[1]
  
  ;add another pixel to the top and right
  xd=[xd,xd,xd+1]
  yd=[yd,yd+1,yd]
  
  ;make back into 1d index to keep unique pairs
  xyd=xd+yd*segast.naxis[1]
  xyd=uniqify(xyd,count=nxyd)

  ;make back into 2
  xd=xyd mod segast.naxis[1]
  yd=xyd  /  segast.naxis[1]
  
  ;must loop over islands
  print,'PMTFILE_CREATE> Should loop over islands.'


  ;compute the hull
  triangulate,xd,yd,tri,b

  ;transform the FLT space
  xy2xy,x[b],yd[b],segast,xf,yf,fltast

  ;drizzle the hull
  pat=spec_drizzle([xf,yf],lam,oconf,beam,count=num,imagesize=fltast.naxis)
   
  ;compute maxima
  val=decimate_array(pat.xyg,pat.val,count=count,mode='max',locations=xyg)
  
  ;make a PMT
  if count ne 0 then begin
     pmt=replicate({PMT},count)
     pmt.xyg=xyg
     pmt.val=val
  endif

  stop,'WRITE THE FILE'


  ;print a message

end

