;+
; create the directory name to navigate the directory trees for the 
; binary tables 
;-

function tables_dir,beam,fits,DIR=dir,CREATE=create

  ;get the path separator  
  psep=path_sep()


  ;find the suffix
  toks=strsplit(file_basename(fits),'.',/ext,count=count)
  g=where(strmatch(toks,'fits',/fold),count)
  root=toks[0:g[0]-1]

  ;make the tokens for the file name
  outdir=strjoin(keyword_set(DIR)?[dir,root,beam]:[root,beam],psep)+psep

  ;make the dir if needbe
  if ~file_test(outdir,/dir) && keyword_set(CREATE) then file_mkdir,outdir

  return,outdir
end
