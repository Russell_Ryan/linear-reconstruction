pro pdtfile_create,obj,lam,pet0,oconf,conf,fltast,segast,beam,$
                        COUNT=count,IMAGE=image
  ;+
  ; main worker program. This can be called as is, or driven by the 
  ; multithreading built into pdtfile_create
  ;
  ; Currently this disperses individual pixels for an object in a PET.
  ; could extend this to deal with convex hulls.
  ;-

  ;set initial time
  t0=systime(/sec)


  ;set default count
  count=0L

  ;threshold the PDT
  threshold=-!values.f_infinity

  ;set a default
  if n_elements(IMAGE) eq 0 then image=0
  
  ;compute the delta
  nlam=n_elements(lam)
  dlam=lam[1:nlam-1]-lam[0:nlam-2]
  if ~array_equal(dlam,dlam[0]) then begin
     print,'PDTFILE_CREATE> The wavelength grid must be linear.'
     return
  endif
  dlam=dlam[0]

  
  ;create a file
  pdtfile=tables_name(obj,beam,fltast.file,/create,dir=conf.savedir,$
                      compr=conf.zip)
                       

  ;get the sensitivity curve
  sens=oconf->sensitivity(lam,beam=beam,sensunit=sensunit)



  ;find the pixels
  gpix=where(pet0.obj eq obj,npix)
  if npix eq 0 then begin
     print,'PDTFILE_CREATE> No objects that match ID='+strtrim(obj,2)
     pdtfile_write,pdtfile,-1,obj=obj,dlamb=dlam,sensunit=sensunit,$
                   compress=conf.zip
     return
  endif
  pet=pet0[gpix]

  ;print a message
  print,'PDTFILE_CREATE> Creating PDT for segid='+string(obj,f='(I0)')+$
        ' with '+string(npix,f='(I0)')+' pixels'
        

  ;compute xyd
  xyd=array_indices(segast.naxis,pet.xyd,/dim)

  ;-------for pixels-------
  dx=[0,0,1,1]
  dy=[0,1,1,0]
  nvert=n_elements(dx)
  ;---------------------------

  ;dummy variable
  reg=fltarr(2,nvert)
  
  ;create the output
  arr=obj_new('array')

  for i=0ul,npix-1 do begin
     ;-----FOR GROUPING OF PIXELS-----
     ;get the pixel
     xd=xyd[0,i]+dx
     yd=xyd[1,i]+dy
     ;--------------------------------

     ;transform the region to the FLT space
     xy2xy,xd,yd,segast,xf,yf,fltast
     
     ;make into a region
     reg[0,*]=xf
     reg[1,*]=yf
        

     ;compute the PATs
     pat=spec_drizzle(reg,lam,oconf,beam,count=num,imagesize=fltast.naxis)


     ;only process if there are good pixels
     if num ne 0 then begin
        ;get grism pixels
        xyg=transpose(array_indices(fltast.naxis,pat.xyg,/dim))

        ;compute all the weights
        img_val=pet[i].val
        sen_val=sens[pat.lam]
        flt_val=oconf->flat(xyg[*,0],xyg[*,1],lam[pat.lam])


        ;compute the product
        ; fractional pixel areas (PAT.VAL)
        ; direct image value (IMG_VAL)
        ; sensitivity curve (SEN_VAL)
        ; flat field (FLT_VAL)
        val=pat.val*img_val*sen_val*flt_val
        
        ;only keep the ones gt the threshold
        g=where(val gt threshold,n)
        if n ne 0 then begin
           patg=pat[g]          ;temporary variable
           pdt=replicate({PDT},n)
;           pdt.obj=pet[i].obj
;           pdt.xyd=pet[i].xyd
;           pdt.img=image
           pdt.xyg=patg.xyg
           pdt.val=val[g]
           pdt.wav=lam[patg.lam]

           arr->push,pdt,/no_copy
        endif
     endif     
  endfor

  ;clean up
  obj_destroy,arr,data=pdt,count=count

  predecimate=0b
  if predecimate then begin
     ;could do this inside the loop also... then push less and 
     ;less memory

     xyg=pdt.xyg
     lid=value_locate(lam,pdt.wav)
     npix=fltast.naxis[0]*fltast.naxis[1]
     id=xyg+npix*lid
     val=decimate_array(id,pdt.val,loc=id,count=num)
     xyg=id mod npix
     lid=id  /  npix
     
     pdt=replicate({pdt},num)
     pdt.val=val
     pdt.xyg=xyg
     pdt.wav=lam[lid]
     print,num,count,float(num)/count
     count=num
  endif

  ;write the file
  pdtfile_write,pdtfile,pdt,obj=obj,dlamb=dlam,sensunit=sensunit,$
                COMPRESS=conf.zip

  ;get final time
  t1=systime(/sec)
  dt=t1-t0
  

  ;print a message
  print,'PDTFILE_CREATE> Made PDT for segid='+string(obj,f='(I0)')+$
        ' in '+string(dt,f='(F0.1)')+' seconds'


end
