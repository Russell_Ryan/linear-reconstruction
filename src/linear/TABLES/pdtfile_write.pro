pro pdtfile_write,file,tab,COMPRESS=compress,_EXTRA=_extra
;
;  hdr=create_struct(name='PDTFILE_HEADER')
;


  ;find the suffix
  toks=strsplit(file_basename(file),'.',/ext,count=count)
  last=toks[count-1]
  
  ;figure out which file it is
  options=['pdt','pmt']
  match,strlowcase(toks),options,tid,oid,count=count

  ;check the number of matches
  case count of
     0: print,'TABLES_WRITE> Only valid file types: '+strjoin(options,',')
     1: type=options[oid[0]]
     else: print,'TABLES_WRITE> Filename has multiple types. Cannot write.'
  endcase
  if size(type,/type) eq 0 then return


  ;make the file header
  hdr=create_struct(name=type+'file_header')


  ;name the output file
  outfile=file
  compress=keyword_set(COMPRESS)
  gzip=strcmp(last,'gz',/fold)
  case compress of
     0: if ~gzip then outfile+='gz'
     1: if  gzip then compress=1b
  endcase

  
  

  ;open the file to write
  openw,lun,outfile,/get_lun,error=error,COMPRESS=compress
  if error ne 0 then begin
     print,'PDTFILE_WRITE has error.'
     stop
  endif

  
  ;is there a valid tabular data
  valid=size(tab,/type) eq 8

  ;put in the count
  if valid && tag_exist(hdr,'count') then hdr.count=n_elements(tab)

;  if keyword_set(DLAMB)    then hdr.dlamb=dlamb
;  if keyword_set(SENSUNIT) then hdr.sensunit=sensunit


  ;do we have things to put in the header?
  next=n_tags(_extra)
  
  if next ne 0 then begin
     etags=tag_names(_extra)
     htags=tag_names(hdr)
     elen=strlen(etags)
     for i=0,next-1 do begin
        htag=strmid(htags,0,elen[i])
        gg=(where(strmatch(htag,etags[i],/fold),nn))[0]
        if nn eq 1 then hdr.(gg)=_extra.(i)        
     endfor
  endif


  ;write the header
  writeu,lun,hdr

  ;write the PDT if it's valid
  if valid then writeu,lun,tab

  free_lun,lun
end

