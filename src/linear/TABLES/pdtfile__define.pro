function pdtfile::read,id,COUNT=count,DLAMB=dlamb,SENSUNIT=sensunit,OBJ=obj
  file=tables_name(id,self.beam,self.file,dir=self.dir,compress=self.compress)

  if ~file_test(file) then begin
     count=0L
     return,-1
  endif

  pdt=pdtfile_read(file,image=self.img,compress=self.compress,HDR=hdr)
  if arg_present(OBJ)      then obj=hdr.obj
  if arg_present(COUNT)    then count=hdr.count
  if arg_present(DLAMB)    then dlamb=hdr.dlamb
  if arg_present(SENSUNIT) then sensunit=hdr.sensunit
  return,pdt
end


pro pdtfile::cleanup
end


function pdtfile::init,fltdat,segast,pet0,oconf,conf,IMAGE=image,$
                       BEAM=beam,NSAMP=nsamp

  ;check input
  if size(oconf,/type) ne 11 || ~obj_isa(oconf,'axeconfig') then begin
     print,'PDTFILE::INIT> Error... oCONF should be an axeconfig object.'
     return,0b
  endif

  ;set some things
  self.img=keyword_set(IMAGE)?image:0             ;image number
  self.file=fltdat.file                           ;filename
  self.beam=keyword_set(BEAM)?strupcase(beam):'A' ;beam
  self.compress=conf.zip                          ;compress file
   
 


  ;verify that this beam is ok
  if ~oconf->has_beam(beam) then return,0b


  ;figure out if we need to remake
  self.dir=conf.savedir
  tabdir=tables_dir(beam,fltdat.file,dir=conf.savedir)
  if ~file_test(tabdir,/dir) then begin
     file_mkdir,tabdir
     make_tab=1b
  endif else make_tab=conf.remake
   
  ;ok... you asked for it, we'll make the bloody PDT
  if make_tab then begin

     
     ;apply the crude baffling
     pad=[10,10]                ;number of dispersed image pixels
     xd=pet0.xyd mod segast.naxis[1]
     yd=pet0.xyd  /  segast.naxis[1]
     oconf->getproperty,xrange=xrange,yrange=yrange
     xr=xrange[[0,0,1,1]]+[-1,-1,+1,+1]*pad[0]
     yr=yrange[[0,1,1,0]]+[-1,+1,+1,-1]*pad[1]
     xy2xy,xr,yr,fltdat,xr2,yr2,segast
     g=where(inside(temporary(xd),temporary(yd),xr2,yr2),npet)
     if npet eq 0 then return,1b
     pet=pet0[g]

     ;find unique objects
     objid=uniqify(pet.obj,count=nobj)

     ;compute wavelength to disperse at
     if ~tag_exist(conf,'dlambsim') then begin
        if ~keyword_set(NSAMP) then nsamp=tag_exist(conf,'nsamp')?conf.nsamp:5.
        disp=oconf->dispersion(fltdat.crpix[0],fltdat.crpix[1],beam=self.beam)
        dlamb=abs(float(floor(disp/nsamp)))
     endif else dlamb=conf.dlambsim
     foo=histogram([conf.lamb0],min=conf.lamb0,max=conf.lamb1,bin=dlamb,$
                   locations=lamb)
     nlamb=n_elements(lamb)

;     nlamb=ceil(float(conf.lamb1-conf.lamb0)/dlamb)+1
;     lamb=findgen(nlamb)*dlamb+conf.lamb0

     ;get number of CPUs
     ncpu=(tag_exist(conf,'ncpu') && conf.ncpu ge 1)?conf.ncpu:(!cpu.hw_ncpu/2)
     ncpu=nobj<ncpu>1


     ;start the processing
     if ncpu ne 1 then begin    ;parallel processing
        ;hooray!!! using mutli-threading!!
        
        ;check for a broot to set..
        root=tag_exist(conf,'broot')?conf.broot:'pdtfile'

        ;run the thread pool
        pool,ncpu,'pdtfile_create',polltime=1.,errmsg=errmsg,status=status,$
             exec='!quiet=1b',outroot=root,iters={obj:objid},$
             args=['obj','lam','pet','oconf','conf','fltast',$
                   'segast','beam','count=count'],$;,'image=image'],$
             consts={lam:lamb,pet:pet,oconf:oconf,conf:conf,$
                     fltast:fltdat,segast:segast,beam:self.beam}
        if ~strcmp(errmsg,'') then begin
           print,'PDTFILE::INIT> Pool had some error: '+errmsg
           return,0b
        endif
        
     endif else begin           ;serial processing
        ;boo!!! serial processing
        
        for i=0ul,nobj-1 do pdtfile_create,objid[i],lamb,pet,oconf,conf,$
                                           fltdat,segast,self.beam,$
                                           count=count;,image=image
     endelse

  endif
  
  return,1b
end

pro pdtfile__define
  _={PDTFILE,$
     compress:0b,$              ;to compress?
     inherits img,$             ;image ID number
     file:'',$                  ;name of the file
     beam:'',$                  ;beam ID
     dir:'' $                   ;output directory
    }
end
