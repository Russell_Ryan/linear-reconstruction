pro pmtfile_write,file,tab,COMPRESS=compress
  h={PMTFILE_HEADER}            ;only hardcode here!



  ;check to compress the file
  compress=keyword_set(COMPRESS)
  if ~compress then begin
     tok=strsplit(file,'.',/ext,count=n)
     if tok[n-1] eq 'gz' then compress=1b
  endif     


  ;is data the valid?
  valid=size(tab,/type) eq 8

  ;open the file to write
  openw,lun,file,/get_lun,error=error,COMPRESS=compress
  if error ne 0 then begin
     print,'PMTFILE_WRITE has error.'
     stop
  endif

  ;make a header

  if valid then h.count=n_elements(tab)


  writeu,lun,h

  if valid then writeu,lun,tab
  
  free_lun,lun
end

  
