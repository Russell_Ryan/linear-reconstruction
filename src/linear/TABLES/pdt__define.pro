;+
; Pixel Dispersion Table (PDT)
; specifies how a given pixel in the PET will disperse onto a given 
; grism image. 
;-

pro pdt__define
  _={PDT,$
;     inherits obj,$             ;put in header
;     inherits img,$             ;don't need (put in by extract_grism)
;     inherits xyd,$             ;don't need
     inherits xyg,$             ;need
     inherits wav,$             ;need
     inherits val $             ;need
    }
;     obj:0u,$                   ;object index [was ll]    (INPUT jj)
;     img:0u,$                   ;grism index  [was ll]    (OUTPUT ii)
;     xyd:0ul,$                  ;direct image pixel       (NOT USED)
;     xyg:0ul,$                  ;grism pixel  [was ll]    (OUTPUT ii)
;     wav:0.0,$                  ;actual wavelength (A)
;     val:0.0 $                  ;collection of terms      (PROJECTION)
;    }
end



