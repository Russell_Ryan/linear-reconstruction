;+
; build the binary table name
;-

function tables_name,id,beam,fits,SUFFIX=suffix,COMPRESS=compress,_EXTRA=_extra

  ;set the suffix
  if ~keyword_set(SUFFIX) then suffix='pdt'

  ;get the directory name
  outdir=tables_dir(beam,fits,_EXTRA=_extra)

  ;update the suffix
  if keyword_set(COMPRESS) then suffix+='.gz'

  ;make the file name
  tabfile=outdir+strtrim(id,2)+'.'+suffix

  return,tabfile
end
