pro footprint,conf,axeconf,objects,flts

  ;get the padding as a hidden parameter
  if tag_exist(conf,'padxy') && n_elements(conf.padxy) eq 2 then begin
     padxy=floor(conf.padxy)>0
  endif else begin
     padxy=[5,5]
  endelse
  beam='A'

  ;number of objects
  nobj=n_elements(objects)
  if nobj eq 0 then return

  ;numbef of images
  nimg=n_elements(flts)
  if nimg eq 0 then return


  for i=0u,nimg-1 do begin
 
     flts[i]->GetProperty,ast=ast,file=file

     
     root=file_basename(file,'_flt.fits')
     wavfile=root+'_wav.fits'

     ;make the tables
;     make_tables,objects,flts[i],axeconf,conf

     ;open the fits file
     fits_open,wavfile,fcb,/write

     ;write a zeroth file
     h=['']
     sxaddpar,h,'IMAGE',file,' original FLT image'
     sxaddpar,h,'NOBJ',nobj,' number of objects'
     sxaddhist,'average wavelength per grism pixel',h
     fits_write,fcb,0,h


     for j=0ul,nobj-1 do begin


        odt=objects[j]->read_odt(flts[i],axeconf,beam,count=count)
        
        if count ne 0 then begin
           objects[j]->GetProperty,name=name,segid=segid
           
           ;compute wavelengths
           wav=decimate_array(odt.xyg,odt.wav,mode='average',loc=xyg)

           ;compute bounding box
           ij=array_indices(ast.naxis,xyg,/dim)
           i0=(min(ij[0,*])-padxy[0])>0
           i1=(max(ij[0,*])+padxy[0])<(ast.naxis[0]-1)
           j0=(min(ij[1,*])-padxy[1])>0
           j1=(max(ij[1,*])+padxy[1])<(ast.naxis[1]-1)
           sz=[i1-i0,j1-j0]+1   ;size of the output image

           
           ;fill an image
           img=make_array(sz,value=!values.f_nan,type=4)
           img[ij[0,*]-i0,ij[1,*]-j0]=wav

           ;make a header
           mkhdr,h,img

           ast2=ast
           ast2.crpix-=[i0,j0]
           ast2.naxis=sz

           ast2hdr,h,temporary(ast2)
           sxaddpar,h,'LTV1',-i0,' offset in X to subsection start'
           sxaddpar,h,'LTV2',-j0,' offset in Y to subsection start'
           sxaddpar,h,'EXTNAME',name,' extension name'
           sxaddpar,h,'SEGID',segid,' ID from segmap'
           sxaddpar,h,'I0',i0,' start of X coordinate in full image'
           sxaddpar,h,'I1',i1,' end of X coordinate in full image'
           sxaddpar,h,'J0',j0,' start of Y coordinate in full image'
           sxaddpar,h,'J1',j1,' end of Y coordinate in full image'
         
           ;write to the file
           fits_write,fcb,img,h,extname=name           

        endif

     endfor
     fits_close,fcb


  endfor







end
