;
;This will work.. but needs a lot of help
;
; Currently only check for Beam A to exist, but need to build in 
; functionality to remake the Object Mask Tables (OMTs).
;



pro make_tables,conf,axeconf,objects,flts
;pro make_tables,objects,fltfile,axeconf,conf
  
  ;number of objects to process
  nobj=n_elements(objects)
  if nobj eq 0 then return

  ;number of FLTs
  nflt=n_elements(flts)
  if nflt eq 0 then return


  ;things for the ThreadPool
  execs=['.com object__define','.com axeconfig__define',$
         '.com axepoly__define','!quiet=1b']
  args=['proc','obj','flt','axe','beam']
  
  ;do we have orders to mask?
  mask=tag_exist(conf,'mskbeams') && ~strcmp(conf.mskbeams[0],'none',/fold)
  if mask then mskbeams=conf.mskbeams

  ;number of CPUs
  ncpu=!cpu.hw_ncpu/2
  if tag_exist(conf,'ncpu') && conf.ncpu gt 0 then ncpu=conf.ncpu




  ;process each FLT in question
  for i=0,nflt-1 do begin
     ;find out what to do?
     if conf.remake then begin
        ;ok make all of them, cause you asked for it
        ntodo=nobj
        gtodo=uindgen(ntodo)
     endif else begin
        ;only make the ones that don't exist
        exist=bytarr(nobj)
        for j=0,nobj-1 do exist[j]=objects[j]->TableExist(flts[i],'A')
        gtodo=where(~exist,ntodo)
     endelse

     ;process the ODTs
     if ntodo ne 0 then begin  
        nproc=ntodo<ncpu>1
        if nproc ge 2 then begin
           pool,nproc,'call_method',args=args,exec=execs,$
                const={proc:'make_odt',flt:flts[i],axe:axeconf,beam:'A'},$
                iters={obj:objects[gtodo]}
        endif else begin
           for j=0u,ntodo-1 do objects[gtodo[j]]->make_odt,flts[i],axeconf,'A'
        endelse
     endif


     ;process the OMTs
     if mask then begin
        nproc=nobj<ncpu>1
        if nproc ge 2 then begin
           pool,ncpu,'call_method',args=args,exec=execs,iters={obj:objects},$
                const={proc:'make_omt',flt:flts[i],axe:axeconf,beam:mskbeams}
        endif else begin
           for j=0u,nobj-1 do objects[j]->make_omt,flts[i],axeconf,mskbeams
        endelse
     endif
  endfor



end
