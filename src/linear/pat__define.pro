;+
; Pixel Area Table (PAT)
; specifies how an given direct image pixel is broken into some number
; of dispersed image pixels, including the fractional areas
;-

pro pat__define
  _={PAT,$
     inherits lam,$
     inherits xyg,$
     inherits val $
;     lam:0u,$                   ;wavelength index
;     xyg:0ul,$                  ;grism pixel index
;     val:0. $                   ;fractional pixel area
    }
end
