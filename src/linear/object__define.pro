;---- methods with __ are expected to be internal only -----
function object::__sxpar__,hdr,key,def  
  ret=sxpar(hdr,key,count=count)
  if count eq 0 then ret=def
  return,ret
end

;--- FILE MAINTENANCE ----
function object::__filetype__,file,ZIPPED=zipped
  toks=strsplit(file,'.',/ext,count=n) ;get the file tokens
  zipped=strcmp(toks[n-1],'gz',/fold)  ;find out if it's zipped
  type=zipped?toks[n-2]:toks[n-1]      ;find out the type
  return,type
end

function object::__read__,file,COUNT=count,_REF_EXTRA=_ref_extra
  ;dummy results
  count=0L
  dat=-1

  catch,theERROR
  if theERROR ne 0 then begin
     print,!error_state.msg
     return,dat
  endif
  
  ;get properties of the filename
  type=self->__filetype__(file,ZIPPED=zipped)

  ;create the header
  hdr=create_struct(name=type+'_HEADER')


  ;open file
  openr,lun,file,error=error,/get_lun,compress=zipped
  
  if error eq 0 then begin
     ;read the header
     readu,lun,hdr

     ;read data if available
     if tag_exist(hdr,'count') && hdr.count ne 0 then begin
        count=hdr.count
        dat=replicate(create_struct(name=type),count)
        readu,lun,dat
     endif

     ;close the file
     free_lun,lun  

     ;extract hdr values to return
     n=n_elements(_ref_extra)
     if n ne 0 then begin
        htags=tag_names(hdr)
        rlen=strlen(_ref_extra)
        for i=0,n-1 do begin
           htag=strmid(htags,0,rlen[i])
           g=(where(strmatch(htag,_ref_extra[i],/fold),n))[0]
           if n eq 1 then (scope_varfetch(_ref_extra[i],/ref_extra))=hdr.(g)
        endfor
     endif
  endif

  return,dat
end


pro object::__write__,file,data,_EXTRA=_extra
  catch,theERROR
  if theERROR ne 0 then begin
     print,!error_state.msg
     return
  endif

  ;get the file type
  type=self->__filetype__(file,ZIPPED=zipped)

  
  idltype=size(data,/type)
  valid=idltype eq 8

  ;some error checking
  count=valid?n_elements(data):0l

  ;create the header
  hdr=create_struct(name=type+'_HEADER')
  if tag_exist(hdr,'count') then hdr.count=count


  ;open the file
  openw,lun,file,/get_lun,error=error,compress=zipped
  if error ne 0 then begin
     print,'cannot write file?'
     return
  endif
  
  
  ;add other things to the header
  next=n_tags(_extra)
  if next ne 0 then begin
     etags=tag_names(_extra)
     htags=tag_names(hdr)
     elen=strlen(etags)
     for i=0,next-1 do begin
        htag=strmid(htags,0,elen[i])
        g=(where(strmatch(htag,etags[i],/fold),n))[0]
        if n eq 1 then hdr.(g)=_extra.(i)
     endfor
  endif
  
  ;write the header
  writeu,lun,hdr

  ;write the data
  if count ne 0 then writeu,lun,data

  ;close file
  free_lun,lun
end
function object::__directory__,fltfile,beam,MKDIR=mkdir
  fltfile->GetProperty,file=filename

  ;extract various suffixes
  suf='.'+['fits','fits.gz']
  nsuf=n_elements(suf)
  root=strarr(nsuf)
  for i=0,nsuf-1 do root[i]=file_basename(filename,suf[i])

  ;use the suffix that gives the shortest (ie most exclusive)
  mn=min(strlen(root),loc)
  root=root[loc]

  ;build the directory
  psep=path_sep()
  dir=strjoin([self.dir,root,beam],psep)+psep
  
  if keyword_set(MKDIR) && ~file_test(dir,/dir) then file_mkdir,dir

  return,dir
end


function object::__filename__,fltfile,beam,suffix

  ;get the directory
  dir=self->__directory__(fltfile,beam,/mkdir)

  ;build the filename
  filename=dir+strtrim(self.name,2)+'.'+suffix
  if self.zip then filename+='.gz'

  return,filename
end

function object::TableExist,fltfile,beam
  dir=self->__directory__(fltfile,beam)
  ret=file_test(dir,/dir)
  return,ret
end

  
  

function object::GetProperty,SEGID=segid,NAME=name,FLUX=flux,LAMB0=lamb0,$
                             LAMB1=lamb1,DLAMB=dlamb,NLAMB=nlamb
  
  if keyword_set(SEGID) then return,self.segid
  if keyword_set(NAME) then return,self.name
  if keyword_set(FLUX) then return,self.flux
  if keyword_set(LAMB0) then return,self.lamb0
  if keyword_set(LAMB1) then return,self.lamb1
  if keyword_set(DLAMB) then return,self.dlamb
  if keyword_set(NLAMB) then return,ceil((self.lamb1-self.lamb0)/self.dlamb)+1
end

pro object::GetProperty,SEGID=segid,NAME=name,FLUX=flux,LAMB0=lamb0,$
                        LAMB1=lamb1,DLAMB=dlamb,NLAMB=nlamb
  if arg_present(SEGID) then segid=self.segid
  if arg_present(NAME) then name=self.name
  if arg_present(FLUX) then flux=self.flux
  if arg_present(LAMB0) then lamb0=self.lamb0
  if arg_present(LAMB1) then lamb1=self.lamb1
  if arg_present(DLAMB) then dlamb=self.dlamb
  if arg_present(NLAMB) then nlamb=ceil((self.lamb1-self.lamb0)/self.dlamb)+1
end

pro object::wavelengths,LIMITS=limits,LAMBDA=lambda
  nlamb=self->GetProperty(/nlamb)
  limits=(findgen(nlamb+1)-0.5)*self.dlamb+self.lamb0
  lambda=findgen(nlamb)*self.dlamb+self.lamb0
end



;---- PRIMARY ROUTINES of things to do to objects ----
function object::read_omt,fltfile,axeconf,beam,COUNT=count
  file=self->__filename__(fltfile,'msk','omt')
  dat=self->__read__(file,count=count)
  return,dat
end


pro object::make_omt,fltfile,axeconf,beams,NOWRITE=nowrite,THRESHOLD=threshold

  t1=systime(/sec)

  ;threshold for a masked pixel
  if ~keyword_set(THRESHOLD) then threshold=-!values.f_infinity

  ;transform to FLT space
  xd=reform((*self.xyd)[0,*])
  yd=reform((*self.xyd)[1,*])
  reg=fltfile->xy2xy(xd,yd,*self.ast)

  ;compute the CONVEX HULL
  triangulate,reg[0,*],reg[1,*],triangles,hull
  reg=reg[*,hull]

  ;get properties of image
  fltfile->GetProperty,naxis=naxis

  ;get the wavelengths
  self->wavelengths,lamb=wav

  ;array to collect beams
  arr=obj_new('array')


  ;combine the BEAMS into a single file
  ;this saves on FILE I/O and the OMTs are generally very light-weight
  for j=0,n_elements(beams)-1 do begin
     ;disperse the CONVEX HULL
     pat=spec_drizzle(reg,wav,axeconf,beams[j],count=count,imagesize=naxis)

     ;ignore wavelength and group the pixels
     if count ne 0 then begin     
        val=decimate_array(pat.xyg,pat.val,loc=xyg,count=count)
        cat=replicate({xyg:xyg[0],val:val[0]},count)
        cat.val=temporary(val)
        cat.xyg=temporary(xyg)
        arr->push,cat,/no_copy
     endif
  endfor

  ;destroy the array collector
  obj_destroy,arr,data=pat
  val=decimate_array(pat.xyg,pat.val,loc=xyg,count=count)
  
  ;only retain pixels above a threshold
  g=where(val gt threshold,count)
  if count ne 0 then begin
     ;make the Object Mask Table
     omt=replicate({omt},count)
     omt.xyg=xyg[g]

     ;write the aggregate table
     file=self->__filename__(fltfile,'msk','omt')
     self->__write__,file,omt,count=count
  endif


end


function object::read_odt,fltfile,axeconf,beam,SENSUNIT=sensunit,COUNT=count
  file=self->__filename__(fltfile,beam,'odt')
  dat=self->__read__(file,sensunit=sensunit,count=count)
  return,dat
end

pro object::make_odt,fltfile,axeconf,beams,NOWRITE=nowrite,THRESHOLD=threshold

  nsamp=5                       ;number of samples per wavelength
                                ;bin to create the ODT

  ;threshold for a valid pixel
  if ~keyword_set(THRESHOLD) then threshold=-!values.f_infinity
  
  ;get the size of the image
  fltfile->GetProperty,naxis=naxis

  ;get the sensitivity units
  axeconf->GetProperty,sensunit=sensunit

  
  ;create wavelength array for this object
  dwav=self.dlamb/nsamp         ;resolution to disperse at
  nwav=ceil((self.lamb1-self.lamb0)/dwav)+1
  wav=findgen(nwav)*dwav+self.lamb0

  ;number of beams to disperse
  nbeam=n_elements(beams)


  ;get some stuff for each beam
;  arr=objarr(nbeam)
;  sens=fltarr(nwav,nbeam)
;  for j=0,nbeam-1 do begin
;     ;get sensitivity curve
;     sens[*,j]=axeconf->sensitivity(wav,beam=beams[j],sensunit=sensunit)
;     
;     ;the data structure
;     arr[j]=obj_new('array')
;  endfor


  ;pixel based --------------------------
  dx=[0,0,1,1]                  ;HARDCODE
  dy=[0,1,1,0]                  ;HARDCODE
  ;--------------------------------------



  ;a data collector
  arr=obj_new('array')

  ;transform each pixel
  npix=(size(*self.xyd,/dim))[1]
  nvert=n_elements(dx)          ;number of vertices
  reg=fltarr(2,nvert,npix)
  for j=0,npix-1 do begin
     ;pixel based -----------------------
     xd=(*self.xyd)[0,j]+dx     ;HARDCODE
     yd=(*self.xyd)[1,j]+dy     ;HARDCODE
     ;-----------------------------------

     ;transform coordiantes to DISPERSED-IMAGE space
     reg[*,*,j]=fltfile->xy2xy(xd,yd,*self.ast)
  endfor




  ;process each beam  
  for i=0u,nbeam-1 do begin
     ;get the sensitivity curve
     sens=axeconf->sensitivity(wav,beam=beams[i],sensunit=sensunit)

     ;process each pixel
     for j=0ul,npix-1 do begin
        ;drizzle the pixels
        pat=spec_drizzle(reg[*,*,j],wav,axeconf,beams[i],count=num,image=naxis)

        ;only keep the good oines
        if num ne 0 then begin
           ;convert to DISPRSED-IMAGE space
           xy=array_indices(naxis,pat.xyg,/dim)
        
           ;compute various terms (could uniqify on pat.lam?)
           flat_val=axeconf->flat(xy,wav[pat.lam])
           sens_val=sens[pat.lam]

           ;compute the weights (Pat.val is an area)
           wht=pat.val*(*self.wht)[j]*sens_val*flat_val*dwav

           ;apply a thresholding
           gg=where(wht gt threshold,nn)
           if nn ne 0 then begin
              pat=pat[gg]
              odt=replicate({ODT},nn)
              odt.xyg=pat.xyg
              odt.val=wht[gg]
              odt.wav=wav[pat.lam]
              
              ;collect the results
              arr->push,odt,/no_copy
           endif
        endif

     endfor                     ;end loop over pixels
     
     ;get the results
     arr->flush,data=odt,count=count
     
     if count ne 0 then begin
        ;decimate over xyd
        sz=naxis[0]*naxis[1]
        lid=value_locate(wav,odt.wav)
        pix=odt.xyg+sz*lid
        val=decimate_array(pix,odt.val,loc=pix,count=count)
        odt=replicate({odt},count)
        odt.val=val
        odt.xyg=pix mod sz
        odt.wav=wav[pix/sz]
     endif

     ;write the table to disk
     if ~keyword_set(NOWRITE) then begin
        ;create a file name ;obj,beam,root,suffix
        file=self->__filename__(fltfile,beams[i],'odt')
        self->__write__,file,odt,SENSUNIT=sensunit
     endif
  endfor

  ;clean up memory usage
  obj_destroy,arr


;  ;process each pixel
;  for i=0ul,(size(*self.xyd,/dim))[1]-1 do begin
;     ;pixel based -----------------------
;     xd=(*self.xyd)[0,i]+dx     ;HARDCODE
;     yd=(*self.xyd)[1,i]+dy     ;HARDCODE
;     ;-----------------------------------
;
;     ;transform coordiantes to DISPERSED-IMAGE space
;     reg=fltfile->xy2xy(xd,yd,*self.ast)
;;     xy2xy,xd,yd,*self.ast,xf,yf,ast;
;;
;;     ;group into a region
;;     reg[0,*]=temporary(xf)
;;     reg[1,*]=temporary(yf)
;
;
;
;
;     ;process each beam
;     for j=0,nbeam-1 do begin
;
;        ;drizzle the pixels
;        pat=spec_drizzle(reg,wav,axeconf,beams[j],count=num,imagesize=naxis)
;
;        ;only keep the good oines
;        if num ne 0 then begin
;           ;convert to DISPRSED-IMAGE space
;           xy=array_indices(naxis,pat.xyg,/dim)
;        
;           ;compute various terms (could uniqify on pat.lam?)
;           flat_val=axeconf->flat(xy,wav[pat.lam])
;           sens_val=sens[pat.lam,j]
;           
;                                 
;           ;compute the weights (Pat.val is an area)
;           wht=pat.val*(*self.wht)[i]*sens_val*flat_val*dwav
;
;           ;apply a thresholding
;           gg=where(wht gt threshold,nn)
;           if nn ne 0 then begin
;              pat=pat[gg]
;              odt=replicate({ODT},nn)
;              odt.xyg=pat.xyg
;              odt.val=wht[gg]
;              odt.wav=wav[pat.lam]
;           endif
;        
;           ;update the list
;           arr[j]->push,odt,/no_copy
;        endif
;     endfor                     ;loop over beams
;  endfor                        ;loop over pixels
;  
;  ;post process
;  for j=0,nbeam-1 do begin
;     ;clean up
;     obj_destroy,arr[j],data=odt,count=count
;
;     ;decimate over xyd
;     npix=naxis[0]*naxis[1]
;     lid=value_locate(wav,odt.wav)
;     pix=odt.xyg+npix*lid
;     val=decimate_array(pix,odt.val,loc=pix,count=count)
;     odt=replicate({odt},count)
;     odt.val=val
;     odt.xyg=pix mod npix
;     odt.wav=wav[pix/npix]
;
;     ;write the table to disk
;     if ~keyword_set(NOWRITE) then begin
;        ;create a file name ;obj,beam,root,suffix
;        file=self->__filename__(fltfile,beams[j],'odt')
;        self->__write__,file,odt,SENSUNIT=sensunit
;     endif
;  endfor

end


pro object::cleanup
  ptr_free,self.xyd,self.wht,self.ast
end

function object::init,img,gpx,hdr,SMOOTH=smooth,DIR=dir,$
                      LAMB0=lamb0,LAMB1=lamb1,DLAMB=dlamb

  ;set default values
  if ~keyword_set(LAMB0) then lamb0=7500.
  if ~keyword_set(LAMB1) then lamb1=12500.
  if ~keyword_set(DLAMB) then dlamb=25.


  ;check dimensionality
  if ~array_equal(size(img,/dim),size(gpx,/dim)) then begin
     print,'OBJECT::INIT> IMG and SEG must be same size.'
     return,0b
  endif
  
  ;get the object name
  segid=sxpar(hdr,'segid',count=count)
  if count eq 0 then begin
     print,'OBJECT::INIT> Must have name.'
     return,0b
  endif

  ;extract a name
  name=self->__sxpar__(hdr,'name',segid)

  ;extract the astrometry
  extast,hdr,ast,noparam
  if noparam eq -1 then begin
     print,'OBJECT::INIT> Must have astrometry.'
     return,0b
  endif

  ;find the good values
  g=where(gpx,ng,complement=b,ncomplement=nb)
  if ng eq 0 then begin
     print,'OBJECT::INIT> No valid pixels.'
     return,0b
  endif


  ;apply a small smoothing?
  if keyword_set(SMOOTH) then begin
     kern=[[1,4.,7.,4.,1.],$
           [4,16,26.,16,4],$
           [7,26,41,26,7],$
           [4,16,26.,16,4],$
           [1,4.,7.,4.,1.]]/273.
     if nb ne 0 then img[b]=!values.f_nan
     img=convol(img,kern,/edge_truncate,/nan,missing=0.0)
  endif


  ;get the values
  val=img[g]
  flux=total(val)
  val/=flux                     ;normalize out the flux

  ;compute x,y pair
  xyd=array_indices(ast.naxis,g,/dim)


  ;save to the self
  self.name=strcompress(name,/rem)
  self.flux=flux
  self.lamb0=self->__sxpar__(hdr,'lamb0',lamb0)
  self.lamb1=self->__sxpar__(hdr,'lamb1',lamb1)
  self.dlamb=self->__sxpar__(hdr,'dlamb',dlamb)
  self.dir=(keyword_set(DIR)?dir:'Tables/')
  self.zip=1b                   ;always zip the files
  self.xyd=ptr_new(xyd,/no_copy)
  self.wht=ptr_new(val,/no_copy)
  self.ast=ptr_new(ast,/no_copy)
  
  return,1b
end
pro odt__define
  _={ODT,$
     inherits xyg,$
     inherits val,$
     inherits wav $
    }
end

pro odt_header__define
  _={ODT_HEADER,$
     SENSUNIT:0d,$
     inherits count $
    }
end

pro omt__define
  _={OMT,$
     inherits xyg $
    }
end

pro omt_header__define
  _={OMT_HEADER,$
     inherits count $
    }
end


pro object__define 
  _={OBJECT,$
     name:'',$
     segid:0ul,$
     flux:0.,$
     lamb0:0.,$
     lamb1:0.,$
     dlamb:0.,$
     dir:'',$                   ;file directory
     zip:0b,$                   ;are files zipped?
     xyd:ptr_new(),$
     wht:ptr_new(),$
     ast:ptr_new() $
    }
end
