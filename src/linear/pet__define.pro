;+
; Pixel Extraction Table (PET)
; specifies data of pixels to be extracted, in units of direct image
;-

pro pet__define
  _={PET,$
     inherits obj,$
     inherits xyd,$
     inherits val $
    }

;     obj:0u,$                   ;index over object
;     xyd:0ul,$                  ;index over pixel in direct image
;     img:0.}                    ;direct image pixel value
end
