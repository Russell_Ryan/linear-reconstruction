function read_objects,conf,count=count

  ;get the number of extensions (ie. objects)
;  fits_info,conf.direct,n_ext=count

;  if count lt 2 then stop
  
  ;open the files
  fits_open,conf.direct,fcbd
  fits_open,conf.segmap,fcbs


  if fcbd.nextend ne fcbs.nextend then begin
     print,'READ_OBJECTS> IMG and SEG differing numbers of extens. Quitting.'
     objects=-1
     count=0l
     goto,QUIT
  endif else count=fcbd.nextend


  ;some outputs
  name=strarr(count)
  objects=objarr(count)
  for i=0,count-1 do begin
     exten_no=i+1

     ;read these images
     fits_read,fcbd,img,himg,exten_no=exten_no
     fits_read,fcbs,seg,hseg,exten_no=exten_no
    
     ;build the object
     objects[i]=obj_new('object',img,seg,himg,$
                        dir=conf.savedir,$
                        lamb0=conf.lamb0,$
                        lamb1=conf.lamb1,$
                        dlamb=conf.dlamb)
     if ~obj_valid(objects[i]) then begin
        stop,'READ_OBJECTS> error building object'
     endif

     ;get the name to ensure uniqness
     objects[i]->GetProperty,name=nm
     name[i]=nm

  endfor


  ;check for uniqness
  nm=uniqify(name,count=nuniq)
  if nuniq ne count then begin
     stop,'READ_OBJECTS> Cannot have repeated segid'
  endif






QUIT:
  ;close the files
  fits_close,fcbd
  fits_close,fcbs


  return,objects  
end
