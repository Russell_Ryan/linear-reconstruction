;+
; Astrometry from extast
;-

pro ast__define
  _={AST,$
     naxis:lonarr(2),$
     CD:dblarr(2,2),$
     CDELT:dblarr(2),$
     CRPIX:dblarr(2),$
     CRVAL:dblarr(2),$
     CTYPE:strarr(2),$
     LONGPOLE:0d,$
     LATPOLE:0d,$
     PV2:dblarr(2),$
     EQUINOX:0d $
    }
end
     
