;+
; Object Lookup Table (OLT)
;-
pro olt__define
  _={OLT,$
     inherits obj,$             ;object/SEGID
     npix:0u,$                  ;number of pixels for this object
     flux:0d $                  ;flux from direct iamge
    }
end
