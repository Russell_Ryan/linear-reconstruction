
pro linear,configfile,_EXTRA=_extra
 
  t1=systime(/sec)
  
  ;check the system variables are set
  var=['LINEAR','LINEAR_SRC','LINEAR_CONFIG','LINEAR_FILTER']
  env=getenv(var)
  g=where(env eq '' or ~file_test(env),n)
  if n ne 0 then begin
     print,'LINEAR> Error. You must have set the environment variables:'
     for j=0,n-1 do print,'        '+var[g[j]]
     print,'        before proceeding.'
     return
  endif
 
  ;turn some message reporting off.  It's fine
  fullquiet=1b
  if fullquiet then begin
     quiet=!quiet & !quiet=1
     except=!except & !except=0
  endif

  
  ;get a configuration file
  if n_elements(configfile) eq 0 then configfile='linear.config'
  if ~file_test(configfile) then begin
     print,'LINEAR> Error.  Configfile not found.'
     return
  endif

  ;inputs
  conf=config_read(configfile,_EXTRA=_extra)
  

  ;print a message
  linear_splash,conf



  ;check for savedir...
  if ~file_test(conf.savedir,/dir) then file_mkdir,conf.savedir
  if lastchar(conf.savedir) ne path_sep() then conf.savedir+=path_sep()

  
  ;new PET
  objects=read_objects(conf,count=nobj)
  if nobj eq 0 then begin
     print,'LINEAR> There are no objects to extract.'
     goto,CLEANUP
  endif

  ;read images?
  flts=read_fltfiles(conf,count=nflt)
  if nflt eq 0 then begin
     print,'LINEAR> There are no images to analyze.'
     goto,CLEANUP
  endif  

  ;read the aXe-like data
  confdir=get_directory('LINEAR_CONFIG')+'aXe'+path_sep()
  axeconf=obj_new('axeconfig',confdir+conf.axeconf,/silent,/unity)

  ;make the ODTs and OMTs
  make_tables,conf,axeconf,objects,flts

  
  ;fork based on job type
  case strlowcase(conf.mode) of
     'extract': extract_grism,conf,axeconf,objects,flts
     'simulate': simulate_grism,conf,axeconf,objects,flts
     'footprint': footprint,conf,axeconf,objects,flts
     'contamination': contamination,conf,axeconf,objects,flts
     'optimize': optimize_grism,conf,axeconf,objects,flts
     'ignore':
     else: begin
        print,'LINEAR> Mode ('+conf.mode+') is not recognized.'
        return
     end
  endcase



CLEANUP:

  ;cleanup
  obj_destroy,axeconf
  obj_destroy,objects
  obj_destroy,flts


  t2=systime(/sec)

  ;we are done, so print a message
  print,'LINEAR> Done...'
  print,'        runtime:',t2-t1

  ;reset the messages
  if fullquiet then begin
     t=check_math()
     !quiet=quiet
     !except=except
  endif

end
