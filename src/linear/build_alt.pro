;+
; BUILD_ALT
;
; Get the Astrometry Lookup Tables (ALT) for a host of images.  There
; are two options for getting this info:
;
; (1) conf.wcstype == 'fltlst'
;     the astrometry is read from the list of FLT files.  The onus is
;     on the user to ensure these headers are valid and correct.
;
; (2) conf.wcstype == 'wcslst'
;     The astrometry is read from a file (default = wcs.lst) that
;     contains ROOT, CRVAL1, CRVAL2, ORIENTAT and the remaining 
;     necessary keywords are generated automagically for WFC3/IR.
;     Eventually would like to create another file that contains the 
;     properties for other instruments (ACS, WFC3/UVIS, WFIRST, etc.)
;
;-

;function build_alt_readsip,hdr,ab,ORDER=order
;  ;+
;  ; read a SIP matrix
;  ;-
;
;  order=sxpar(hdr,ab+'_ORDER')
;  sip=dblarr(order+1,order+1)
;  for i=0,order do begin
;     ii=strtrim(i,2)
;     for j=0,order-i do begin
;        if i+j ge 2 then begin
;           jj=strtrim(j,2)
;           sip[i,j]=sxpar(hdr,ab+'_'+ii+'_'+jj)
;        endif
;     endfor
;  endfor
;
;  return,sip
;end
;function build_alt_sip,hdr,VALID=valid
;
;
;  a=build_alt_readsip(hdr,'a',order=na)
;  b=build_alt_readsip(hdr,'b',order=nb)
;  ap=build_alt_readsip(hdr,'ap',order=nap)
;  bp=build_alt_readsip(hdr,'bp',order=nbp)
;  
;
;  if nap eq 0 && nbp eq 0 && na ne 0 && nb ne 0 then begin
;     extast,hdr,ast,noparam
;
;     if noparam gt 0 then begin
;        ;build the inverse SIP
;        nap=5 & nbp=5
;        print,'BUILD_ALT_SIP> Building INVERSE SIP transform. NAP='+$
;              string(nap,f='(I0)')+' NBP='+string(nbp,f='(I0)')
;        ast=compute_inverse_sip(ast,nap=nap,nbp=nbp)
;        ap=ast.distort.ap
;        bp=ast.distort.bp
;     endif
;  endif
;     
;
;
;  distort={name:'SIP',a:a,b:b,ap:ap,bp:bp}
;  valid=(na ne 0 && nb ne 0) || (nap ne 0 && nbp ne 0)  
;
;  return,distort
;end



function build_alt_fromlist,list,COUNT=count
  ;+
  ; Build the Astrometry Lookup Table (ALT) from a file which contains 
  ; a column for ROOTNAME, CRVAL1, CRVAL2, CROTA.  These are then used 
  ; to build a header (assuming other things for CRPIX, NAXIS, etc) to 
  ; populate the header.  Currently, it assumes the parameters for 
  ; WFC3/IR, but eventually should be passed as additional columns in 
  ; the LIST file.
  ;-


  ;HARDCODED
  print,'BUILD_ALT_FROMLIST> Hardcode for WFC3/IR'
  fovfile='hst_wfc3_ir_fov.fits' & chipname='ir'
;  fovfile='hst_wfc3_ir_fov_nosip.fits' & chipname='ir'
;  fovfile='wfirst_wfi_fov.fits' & chipname='sca10'



  ;read the WCS to simulate
  readcol,list,root,crval1,crval2,orientat,f='a,d,d,f',$
          count=nimg,/silent
  


  ;check for commented entries
  g=not_comments(root,COUNT=count,/silent)
  if count eq 0 then stop,'BUILD_ALT_FROMLIST> '+list+' contains no valid WCS.'
  select,g,root,crval1,crval2,orientat


  ;read the header for the given FoV file and chip
;  fits_open,getenv('LINEAR_CONFIG')+'camera'+path_sep()+fovfile,fcb
;  fits_read,fcb,dat,hdr,extname=chipname
;  fits_close,fcb


  fovfile=getenv('LINEAR_CONFIG')+'camera'+path_sep()+fovfile
  hdr=headfits(fovfile,ext=chipname)
;  pixscl=sxpar(hdr,'PIXSCL*')

  extast,hdr,ast

  ;add two things
;  sxaddpar,hdr,'CD1_1',0d0
;  sxaddpar,hdr,'CD1_2',0d0
;  sxaddpar,hdr,'CD2_1',0d0
;  sxaddpar,hdr,'CD2_2',0d0
;  sxaddpar,hdr,'CRVAL1',0d0
;  sxaddpar,hdr,'CRVAL2',0d0

  alt=create_struct(ast,'file','')


;  ;build the ALT
;  alt={file:'',$
;       crval:dblarr(2),$
;       cd:dblarr(2,2),$
;       naxis:sxpar(hdr,'NAXIS*'),$
;       cdelt:sxpar(hdr,'CDELT*'),$
;       crpix:sxpar(hdr,'CRPIX*'),$
;       ctype:sxpar(hdr,'CTYPE*'),$
;       longpole:sxpar(hdr,'LONGPOLE'),$
;       latpole:sxpar(hdr,'LATPOLE'),$
;       equinox:sxpar(hdr,'EQUINOX')}
;
;  ;look for the PV2
;  pv2=sxpar(hdr,'PV2',count=npv2)
;  alt=create_struct(alt,'PV2',(npv2 eq 0)?0.:pv2);
;
;
;
;  print,'THIS DOES NOT WORK FOR THE FOV FILES';
;
;
;  ;get the distortion model
;  distort=build_alt_sip(hdr,valid=valid)
;  if valid then begin
;     alt.ctype=['RA---TAN-SIP','DEC--TAN-SIP']
;     alt=create_struct(alt,'distort',distort)
;  endif else begin
;     alt.ctype=['RA---TAN','DEC--TAN']
;  endelse
;
;
  ;make a master file
  alt=replicate(alt,count)
  alt.file=root+'_flt.fits'
  alt.crval[0]=crval1
  alt.crval[1]=crval2

  ;fix the CD matrix
  for i=0,count-1 do begin
     ;do some trig and make a rotation matrix
     cs=cos(orientat[i]*!DPI/180d0)
     sn=sin(orientat[i]*!DPI/180d0)
     rot=[[+cs,-sn],[+sn,+cs]]
 
     alt[i].cd #= rot
    
;     ;update the ALT
;     alt[i].cd=rot##diag_matrix(pixscl/3600d)
;     alt[i].crval=[crval1[i],crval2[i]]
  endfor


  return,alt
end


function build_alt_fromfits,file,COUNT=count,SILENT=silent
  ;+
  ; Build the Astrometry Lookup Table (ALT) from a series of 
  ; dispersed images.  the fullpaths of the files are given in a 
  ; file list, which is passed as the sole input to this code.
  ;-

  ;read the file that contains file names
  readcol,file,files,count=count,f='a',SILENT=silent
  
  ;check for commented entries
  g=not_comments(files,count=count,/silent)
  if count eq 0 then stop,'FILE ('+file+') contains no valid files.'
  select,g,files

  ;process each file
  for i=0,count-1 do begin

     if file_test(files[i]) then begin

        ;read the header
        hdr=headfits(files[i],ext=1,SILENT=silent)
        extast,hdr,tmp
        tmp=create_struct(tmp,'file',files[i])


        if tag_exist(tmp,'DISTORT') && strcmp(tmp.distort.name,'SIP') then begin
           na=(tmp.distort.a)[0]
           nb=(tmp.distort.b)[0]
           nap=(tmp.distort.ap)[0]
           nbp=(tmp.distort.bp)[0]
           
           has_forward=(na ne 1 || nb ne 1)
           has_reverse=(nap ne 1 || nbp ne 1)

           if has_forward && ~has_reverse then $
              print,'BUILD_ALT_FROMFITS> Have only forward SIP, both needed.'
           if ~has_forward && has_reverse then $
              print,'BUILD_ALT_FROMFITS> Have only reverse SIP, both needed.'
        endif


;        ;create temporary variable
;        tmp={file:files[i],$
;             crval:sxpar(hdr,'CRVAL*'),$
;             cd:dblarr(2,2),$
;             naxis:sxpar(hdr,'NAXIS*'),$
;             cdelt:[1d,1d],$
;             crpix:sxpar(hdr,'CRPIX*'),$
;             ctype:sxpar(hdr,'CTYPE*'),$
;             longpole:sxpar(hdr,'LONGPOLE'),$
;             latpole:sxpar(hdr,'LATPOLE'),$
;             pv2:0d,$
;             equinox:sxpar(hdr,'EQUINOX')}
;        tmp.cd[0,0]=sxpar(hdr,'cd1_1')
;        tmp.cd[0,1]=sxpar(hdr,'cd1_2')
;        tmp.cd[1,0]=sxpar(hdr,'cd2_1')
;        tmp.cd[1,1]=sxpar(hdr,'cd2_2')
;;;
;
;        ;get the SIP
;        distort=build_alt_sip(hdr,valid=valid)
;        if valid then tmp=create_struct(tmp,'distort',distort)
;
        ;build/fill the output
        if i eq 0 then begin
           alt=replicate(tmp,count)
        endif else begin
           alt[i]=tmp
        endelse





     endif else begin
        print,'BUILD_ALT_FROMFILE> the file ('+files[i]+') does not exist.'
     endelse
  endfor     

  return,alt
end


function build_alt,conf,COUNT=count;,FLTLST=fltlst
  ;+
  ; get the astrometry-lookup table
  ;-

  ;this forces the user to use
;  if keyword_set(FLTLST) then conf.wcstype='fltlst'

  case strlowcase(conf.wcstype) of
     'wcslst': alt=build_alt_fromlist(conf.wcslst,count=count)
     'fltlst': alt=build_alt_fromfits(conf.fltlst,count=count)
     else: stop,'WCSTYPE ('+conf.wcstype+') keyword not supported.'
  endcase
  
  return,alt
end
