;+
; Script to continuously print to terminal
;-

pro cprint,n1,n2,MSG=msg
  frac=float(n1)/float(n2)*100.
  if ~keyword_set(MSG) || (size(msg,/type) ne 7) then msg=''

;  if keyword_set(MSG) then begin
     fmt='(%"\33[1M '+msg+' %s/%s = %s\% \33[1A")'
;  endif else begin
;     fmt='(%"\33[1M %s/%s = %s\% \33[1A")'
;  endelse

  print,string(n1,f='(I0)'),string(n2,f='(I0)'),string(frac,f='(I3)'),f=fmt

end

