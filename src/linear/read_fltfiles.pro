function read_fltfiles,conf,COUNT=count
  if strcmp(conf.mode,'extract') then conf.wcstype='fltlst'

  psep=path_sep()
  path=getenv('LINEAR_CONFIG')+psep+'camera'+psep

  ;where to get the astrometry?
  fovfile=path+'hst_wfc3_ir_fov.fits'
  chipname='IR'


  case strlowcase(conf.wcstype) of
     'wcslst': begin
        if ~file_test(conf.wcslst) then begin
           print,'READ_FLTFILES> WCSLIST not found.'
           return,0b
        endif
        readcol,conf.wcslst,root,crval1,crval2,orientat,count=count,f='a,d,d,f'
        g=not_comments(root,count=count)
        if count eq 0 then return,0
        flts=objarr(count)
        for i=0,count-1 do begin
           cat={root:root[g[i]],crval1:crval1[g[i]],$
                crval2:crval2[g[i]],orientat:orientat[g[i]]}
           flts[i]=obj_new('fltfile',cat,chipname,fovfile=fovfile)
        endfor
     end
     'fltlst': begin
        if ~file_test(conf.fltlst) then begin
           print,'READ_FLTFILES> FLTLIST not found.'
           return,0b
        endif
        readcol,conf.fltlst,file,f='a',count=count
        g=not_comments(file,count=count)
        if count eq 0 then return,0


        flts=objarr(count)
        for i=0,count-1 do flts[i]=obj_new('fltfile',file[g[i]],'SCI')
     end
     else: stop
  endcase

  return,flts
end
