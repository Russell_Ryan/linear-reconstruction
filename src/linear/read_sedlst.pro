function read_sedlst,conf,COUNT=count

  ;read the sed.lst file
  readcol,conf.sedlst,name,file,z,mag,f='a,a,f,f',/silent,count=count

  ;remove comments
  g=not_comments(name,count=count)
  

  ;make the output
  sedlib=replicate({name:'',file:'',z:0.,mag:0.},count)


  ;fill the output
  sedlib.name=name[g]
  sedlib.file=file[g]
  sedlib.z=z[g]
  sedlib.mag=mag[g]





  return,sedlib
end
