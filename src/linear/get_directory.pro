;+
; quick script to access the LINEAR environment variables
;-


function get_directory,type
  psep=path_sep()
  dir=getenv(type)
  if lastchar(dir) ne psep && dir ne '' then dir+=psep
  return,dir
end
